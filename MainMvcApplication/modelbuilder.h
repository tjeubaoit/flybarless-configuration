#ifndef MODELBUILDER_H
#define MODELBUILDER_H

#include "abstractadaptorfactory.h"
#include <QHash>
#include <QObject>

class ParameterModel;
class AdaptorModel;
class Ui_MainWidget;

class ModelBuilder: public AbstractAdaptorFactory
{
public:
    explicit ModelBuilder(Ui_MainWidget* ui);

    ParameterModel* buildParameterModel();
    AdaptorModel* buildAdaptorModel();

    AbstractFieldAdaptor* createAdaptor(int, QObject *);

private:
    void intializeWidgetHashValues();

    QHash<int, QObject*> m_hash;
    Ui_MainWidget *ui;
    ParameterModel* m_paramModel;
    AdaptorModel * m_adaptorModel;

};

#endif // MODELBUILDER_H
