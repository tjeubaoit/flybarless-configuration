#ifndef GLOBAL_H
#define GLOBAL_H

#include <QSemaphore>
#include <QMutex>
#include <QReadWriteLock>
#include <QWaitCondition>


#define APPLICATION_NAME "Apollo Flybarless Configuration"
#define APPLICATION_ORGANIZATION "Monster"
#define APPLICATION_VERSION "0.9.15 beta"
#define APP_PROFILE_FILE_EXTENSION ".afp"
#define APP_PROFILE_FILE_PREFIX "conf/profile"
#define APP_CONFIGURATION_FILE_PATH QStandardPaths::standardLocations(QStandardPaths::DataLocation).at(0) + "/fbl.afc"

#define MAX_TAB_COUNT 9

#define HELLO_REQUEST "*DEVICE?#"
#define HELLO_RESPONSE "VTEKQUAD01"
#define RXCENTRE_REQUEST "0129,001"
#define COMMAND_HARDWARE "*HADRWARE?#"
#define COMMAND_FIRMWARE "*FIRMWARE?#"
#define PRODUCT_ID 24577
#define VENDOR_ID 1027

#define WINDOW_WIDTH 1000
#define WINDOW_HEIGHT 600
#define WINDOW_TITLE_HEIGHT 40

#define RX_MONITOR_WIDTH 190
#define RX_MONITOR_HEIGHT 190
#define RX_SENSOR_WIDTH 18
#define RX_SENSOR_HEIGHT 18

#define RX_MIN_VAL 920
#define RX_MAX_VAL 2120
#define RX_CENTER_VAL 1520

#define TRIM_MIN -100
#define TRIM_MAX 100

#define SLIDER_IDEAL_MIN 80
#define SLIDER_IDEAL_MAX 120


class Fbl
{
public:
    enum RxType
    {
        Tradition = 1,
        SBus = 2,
        PPM = 3,
        DSM2 = 4,
        DSMX = 5,
        JR_XBus = 6,
        Satellite = 0
    };

    enum Tabs
    {
        TabStart = 0,
        TabRx = 1,
        TabSensor = 2,
        TabSwash = 3,
        TabServo = 4,
        TabCollective = 5,
        TabCyclic = 6,
        TabTail = 7,
        TabGov = 8
    };

    enum SwashType
    {
        Hr3 = 1,
        H3 = 2,
        H1 = 3,
        H4 = 4,
        _135deg = 5
    };

    enum GovernorMode
    {
        Disabled = 0,
        Electric = 1,
        Nitro = 2
    };
};

void qTextToDouble(QString, int& left, int& right);

#define SLEEP(delay) QThread::currentThread()->msleep(delay);
#define FILL(text, len) while (text.length() < len) { text.insert(0, "0"); }
#define TO_BYTES(str) QString(str).toUtf8();

#endif // GLOBAL_H

