#ifndef ADAPTORMODEL_H
#define ADAPTORMODEL_H

#include <QObject>
#include <QHash>
#include <QList>

class AbstractFieldAdaptor;

class AdaptorModel : public QObject
{
    Q_OBJECT
public:
    explicit AdaptorModel(QObject *parent = 0);
    ~AdaptorModel();

    int size() const;
    QList<QObject*> getListFields() const;

    AbstractFieldAdaptor* getAdaptor(int addr) const;
    QList<AbstractFieldAdaptor*> getListAdaptors() const;
    void addAdaptor(int addr, AbstractFieldAdaptor*);
    void removeAdaptor(int addr);

    int getAdaptorValue(int addr) const;
    bool isAdaptorReadOnly(int addr) const;

public slots:
    void setAdaptorValue(int addr, int val);
    void setAdaptorReadOnly(int addr, bool val);

signals:
    void _adaptorValueChanged(int, int, bool = false);

private slots:
    void adaptorValueChanged(const QVariant&, bool isBlocking);

private:
    QHash<int, AbstractFieldAdaptor*> m_adaptorHash;

};

#endif // ADAPTORMODEL_H
