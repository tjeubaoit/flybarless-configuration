#include "global.h"
#include "address.h"
#include "resources.h"
#include "mainwidget_ui.h"
#include "filehelper.h"

#include <QDebug>
#include <QPropertyAnimation>
#include <QParallelAnimationGroup>
#include <QGraphicsOpacityEffect>
#include <QSequentialAnimationGroup>
#include <QKeyEvent>

#define ANIMATION_DURATION_WHEN_CHANGE_TAB 200
#define DURATION_ANIMATION 200


Ui_MainWidget::Ui_MainWidget() : CustomFrame(),
    m_res(ResourceManager::instance())
{
    setupUi(this);

    // global
    CustomFrame::setWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    CustomFrame::setWindowTitleSize(WINDOW_WIDTH, WINDOW_TITLE_HEIGHT);
    CustomFrame::setWindowTitle(APPLICATION_NAME);
    CustomFrame::setBackgroundImage(url_application_background);

    lbLineEditAlert = new LineEditAlert(this);
    lbLineEditAlert->setText(Res(1));
    lbLineEditAlert->setFixedSize(170, 45);

    progressDialog = new ProgressDialog(this);
    progressDialog->hide();

    frameWriteLoadedParams->hide();
    checkGyroOff->hide();

    cbLanguage->addItem("English", "en");
    cbLanguage->addItem("Tiếng Việt", "vi");
    cbLanguage->hide();

    // init pointer objects

    // tab start
    btGroupRadioHeliSize = new QButtonGroup(this);
    flyStyleView = new FlyStyleView(frameStart);
    // tab rx
    rxViewLeft = new RxMonitorView(RX_MONITOR_WIDTH, RX_MONITOR_HEIGHT,
                                   RX_SENSOR_WIDTH, RX_SENSOR_HEIGHT, frameRx);
    rxViewRight = new RxMonitorView(RX_MONITOR_WIDTH, RX_MONITOR_HEIGHT,
                                    RX_SENSOR_WIDTH, RX_SENSOR_HEIGHT, frameRx);
    // tab sensor
    btGroupSensorDirection = new ButtonGroup(url_checked_icon, frameSensor);
    // tab swash
    btGroupSwashplate = new ButtonGroup(url_checked_icon, stackedSwash->widget(0));
    btGroupRotateDirection = new ButtonGroup(url_checked_icon, frameSwash);
    swashView = new AdvanceServosView(btnSwashView->width(),
                                             btnSwashView->height(),
                                             stackedSwash->widget(1));
    // tab servos
    servoView = new AdvanceServosView(btnServosView->width(),
                                              btnServosView->height(),
                                              stackedServos->widget(1));
    sliderServoCh1 = new DoubleDirectSlider(stackedServos->widget(0));
    sliderServoCh2 = new DoubleDirectSlider(stackedServos->widget(0));
    sliderServoCh3 = new DoubleDirectSlider(stackedServos->widget(0));
    sliderServoCh4 = new DoubleDirectSlider(stackedServos->widget(0));
    trimSwashCyclic = new SwashTrimCyclic(stackedServos->widget(1));
    // tab collective
    btGroupCollectDirection = new ButtonGroup(url_checked_icon,
                                              stackedCollective->widget(0));
    trimSwashCollective = new SwashTrimCollective(stackedCollective->widget(1));
    // tab tail
    btGroupRadioTail = new QButtonGroup(this);
    sliderTailServo = new DoubleDirectSlider(stackedTail->widget(0));

    lbWriteLoadedParams->setText(Res(7));
}

Ui_MainWidget::~Ui_MainWidget()
{
}

void Ui_MainWidget::registerUiConnections()
{
    connect(btnSwashAdvance, SIGNAL(clicked(bool)),
            this, SLOT(btnChangeAdvanceNormalMode_clicked(bool)));
    connect(btnSwashAdvance, SIGNAL(toggled(bool)),
            this, SLOT(btnChangeAdvanceNormalMode_toggled(bool)));

    connect(btnServosAdvance, SIGNAL(clicked(bool)),
            this, SLOT(btnChangeAdvanceNormalMode_clicked(bool)));
    connect(btnServosAdvance, SIGNAL(toggled(bool)),
            this, SLOT(btnChangeAdvanceNormalMode_toggled(bool)));

    connect(btnCollectiveAdvance, SIGNAL(clicked(bool)),
            this, SLOT(btnChangeAdvanceNormalMode_clicked(bool)));
    connect(btnCollectiveAdvance, SIGNAL(toggled(bool)),
            this, SLOT(btnChangeAdvanceNormalMode_toggled(bool)));

    connect(btnCyclicAdvance, SIGNAL(clicked(bool)),
            this, SLOT(btnChangeAdvanceNormalMode_clicked(bool)));
    connect(btnCyclicAdvance, SIGNAL(toggled(bool)),
            this, SLOT(btnChangeAdvanceNormalMode_toggled(bool)));

    connect(btnTailAdvance, SIGNAL(clicked(bool)),
            this, SLOT(btnChangeAdvanceNormalMode_clicked(bool)));
    connect(btnTailAdvance, SIGNAL(toggled(bool)),
            this, SLOT(btnChangeAdvanceNormalMode_toggled(bool)));

    connect(trimSwashCyclic, SIGNAL(_monitorChannelValueChanged(int,int)),
            trimSwashCollective, SLOT(setTrimMonitorValue(int,int)));

    connect(checkCyclicRing, SIGNAL(toggled(bool)),
            editCyclicRing, SLOT(setEnabled(bool)));
    connect(checkPitchpump, SIGNAL(toggled(bool)),
            editPitchpump, SLOT(setEnabled(bool)));

    connect(rxViewLeft, SIGNAL(_sensorValueChanged(qint32, qint32)),
            this, SLOT(rxView_sensor_value_changed(qint32, qint32)));
    connect(rxViewRight, SIGNAL(_sensorValueChanged(qint32, qint32)),
            this, SLOT(rxView_sensor_value_changed(qint32, qint32)));

    connect(swashView, SIGNAL(_angleChanged(QList<int>)),
            this, SLOT(swashView_angle_changed(QList<int>)));
}

void Ui_MainWidget::intializeWidgetProperties()
{
    // tab start

    btGroupRadioHeliSize->setExclusive(true);
    btGroupRadioHeliSize->addButton(btnHeliSize1, 1);
    btGroupRadioHeliSize->addButton(btnHeliSize2, 2);
    btGroupRadioHeliSize->addButton(btnHeliSize3, 3);
    btGroupRadioHeliSize->addButton(btnHeliSize4, 4);
    btGroupRadioHeliSize->addButton(btnHeliSize5, 5);
    btGroupRadioHeliSize->addButton(btnHeliSize6, 6);
    btGroupRadioHeliSize->addButton(btnHeliSize7, 7);
    btnHeliSize1->setChecked(true);

    flyStyleView->move(btnFlyStyle->pos());

    // tab rx

    cbTxMode->addItem("Mode 1", 1);
    cbTxMode->addItem("Mode 2", 2);
    cbTxMode->addItem("Mode 3", 3);
    cbTxMode->addItem("Mode 4", 4);

    cbReceiverType->addItem("Traditional Receiver", Fbl::Tradition);
    cbReceiverType->addItem("PPM", Fbl::PPM);
    cbReceiverType->addItem("S-Bus", Fbl::SBus);
    cbReceiverType->addItem("JR XBus/UDI", Fbl::JR_XBus);
    cbReceiverType->addItem("DSM2/DSMX Satellite", Fbl::Satellite);

    cbSatelliteType->addItem("DSM2", Fbl::DSM2);
    cbSatelliteType->addItem("DSMX", Fbl::DSMX);
    cbSatelliteType->hide();

    btnBind->hide();

    rxViewLeft->move(btnRxLeft->pos());
    rxViewLeft->setRangeValue(RX_MIN_VAL, RX_MAX_VAL);
    rxViewLeft->setLocation(RX_LOCATION_LEFT);
    rxViewLeft->setObjectName(QStringLiteral("rxViewLeft"));

    rxViewRight->move(btnRxRight->pos());
    rxViewRight->setRangeValue(RX_MIN_VAL, RX_MAX_VAL);
    rxViewRight->setLocation(RX_LOCATION_RIGHT);
    rxViewRight->setObjectName(QStringLiteral("rxViewRight"));

    // tab sensor

    btGroupSensorDirection->setExclusive(true);
    btGroupSensorDirection->addButton(btnSensorDirection1, 1);
    btGroupSensorDirection->addButton(btnSensorDirection2, 2);
    btGroupSensorDirection->addButton(btnSensorDirection3, 3);
    btGroupSensorDirection->addButton(btnSensorDirection4, 4);
    btGroupSensorDirection->addButton(btnSensorDirection5, 5);
    btGroupSensorDirection->addButton(btnSensorDirection6, 6);
    btGroupSensorDirection->addButton(btnSensorDirection7, 7);
    btGroupSensorDirection->addButton(btnSensorDirection8, 8);
    btGroupSensorDirection->setLastButtonChecked(btnSensorDirection1);

    btnSensorDirection1->setChecked(true);

    // tab swash

    btGroupSwashplate->setExclusive(true);
    btGroupSwashplate->addButton(btnSwashplate1, Fbl::Hr3);
    btGroupSwashplate->addButton(btnSwashplate2, Fbl::H3);
    btGroupSwashplate->addButton(btnSwashplate3, Fbl::H1);
    btGroupSwashplate->addButton(btnSwashplate4, Fbl::H4);
    btGroupSwashplate->addButton(btnSwashplate5, Fbl::_135deg);
    btGroupSwashplate->setLastButtonChecked(btnSwashplate1);

    btGroupRotateDirection->setExclusive(true);
    btGroupRotateDirection->addButton(btnRotateDirection1, 1);
    btGroupRotateDirection->addButton(btnRotateDirection2, 2);
    btGroupRotateDirection->setLastButtonChecked(btnRotateDirection1);

    btnSwashplate1->setChecked(true);
    btnRotateDirection1->setChecked(true);

    swashView->move(btnSwashView->x(), btnSwashView->y());
    swashView->setAngle(0);

    // tab servos

    sliderServoCh1->setSliderRange(TRIM_MIN, TRIM_MAX);
    sliderServoCh2->setSliderRange(TRIM_MIN, TRIM_MAX);
    sliderServoCh3->setSliderRange(TRIM_MIN, TRIM_MAX);
    sliderServoCh4->setSliderRange(TRIM_MIN, TRIM_MAX);

    cbDigtalAnalogServos->addItem("Digital Servos", 1);
    cbDigtalAnalogServos->addItem("Analog Servos", 2);

    servoView->move(btnServosView->x(), btnServosView->y());

    trimSwashCyclic->setGeometry(btnTrimSwashCyclic->geometry());
    trimSwashCyclic->setTrimRange(TRIM_MIN, TRIM_MAX);
    trimSwashCyclic->setTrimMonitorRange(1120, 1920);

    editCyclicRing->setValidator(new QIntValidator(0, 100));

    // tab collective

    btGroupCollectDirection->setExclusive(true);
    btGroupCollectDirection->addButton(btnCollectiveDirection1, 1);
    btGroupCollectDirection->addButton(btnCollectiveDirection2, 2);
    btGroupCollectDirection->setLastButtonChecked(btnCollectiveDirection1);

    btnCollectiveDirection1->setChecked(true);

    editCollectiveTravel->setValidator(new QIntValidator(50, 150));

    trimSwashCollective->setGeometry(btnTrimSwashCollective->geometry());
    trimSwashCollective->setTrimRange(TRIM_MIN, TRIM_MAX);
    trimSwashCollective->setTrimMonitorRange(1120, 1920);

    editPitchpump->setValidator(new QIntValidator(0, 100));

    // tab cyclic

    editCyclicLimit->setValidator(new QIntValidator(50, 150));
    editCyclicGain->setValidator(new QIntValidator(50, 150));
    editCyclicResponse->setValidator(new QIntValidator(50, 150));
    editCyclicFlip->setValidator(new QIntValidator(150, 350));
    editCyclicRoll->setValidator(new QIntValidator(150, 350));

    editRcDeadband->setValidator(new QIntValidator(0, 25));
    editElevatorPrecom->setValidator(new QIntValidator(0, 100));
    editPddleSimulate->setValidator(new QIntValidator(0, 100));
    editCyclicExpo->setValidator(new QIntValidator(0, 100));

    // tab tail

    btGroupRadioTail->setExclusive(true);
    btGroupRadioTail->addButton(radioTail1, 1);
    btGroupRadioTail->addButton(radioTail2, 2);
    btGroupRadioTail->addButton(radioTail3, 3);

    sliderTailServo->setSliderRange(TRIM_MIN, TRIM_MAX);
    sliderTailServo->move(btnTailServo->x() + btnTailServo->width(),
                           btnTailServo->y() - 20);

    editTailTravel1->setValidator(new QIntValidator(50, 150));
    editTailTravel2->setValidator(new QIntValidator(50, 150));

    editTailGain->setValidator(new QIntValidator(50, 150));
    editTailYaw->setValidator(new QIntValidator(300, 700));

    editTailAccelerate->setValidator(new QIntValidator(0, 100));
    editTailStopGain->setValidator(new QIntValidator(0, 100));
    editTailExpo->setValidator(new QIntValidator(0, 100));
    editTailCollective->setValidator(new QIntValidator(0, 100));
    editTailCyclic->setValidator(new QIntValidator(0, 100));
    editTailZeroCol->setValidator(new QIntValidator(0, 100));

    // tab governor

    cbGovernorMode->addItem("Disabled", 0);
    cbGovernorMode->addItem("Electric", 1);
    cbGovernorMode->addItem("Nitro", 2);
    cbGovernorMode->setCurrentIndex(1);

    cbGasServosType->addItem("Digital Servo", 1);
    cbGasServosType->addItem("Analog Servo", 2);
    cbGasServosType->hide();
    checkThrottleServosReverse->hide();

    editGovernorMotorOff->setValidator(new QIntValidator(-150, -50));
    editGovernorMaxThrottle->setValidator(new QIntValidator(50, 150));

    editMaxHeadspeed->setValidator(new QIntValidator(1000, 5000));
    QDoubleValidator *validator = new QDoubleValidator(0.0, 99.99, 2);
    validator->setNotation(QDoubleValidator::StandardNotation);
    editGearRatio->setValidator(validator);
    editSensorConfig->setValidator(new QIntValidator(0, 100));


#define LOAD_HTML_TO_TEXTBROWSERS
    txtBrowserStart->setHtml(qLoadHtmlString(url_start_guide));
    txtBrowserRx->setHtml(qLoadHtmlString(url_rx_guide));
    txtBrowserSensor->setHtml(qLoadHtmlString(url_sensor_guide));
    txtBrowserSwash->setHtml(qLoadHtmlString(url_swash_guide));
    txtBrowserServos->setHtml(qLoadHtmlString(url_servos_guide));
    txtBrowserCollective->setHtml(qLoadHtmlString(url_collective_guide));
    txtBrowserCyclic->setHtml(qLoadHtmlString(url_cyclic_guide));
    txtBrowserTail->setHtml(qLoadHtmlString(url_tail_guide));
    txtBrowserGov->setHtml(qLoadHtmlString(url_governor_guide));
}

void Ui_MainWidget::setToolTipForWidgets()
{
    checkGyroOff->setToolTip(Res(2000));
    cbProfile->setToolTip(Res(2001));
    btnOpen->setToolTip(Res(2002));
    btnSave->setToolTip(Res(2003));
    btnFirmware->setToolTip(Res(2004));
    btnInfo->setToolTip(Res(2005));
    btnSaveProfileAs->setToolTip(Res(2006));

    btnBind->setToolTip(Res(2200));
    btnSetCenter->setToolTip(Res(2201));
    cbTxMode->setToolTip(Res(2202));

    btnSensorDirection1->setToolTip(Res(130));
    btnSensorDirection2->setToolTip(Res(131));
    btnSensorDirection3->setToolTip(Res(132));
    btnSensorDirection4->setToolTip(Res(133));
    btnSensorDirection5->setToolTip(Res(134));
    btnSensorDirection6->setToolTip(Res(135));
    btnSensorDirection7->setToolTip(Res(136));
    btnSensorDirection8->setToolTip(Res(137));

    btnSwashplate1->setToolTip(Res(140));
    btnSwashplate2->setToolTip(Res(141));
    btnSwashplate3->setToolTip(Res(142));
    btnSwashplate4->setToolTip(Res(143));
    btnSwashplate5->setToolTip(Res(144));

    btnRotateDirection1->setToolTip(Res(146));
    btnRotateDirection2 ->setToolTip(Res(147));

    editCyclicRing->setToolTip(Res(2400));

    btnServoCh1->setToolTip(btnServoCh1->isChecked() ? Res(151) : Res(150));
    btnServoCh2->setToolTip(btnServoCh2->isChecked() ? Res(151) : Res(150));
    btnServoCh3->setToolTip(btnServoCh3->isChecked() ? Res(151) : Res(150));
    btnServoCh4->setToolTip(btnServoCh4->isChecked() ? Res(151) : Res(150));

    sliderServoCh1->setToolTip(Res(2500));
    sliderServoCh2->setToolTip(Res(2500));
    sliderServoCh3->setToolTip(Res(2500));
    sliderServoCh4->setToolTip(Res(2500));

    trimSwashCyclic->setToolTipForClearButton(Res(2501));

    btnCollectiveDirection1->setToolTip(Res(160));
    btnCollectiveDirection2->setToolTip(Res(161));

    sliderCollectiveTravel->setToolTip(Res(2600));
    editCollectiveTravel->setToolTip(Res(2600));

    trimSwashCollective->setToolTip(Res(2501));

    editPitchpump->setToolTip(Res(2601));

    sliderCyclicFlip->setToolTip(Res(2704));
    sliderCyclicGain->setToolTip(Res(2702));
    sliderCyclicLimit->setToolTip(Res(2700));
    sliderCyclicReponse->setToolTip(Res(2701));
    sliderCyclicRoll->setToolTip(Res(2703));

    editCyclicFlip->setToolTip(Res(2704));
    editCyclicGain->setToolTip(Res(2702));
    editCyclicLimit->setToolTip(Res(2700));
    editCyclicResponse->setToolTip(Res(2701));
    editCyclicRoll->setToolTip(Res(2703));

    editRcDeadband->setToolTip(Res(2705));
    editElevatorPrecom->setToolTip(Res(2706));
    editPddleSimulate->setToolTip(Res(2707));
    editCyclicExpo->setToolTip(Res(2708));

    btnCyclicDefault->setToolTip(Res(170));
    btnCyclicDefaultAdvance->setToolTip(Res(170));

    btnTailServo->setToolTip(btnTailServo->isChecked() ? Res(181) : Res(180));
    sliderTailServo->setToolTip(Res(2800));

    sliderTailTravel1->setToolTip(Res(2801));
    sliderTailTravel2->setToolTip(Res(2801));
    editTailTravel1->setToolTip(Res(2801));
    editTailTravel2->setToolTip(Res(2801));

    sliderTailYaw->setToolTip(Res(2802));
    editTailYaw->setToolTip(Res(2802));

    sliderTailGain->setToolTip(Res(2803));
    editTailGain->setToolTip(Res(2803));

    editTailAccelerate->setToolTip(Res(2804));
    editTailStopGain->setToolTip(Res(2805));
    editTailExpo->setToolTip(Res(2806));
    editTailCollective->setToolTip(Res(2807));
    editTailCyclic->setToolTip(Res(2808));
    editTailZeroCol->setToolTip(Res(2809));

    btnTailDefault->setToolTip(Res(183));

    sliderGovernorMotorOff->setToolTip(Res(2900));
    editGovernorMotorOff->setToolTip(Res(2900));

    sliderGovernoMaxThrottle->setToolTip(Res(2901));
    editGovernorMaxThrottle->setToolTip(Res(2901));

    editMaxHeadspeed->setToolTip(Res(2902));
    editGearRatio->setToolTip(Res(2903));
    editSensorConfig->setToolTip(Res(2904));

    btnSwashAdvance->setToolTip(
                btnSwashAdvance->isChecked() ? Res(14) : Res(13));
    btnServosAdvance->setToolTip(
                btnServosAdvance->isChecked() ? Res(14) : Res(13));
    btnCollectiveAdvance->setToolTip(
                btnCollectiveAdvance->isChecked() ? Res(14) : Res(13));
    btnCyclicAdvance->setToolTip(
                btnCyclicAdvance->isChecked() ? Res(14) : Res(13));
    btnTailAdvance->setToolTip(
                btnTailAdvance->isChecked() ? Res(14) : Res(13));
}

void Ui_MainWidget::animationWhenTabChange(int tabIndex)
{
    QWidget *widget = NULL;
    switch(tabIndex)
    {
    case Fbl::TabStart:
        widget = frameStart;
        break;
    case Fbl::TabRx:
        widget = frameRx;
        break;
    case Fbl::TabSensor:
        widget = frameSensor;
        break;
    case Fbl::TabSwash:
        widget = frameSwash;
        break;
    case Fbl::TabServo:
        widget = frameServos;
        break;
    case Fbl::TabCollective:
        widget = frameCollective;
        break;
    case Fbl::TabCyclic:
        widget = frameCyclic;
        break;
    case Fbl::TabTail:
        widget = frameTail;
        break;
    case Fbl::TabGov:
        widget = frameGovernor;
        break;
    default:
        return;
    }

    QPropertyAnimation *anim = new QPropertyAnimation(widget, "pos");

    anim->setKeyValueAt(0, QPointF(widget->x() + 20, widget->y()));
    anim->setKeyValueAt(0.8, QPointF(widget->x() - 5, widget->y()));
    anim->setKeyValueAt(1, widget->pos());
    anim->setDuration(ANIMATION_DURATION_WHEN_CHANGE_TAB);

    anim->start(QAbstractAnimation::DeleteWhenStopped);
}

void Ui_MainWidget::animationWhenShowAdvance(QStackedWidget *stackedWidget, bool checked)
{
    int u = checked ? 20 : -20;
    int v = checked ? -5 : 5;

    QPropertyAnimation *anim = new QPropertyAnimation(stackedWidget, "pos");

    anim->setKeyValueAt(0, QPointF(30 + u, stackedWidget->y()));
    anim->setKeyValueAt(0.8, QPointF(30 + v, stackedWidget->y()));
    anim->setKeyValueAt(1, QPointF(30, stackedWidget->y()));
    anim->setDuration(DURATION_ANIMATION);

    anim->start(QAbstractAnimation::DeleteWhenStopped);
}

void Ui_MainWidget::animationWhenLoadFirmwareParams(bool isAnimToShow)
{
    if (!isAnimToShow) {
        if (!frameWriteLoadedParams->isVisible()) {
            return;
        }

        QPropertyAnimation *anim = new QPropertyAnimation(frameWriteLoadedParams, "pos");
        anim->setDuration(DURATION_ANIMATION);
        anim->setStartValue(frameWriteLoadedParams->pos());
        anim->setEndValue(QPointF(frameWriteLoadedParams->x(), height()));
        anim->setDuration(DURATION_ANIMATION);

        connect(anim, SIGNAL(finished()), frameWriteLoadedParams, SLOT(hide()));
        connect(anim, SIGNAL(finished()), this, SLOT(animationShowGyroOff()));

        anim->start(QAbstractAnimation::DeleteWhenStopped);
    }
    else {
        if (frameWriteLoadedParams->isVisible()) {
            return;
        }

        QSequentialAnimationGroup *group = new QSequentialAnimationGroup;

        if (checkGyroOff->isVisible()) {
            QPropertyAnimation *anim = new QPropertyAnimation(checkGyroOff, "pos");
            anim->setStartValue(checkGyroOff->pos());
            anim->setEndValue(QPointF(checkGyroOff->x(), height()));
            anim->setDuration(DURATION_ANIMATION);

            connect(anim, SIGNAL(finished()), checkGyroOff, SLOT(hide()));
            group->addAnimation(anim);
        }

        frameWriteLoadedParams->show();

        QPropertyAnimation *anim = new QPropertyAnimation(frameWriteLoadedParams, "pos");
        anim->setStartValue(frameWriteLoadedParams->pos());
        anim->setEndValue(QPointF(frameWriteLoadedParams->x(),
                frameWriteLoadedParams->y() - frameWriteLoadedParams->height()));
        anim->setDuration(DURATION_ANIMATION);
        group->addAnimation(anim);

        group->start(QAbstractAnimation::DeleteWhenStopped);
    }
}

void Ui_MainWidget::animationWhenBindRx()
{
    int rxType = cbReceiverType->currentData().toInt();
    if (rxType == Fbl::Satellite) {

        if (btnBind->isVisible()) {
            return;
        }

        btnBind->show();
        cbSatelliteType->show();

        QPropertyAnimation *animBtt = new QPropertyAnimation(btnBind, "pos");
        animBtt->setStartValue(btnBind->pos());
        animBtt->setEndValue(QPoint(btnBind->x(), cbReceiverType->y() + 40));
        animBtt->setDuration(DURATION_ANIMATION);

        QPropertyAnimation *animCbbSatel =
                new QPropertyAnimation(cbSatelliteType, "geometry");
        animCbbSatel->setStartValue(
            QRect(cbReceiverType->x() + cbReceiverType->width()/2 - 3,
                  cbReceiverType->y() + 40 + cbReceiverType->height(), 5, 1));
        animCbbSatel->setEndValue(
            QRect(cbReceiverType->x(), cbReceiverType->y() + 40,
                  cbReceiverType->width() - btnBind->width() - 10,
                  cbReceiverType->height()));
        animCbbSatel->setDuration(DURATION_ANIMATION);

        QParallelAnimationGroup *group = new QParallelAnimationGroup;
        group->addAnimation(animBtt);
        group->addAnimation(animCbbSatel);

        group->start(QAbstractAnimation::DeleteWhenStopped);
    }
    else {
        if (! btnBind->isVisible()) {
            return;
        }

        QPropertyAnimation *animBtt = new QPropertyAnimation(btnBind, "pos");
        animBtt->setStartValue(btnBind->pos());
        animBtt->setEndValue(QPoint(btnBind->x(), - btnBind->height()));
        animBtt->setDuration(DURATION_ANIMATION);

        QPropertyAnimation *animCbbSatel =
                new QPropertyAnimation(cbSatelliteType, "geometry");
        animCbbSatel->setStartValue(cbSatelliteType->geometry());
        animCbbSatel->setEndValue(
            QRect(cbReceiverType->x() + cbReceiverType->width()/2 - 3,
                  cbReceiverType->y() + 40 + cbReceiverType->height(), 5, 1));
        animCbbSatel->setDuration(DURATION_ANIMATION);

        QParallelAnimationGroup *group = new QParallelAnimationGroup;
        group->addAnimation(animBtt);
        group->addAnimation(animCbbSatel);

        connect(group, SIGNAL(finished()), btnBind, SLOT(hide()));
        connect(group, SIGNAL(finished()), cbSatelliteType, SLOT(hide()));

        group->start(QAbstractAnimation::DeleteWhenStopped);
    }
}

void Ui_MainWidget::animationShowGyroOff()
{
    QPropertyAnimation *anim = new QPropertyAnimation(checkGyroOff, "pos");
    anim->setDuration(DURATION_ANIMATION);

    switch (mainTab->currentIndex()) {
    case Fbl::TabSwash:
    case Fbl::TabCyclic:
    case Fbl::TabServo:
    case Fbl::TabTail:
    case Fbl::TabCollective:
        if (!checkGyroOff->isVisible() && !frameWriteLoadedParams->isVisible()) {
            checkGyroOff->show();
            anim->setStartValue(checkGyroOff->pos());
            anim->setEndValue(QPointF(checkGyroOff->x(),
                    checkGyroOff->y() - checkGyroOff->height()));
            anim->start(QAbstractAnimation::DeleteWhenStopped);
        }
        break;

    default:
        if (checkGyroOff->isVisible()) {
            connect(anim, SIGNAL(finished()), checkGyroOff, SLOT(hide()));

            anim->setStartValue(checkGyroOff->pos());
            anim->setEndValue(QPointF(checkGyroOff->x(), height()));
            anim->start(QAbstractAnimation::DeleteWhenStopped);
        }
    }
}

QList<int> Ui_MainWidget::getListChannelAngleBySwashType(int swashType)
{
    QList<int> listAngle;

    switch (swashType) {
    case Fbl::Hr3:
        listAngle << 300 << 180 << 60 << -1;
        break;
    case Fbl::H3:
        listAngle << 240 << 0 << 120 << -1;
        break;
    case Fbl::H1:
        listAngle << 225 << 0 << 90 << -1;
        break;
    case Fbl::H4:
        listAngle << 270 << 0 << 90 << 180;
        break;
    case Fbl::_135deg:
        listAngle << -1 << -1 << -1 << -1;
        break;
    }

    return listAngle;
}

void Ui_MainWidget::updateWidgetsPositionOnTabServos(int swashType)
{
    QPoint *pCh1 = NULL;
    QPoint *pCh2 = NULL;
    QPoint *pCh3 = NULL;
    QPoint *pCh4 = NULL;

    QList<int> listAngle = getListChannelAngleBySwashType(swashType);

    if (swashType == Fbl::Hr3) {
        pCh1 = new QPoint(10, 40);
        pCh2 = new QPoint(150, 300);
        pCh3 = new QPoint(290, 40);
    }
    else if (swashType == Fbl::H3) {
        pCh1 = new QPoint(10, 300);
        pCh2 = new QPoint(150, 40);
        pCh3 = new QPoint(290, 300);
    }
    else if (swashType == Fbl::H1) {
        pCh1 = new QPoint(10, 300);
        pCh2 = new QPoint(150, 40);
        pCh3 = new QPoint(290, 180);
    }
    else if (swashType == Fbl::H4) {
        pCh1 = new QPoint(10, 180);
        pCh2 = new QPoint(150, 30);
        pCh3 = new QPoint(150, 330);
        pCh4 = new QPoint(310, 180);
    }
    else return;

    updateRelativePositionOnTabServos(*pCh1, btnServoCh1, sliderServoCh1);
    updateRelativePositionOnTabServos(*pCh2, btnServoCh2, sliderServoCh2);
    updateRelativePositionOnTabServos(*pCh3, btnServoCh3, sliderServoCh3);

    delete pCh1;
    delete pCh2;
    delete pCh3;

    if (pCh4 != NULL) {
        btnServoCh4->show();
        sliderServoCh4->show();

        updateRelativePositionOnTabServos(*pCh4, btnServoCh4, sliderServoCh4);
        delete pCh4;
    }
    else {
        btnServoCh4->hide();
        sliderServoCh4->hide();
    }

    swashView->resetChannelDefaultAngle(listAngle);
    servoView->resetChannelDefaultAngle(listAngle);
}

void Ui_MainWidget::updateRelativePositionOnTabServos(
        const QPoint& point, QAbstractButton *button, DoubleDirectSlider *sliderTrim)
{
    button->move(point);
    sliderTrim->move(point.x() + button->width(), point.y() - 20);
}

void Ui_MainWidget::updateLabelPortStatus(bool isConnected)
{
    if (isConnected) {
        lbPortStatus->setStyleSheet(style_label_connected);
    }
    else {
        lbPortStatus->setStyleSheet(style_label_disconnected);
    }
}

void Ui_MainWidget::updateLabelFlashStatus(const QString& text)
{
    progressDialog->setContent(text);
}

void Ui_MainWidget::changeColorWhenSliderExceedIdealValue(QSlider *slider)
{
    int value = slider->value();
    if (value < SLIDER_IDEAL_MIN || value > SLIDER_IDEAL_MAX) {
        slider->setStyleSheet(style_slider_outside_ideal_value);
    } else {
        slider->setStyleSheet("");
    }
}

void Ui_MainWidget::setLineEditText(QLineEdit *edit, QString text)
{
    if (edit->text() != text) {
        edit->setText(text);
    }
}

void Ui_MainWidget::showLineEditAlert(const QPoint &p)
{
    lbLineEditAlert->move(p);
    lbLineEditAlert->showMe();
}

void Ui_MainWidget::hideLineEditAlert()
{
    lbLineEditAlert->hideMe();
}

void Ui_MainWidget::showProgressDialog(ProgressDialog::Type state,
                                        QString content)
{
    progressDialog->show();
    progressDialog->setContent(content);
    progressDialog->changeState(state);
}

void Ui_MainWidget::hideProgressDialog()
{
    progressDialog->hide();
}

void Ui_MainWidget::comboBox_currentIndexChanged(int index)
{
    QComboBox *cb = qobject_cast<QComboBox*> (sender());
    if (cb == cbReceiverType) {
        animationWhenBindRx();
    }
    else if (cb == cbGovernorMode) {
        if (index == Fbl::Disabled) {
            foreach (QObject* obj, frameGovernor->children()) {
                QWidget *widget = qobject_cast<QWidget*> (obj);
                if (widget != NULL && widget != cbGovernorMode
                        && widget != lbGovernorMode) {
                    widget->hide();
                }
            }
        } else {
            foreach (QObject* obj, frameGovernor->children()) {
                QWidget *widget = qobject_cast<QWidget*> (obj);
                if (widget != NULL) {
                    widget->show();
                }
            }
        }

        if (index == Fbl::Nitro) {
            cbGasServosType->show();
            checkThrottleServosReverse->show();

            editSensorConfig->hide();
            lbSensorConfig->hide();
            checkIdleDuringBailout->hide();
        }
        else if (index == Fbl::Electric) {
            editSensorConfig->show();
            lbSensorConfig->show();
            checkIdleDuringBailout->show();

            cbGasServosType->hide();
            checkThrottleServosReverse->hide();
        }
    }
}


void Ui_MainWidget::btnChangeAdvanceNormalMode_clicked(bool checked)
{
    QAbstractButton *btt = qobject_cast<QAbstractButton*> (sender());
    QStackedWidget *stack = NULL;

    int id = (checked) ? 1 : 0;

    if (btt == btnSwashAdvance) {
        stack = stackedSwash;
        stackedServos->setCurrentIndex(id);
        btnServosAdvance->setChecked(checked);
    }
    else if (btt == btnServosAdvance) {
        stack = stackedServos;
        stackedSwash->setCurrentIndex(id);
        btnSwashAdvance->setChecked(checked);
    }
    else if (btt == btnCollectiveAdvance) {
        stack = stackedCollective;
    }
    else if (btt == btnCyclicAdvance) {
        stack = stackedCyclic;
    }
    else if (btt == btnTailAdvance) {
        stack = stackedTail;
    }
    else return;

    stack->setCurrentIndex(id);
    animationWhenShowAdvance(stack, checked);
}

void Ui_MainWidget::btnChangeAdvanceNormalMode_toggled(bool check)
{
    qobject_cast<QAbstractButton*> (sender())->setToolTip(
                check ? Res(14) : Res(13));
}

void Ui_MainWidget::buttonGroup_button_toggled(int id, bool checked)
{
    if (checked && sender() == btGroupSwashplate) {
        updateWidgetsPositionOnTabServos(id);
    }
}

void Ui_MainWidget::servoButtons_toggled(bool check)
{
    qobject_cast<QAbstractButton*> (sender())->setToolTip(
                check ? Res(151) : Res(150));
}

void Ui_MainWidget::slider_valueChanged(int value)
{
    QSlider *slider = qobject_cast<QSlider*> (sender());

    if (slider == sliderRx1) {
        lbSlider1_ms->setText(QString::number(value) + " µs");
    }
    else if (slider == sliderRx2) {
        lbSlider2_ms->setText(QString::number(value) + " µs");
    }
    else if (slider == sliderRx3) {
        lbSlider3_ms->setText(QString::number(value) + " µs");
    }
    else if (slider == sliderCollectiveTravel) {
        changeColorWhenSliderExceedIdealValue(slider);
    }
    else if (slider == sliderCyclicLimit) {
        changeColorWhenSliderExceedIdealValue(slider);
    }
    else if (slider == sliderTailTravel1) {
        changeColorWhenSliderExceedIdealValue(slider);
    }
    else if (slider == sliderTailTravel2) {
        changeColorWhenSliderExceedIdealValue(slider);
    }
}

void Ui_MainWidget::lineEdit_textEdited(const QString &)
{
    QLineEdit *edit = qobject_cast<QLineEdit*> (sender());

    QPoint point(-120, edit->height() + 2);
    showLineEditAlert(edit->mapTo(this, point));
}

void Ui_MainWidget::rxView_sensor_value_changed(qint32 x, qint32 y)
{
    QString xStr, yStr;

    if (x < RX_MIN_VAL || x > RX_MAX_VAL) {
        xStr = "∞";
    }
    else {
        xStr = QString::number(x - RX_CENTER_VAL);
    }

    if (y < RX_MIN_VAL || y > RX_MAX_VAL) {
        yStr = "∞";
    }
    else {
        yStr = QString::number(y - RX_CENTER_VAL);
    }

    RxMonitorView *view = qobject_cast<RxMonitorView*> (sender());
    if (view->getLocation() == RX_LOCATION_LEFT) {
        lbRxLeft_x->setText(xStr);
        lbRxLeft_y->setText(yStr);
    }
    else if (view->getLocation() == RX_LOCATION_RIGHT) {
        lbRxRight_x->setText(xStr);
        lbRxRight_y->setText(yStr);
    }
}

void Ui_MainWidget::swashView_angle_changed(const QList<int>& list)
{
    lbAngleCh1->setText(QString::number(list.at(1)));
    lbAngleCh2->setText(QString::number(list.at(2)));
    lbAngleCh3->setText(QString::number(list.at(3)));
    lbAngleCh4->setText(QString::number(list.at(4)));
}

void Ui_MainWidget::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Escape) {
        QApplication::focusWidget()->clearFocus();
    }
}
