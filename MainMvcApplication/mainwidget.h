﻿#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QTimer>
#include <QHash>
#include <QSet>

#define GYRO_OFF_DELAY 4
#define BIND_RX_DELAY 20
#define SET_CENTRE_DELAY 20
#define TAB_COUNTER_DELAY 20

#define MAX_ERROR 3

#define TIMER_UPDATE_UI 50
#define COUNTER_MAX_VALUE 200

#define MAX_TIME_WAIT_THREAD_DESTROY 1000

#define LOAD_PARAMS_LEVEL_ALL 2
#define LOAD_PARAMS_LEVEL_PROFILE 1


class SerialManager;
class AppConfigManager;
class ResourceManager;
namespace Ui {
    class MainWidget;
}

class MainWidget : public QObject
{
    Q_OBJECT
    Q_ENUMS(Action)

public:
    enum Action
    {
        Sleep = 0,
        Running = 1
    };

    enum AppRunMode
    {
        Online = 1,
        Offline = 0
    };

    explicit MainWidget(AppConfigManager* config);
    ~MainWidget();

signals:
    void _needUpdateFlash(const QByteArray&);
    void _needReconnect();

public slots:
    void show();

    void handleParamsChanged(const QVariant&);
    void handlePortStatusChanged(int);

    void handleFlashStatusChanged(int);
    void handleFlashProgressChanged(int);

protected:
    bool eventFilter(QObject *obj, QEvent *event);

    void onConnect();
    void onDisconnect();

private slots:

    // general slots

    void sliderPressed();
    void sliderReleased();
    void slider_action_triggered(int);
    void slider_value_changed(int);

    void lineEdit_text_edited(const QString&);
    void lineEdit_return_pressed();

    void buttonGroup_button_clicked(int);

    void checkableToolButton_clicked(bool);

    // tab rx slots

    void cbTxMode_current_index_changed(int);

    void cbReceiverType_activated(int);

    void rxView_sensor_value_changed(qint32, qint32);

    void btnSetCenter_clicked();

    void btnBind_clicked();

    // tab swash slots

    void swashView_angle_changed(const QList<int>&);

    void btnSwashViewRotateLeft_clicked();
    void btnSwashViewRotateRight_clicked();

    // tab servos slots

    void trimSwashCyclic_value_changed(int, int, int, bool);

    // tab collective slots

    void trimSwashCollective_value_changed(int);

    // tab cyclic slots

    void btnCyclicDefault_button_clicked();

    // tab tail slots

    void btnTailDefault_button_clicked();

    // global slots

    void mainTab_current_changed(int);

    void btnOpen_button_clicked();
    void btnSave_button_clicked();
    void btnFirmware_button_clicked();
    void btnInfo_button_clicked();

    void btnWriteLoadedParams_button_clicked();

    void cbProfile_activated(int);

    void requestReadAllAddress();
    void requestReadValuesOnTab();

    void requestWriteConfigChanges();
    void requestWriteConfigChanges(QObject *objToWrite, int key = -1,
                                   int val = -1, bool isBlocking = false);
    void requestWriteConfigChanges(QObject *objToWrite, bool isBlocking);

    void requestSendGyroOff();

private:
    void registerConnections();
    void intializeWidgetHashValues();

    void resetTabCounter();
    void updateTabCounter(int index);

    void changeAction(Action action);

    void updateSoftware();

    void loadApplicationConfig();
    void saveApplicationConfig();

    bool loadProfile(int profileIndex);
    bool saveProfile(int profileIndex);

    int findKeyByObject(void *);
    bool isStableWidget(QObject* obj, int addr);
    static void textToDouble(QString, int& left, int& right);
    void updateParamsCache(qint32 key, qint32 value);

    Ui::MainWidget *ui;

    AppConfigManager *m_appConfig;
    ResourceManager *m_res;
    SerialManager* m_serialManager;

    Action m_currentAction;

    QTimer m_updateUiTimer;
    qint32 m_updateUiCounter;
    qint32* m_tabUpdateCounter;

    qint32 m_errorCounter;

    QSet<QObject*> m_stableWidgets;
    QSet<qint32> m_waitToWriteKeys;

    QHash<qint32, QObject*> m_widgetIdHash;
    QHash<qint32, qint32> m_fblParamsCache;

    QString m_tmpLineEditText;

    int m_currentProfile;

    int m_LoadProfileLevel;
    AppRunMode m_applicationMode;
};

#endif // MAINWIDGET_H
