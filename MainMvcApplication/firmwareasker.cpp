#include "firmwareasker.h"
#include <QStringList>

#define MAX_TAB_COUNT 9
#define UPDATE_COUNTER_DELAY 20

static const QString FBL_ADDR_LIST[MAX_TAB_COUNT] =
{
    ":0121,0122",
    "0111,0112,0113,0114,0115,0116,0117:0101",
    ":0127",
    ":0203,0124,0209,0210,0211",
    "0217,0218,0219,0220:0204,0205,0206,0207,0132,0133,0134,0131,0125,0222,0223",
    "0217,0218,0219,0220:0136,0137,0138,0139,0140",
    ":0301,0302,0300,0305,0306,0310,0311,0312,0313",
    ":0126,0135,0207,0212,0216,0303,0304,0314,0315,0316,0317,0318,0319",
    ":0511,0512,0520,0521,0522,0523,0524,0525,0526,0527,0528,0529"
};

FirmwareAsker::FirmwareAsker()
{
    m_updateCounters = new int[MAX_TAB_COUNT];
    resetCounters();
}

FirmwareAsker::~FirmwareAsker()
{
    delete[] m_updateCounters;
}

QString FirmwareAsker::getAddressNeedUpdate(int index)
{
    QString tmp = FBL_ADDR_LIST[index];
    updateCounters(index);

    if (m_updateCounters[index] % UPDATE_COUNTER_DELAY == 0) {
        m_requestErrorCounter++;
        return tmp.replace(":", ",");
    } else {
        return tmp.split(":").first();
    }
}

QString FirmwareAsker::getAllAddress()
{
    QString text;
    for (int i = 0; i < MAX_TAB_COUNT; i++) {
        text.append(FBL_ADDR_LIST[i]).append(",");
    }

    return text.replace(":", ",");
}

void FirmwareAsker::resetCounters()
{
    for (int i = 0; i < MAX_TAB_COUNT; i++) {
        m_updateCounters[i] = -1;
    }
}

int FirmwareAsker::totalRequestError() const
{
    return m_requestErrorCounter;
}

void FirmwareAsker::setRequestAccepted()
{
    if (m_requestErrorCounter > 0)  {
        m_requestErrorCounter--;
    }
}

void FirmwareAsker::resetErrorCounter()
{
    m_requestErrorCounter = 0;
}

void FirmwareAsker::updateCounters(int index)
{
    for (qint8 i = 0; i < MAX_TAB_COUNT; i++) {
        if (i == index) {
            m_updateCounters[index] += 1;
            if (m_updateCounters[index] >= 200) {
                m_updateCounters[index] = 1;
            }
            continue;
        }
        m_updateCounters[i] = -1;
    }
}
