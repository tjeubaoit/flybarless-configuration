#ifndef FIRMWARECONTROLLER_H
#define FIRMWARECONTROLLER_H

#include <QObject>
#include <QEventLoop>
#include <QThread>

class ResourceManager;
class SerialManager;
namespace Ui { class MainWidget; }

class FirmwareController : public QObject
{
    Q_OBJECT
public:
    explicit FirmwareController(Ui::MainWidget* ui,
                                QObject *parent = 0);

signals:
    void _flashUpdateFinish();

public slots:
     void startUpdateFirmware(SerialManager* serialManager,
                                     const QByteArray&);

    void handleFlashStatusChanged(int);
    void handleFlashProgressChanged(int);

    void flashUpdateFinish();

private:
    ResourceManager* m_res;
    QEventLoop m_eventLoop;
    QThread m_thread;
    Ui::MainWidget* ui;

};

#endif // FIRMWARECONTROLLER_H
