#include "maincontroller.h"
#include "mainwidget_ui.h"
#include "global.h"
#include "resources.h"
#include "address.h"
#include "serialmanager.h"
#include "filehelper.h"
#include "flashhelper.h"
#include "firmwaredialog.h"
#include "appconfigmanager.h"
#include "resourcemanager.h"
#include "modelbuilder.h"
#include "parametermodel.h"
#include "extraadaptors.h"
#include "infodialog.h"
#include "adaptormodel.h"
#include "slideradaptor.h"
#include "lineeditadaptor.h"
#include "sliderwithlineeditadaptor.h"

#include <QMessageBox>
#include <QToolTip>
#include <QFileDialog>
#include <QKeyEvent>
#include <QDebug>
#include <QMenu>

MainController::MainController(AppConfigManager *config)
    : ui(new Ui::MainWidget),
      m_appConfig(config),
      m_res(ResourceManager::instance()),
      m_serialManager(new SerialManager),
      m_currentAction(Sleep),
      m_appMode(Online),
      m_currentProfile(0),
      m_loadProfileLevel(0)
{
    this->registerConnections();
    this->intializeModels();
    this->setupUi();
    this->loadApplicationConfig();

    m_updateUiTimer.setSingleShot(false);
    m_updateUiTimer.setInterval(TIMER_UPDATE_UI);

    ui->mainTab->setCurrentIndex(Fbl::TabStart);
    ui->updateLabelPortStatus(false);

    m_serialManager->setHelloPairString(HELLO_REQUEST, HELLO_RESPONSE);
    m_serialManager->addProductId(PRODUCT_ID);
    m_serialManager->addVendorId(VENDOR_ID);
    m_serialManager->start(QThread::HighPriority);
}

MainController::~MainController()
{
    changeAction(Sleep);
    saveApplicationConfig();
    saveProfile(m_currentProfile);

    if (m_serialManager->isRunning()) {
        m_serialManager->quit();
        m_serialManager->wait(MAX_TIME_WAIT_THREAD_DESTROY);
    }
    m_serialManager->deleteLater();

    m_paramModel->deleteLater();
    m_adaptorModel->deleteLater();

    delete ui;
}

void MainController::showMainWindow()
{
    ui->show();
}

bool MainController::eventFilter(QObject*, QEvent* event)
{
    if (event->type() == QEvent::MouseButtonPress) {
        QWidget *widget =  QApplication::focusWidget();
        if (widget != 0) {
            widget->clearFocus();
        }
    }

    return false;
}

void MainController::onConnect()
{
    requestReadAllAddress();

    ui->updateLabelPortStatus(true);
    changeAction(Running);

    m_requestManager.resetErrorCounter();
}

void MainController::onDisconnect()
{
    ui->updateLabelPortStatus(false);
    changeAction(Sleep);
}

void MainController::handleParamsChanged(const QVariant& data)
{
    QStringList list = data.toString().split(',');
    qint32 addr = list.first().toInt();
    qint32 val = list.last().toInt();

    m_adaptorModel->setAdaptorValue(addr, val);
    m_paramModel->setParamValue(addr, val);
    m_requestManager.setRequestAccepted();
}

void MainController::handlePortStatusChanged(int status)
{
    switch (status) {
    case SerialManager::PortConnected:
        this->onConnect();
        break;

    case SerialManager::PortDisconneted:
        this->onDisconnect();
        break;
    }
}

void MainController::mainTimer_timeout()
{
    // check fbl device is connected before request send
    if (m_requestManager.totalRequestError() > MAX_ERROR ) {
        emit _needReconnect();
        return;
    }

    this->requestSendGyroOff();

    const int tabIndex = ui->mainTab->currentIndex();
    // check permisson send request read params by a counter
    if (m_updateUiCounter < 0) {
        m_updateUiCounter ++;
        return;
    }
    else if (tabIndex == Fbl::TabRx) {
        if (!ui->rxViewLeft->animFinished()
                || !ui->rxViewRight->animFinished()) {

            return;
        }
    }

    m_serialManager->readMultipleAddress(
            m_requestManager.getAddressNeedUpdate(tabIndex));
}

void MainController::requestReadAllAddress()
{
    m_serialManager->readMultipleAddress(m_requestManager.getAllAddress());
}

void MainController::requestWriteConfigChanges(int addr, int val,
                                               bool isBlocking)
{
    // save key/value pair to params model before write
    m_paramModel->setParamValue(addr, val);

    if (m_currentAction == Sleep) {
        return;
    }
    if (addr > 0 && val >= 0) {
        // clear all signals posted in queue of SerialWriter
        m_serialManager->clearAllSignalsInReader();

        QString text = QString::number(addr) + "," + QString::number(val);
        if (isBlocking) {
            m_serialManager->writeConfigBlocking(text);
        } else {
            m_serialManager->writeConfig(text);
        }
    }
}

void MainController::requestSendGyroOff()
{
    static int gyroOffCounter = 0;
    int tabIndex = ui->mainTab->currentIndex();

    // check send request gyro off
    if (ui->checkGyroOff->isChecked()
            && (gyroOffCounter % GYRO_OFF_DELAY == 0)) {

        if (tabIndex >= 3 && tabIndex <= 7)
            emit requestWriteConfigChanges(ADDR_CHECK_GYRO_OFF, 1);
    }

    if (++gyroOffCounter >= COUNTER_MAX_VALUE) {
        gyroOffCounter = 1;
    }
}

void MainController::mainTab_current_changed(int index)
{
    m_updateUiCounter = 0; // khi chuyen tab, reset bo dem ve 0

    // xoa cac signal chua thuc hien trong queue cua SerialWriter
    m_serialManager->clearAllSignalsInReader();

    ui->animationWhenTabChange(index);
    ui->animationShowGyroOff();
}

void MainController::buttonGroup_button_clicked(QObject* obj, int id,
                                                bool* accept)
{
    QButtonGroup* btGroup = qobject_cast<QButtonGroup*> (obj);
    QAbstractButton* button = btGroup->button(id);

    QString msgText;
    if (button == ui->btnSensorDirection1) msgText = Res(130);
    else if (button == ui->btnSensorDirection2) msgText = Res(131);
    else if (button == ui->btnSensorDirection3) msgText = Res(132);
    else if (button == ui->btnSensorDirection4) msgText = Res(133);
    else if (button == ui->btnSensorDirection5) msgText = Res(134);
    else if (button == ui->btnSensorDirection6) msgText = Res(135);
    else if (button == ui->btnSensorDirection7) msgText = Res(136);
    else if (button == ui->btnSensorDirection8) msgText = Res(137);

    else if (button == ui->btnSwashplate1) msgText = Res(140);
    else if (button == ui->btnSwashplate2) msgText = Res(141);
    else if (button == ui->btnSwashplate3) msgText = Res(142);
    else if (button == ui->btnSwashplate4) msgText = Res(143);
    else if (button == ui->btnSwashplate5) msgText = Res(144);

    else if (button == ui->btnRotateDirection1) msgText = Res(146);
    else if (button == ui->btnRotateDirection2) msgText = Res(147);

    else if (button == ui->btnCollectiveDirection1) msgText = Res(160);
    else if (button == ui->btnCollectiveDirection2) msgText = Res(161);

    QString msgTitle;
    if (btGroup == ui->btGroupSensorDirection) msgTitle = Res(138);
    else if (btGroup == ui->btGroupSwashplate) msgTitle = Res(145);
    else if (btGroup == ui->btGroupCollectDirection) msgTitle = Res(162);
    else if (btGroup == ui->btGroupRotateDirection) msgTitle = Res(148);

    int ret = QMessageBox::question(ui, msgTitle, msgText,
                                    QMessageBox::Ok, QMessageBox::Cancel);
    if (QMessageBox::Ok == ret) {
        if (btGroup == ui->btGroupSwashplate) {
            ui->swashView->setAngle(0);
            requestWriteConfigChanges(ADDR_SWASHPLATE_ANGLE, 0);
        }
        *accept = true;
    }
    else
        *accept = false;
}

void MainController::servoButtons_clicked(QObject* btt, bool check,
                                                 bool* accept)
{
    QString msgTitle = Res(152);
    QString msgText = (check) ? Res(150) : Res(151);
    if (btt == ui->btnTailServo) {
        msgTitle = Res(182);
        msgText = (check) ? Res(180) : Res(181);
    }

    int ret = QMessageBox::question(ui, msgTitle, msgText,
                                    QMessageBox::Ok, QMessageBox::Cancel);
    if (QMessageBox::Ok == ret)
        *accept = true;
    else
        *accept = false;
}

void MainController::btnSetCenter_clicked()
{
    requestWriteConfigChanges(ADDR_RX_SETCENTER, 1);
    m_updateUiCounter = -SET_CENTRE_DELAY;

    ui->showProgressDialog(ProgressDialog::RingType, Res(122));
    QTimer::singleShot(SET_CENTRE_DELAY * TIMER_UPDATE_UI,
                       ui, SLOT(hideProgressDialog()));
}

void MainController::btnBind_clicked()
{
    int ret = QMessageBox::warning(ui, Res(120), Res(121),
                             QMessageBox::Yes, QMessageBox::No);
    if (ret == QMessageBox::No) {
        return;
    }

    qint32 val = (Fbl::DSM2 == ui->cbSatelliteType->currentData().toInt())
                                ? 3 : 9;
    requestWriteConfigChanges(ADDR_RX_BIND, val);
    m_updateUiCounter = -BIND_RX_DELAY;
}

void MainController::btnSwashViewRotateLeft_clicked()
{
    ui->swashView->rotateLeft();
    int val = m_adaptorModel->getAdaptorValue(ADDR_SWASHPLATE_ANGLE);
    requestWriteConfigChanges(ADDR_SWASHPLATE_ANGLE, val);
}

void MainController::btnSwashViewRotateRight_clicked()
{
    ui->swashView->rotateRight();
    int val = m_adaptorModel->getAdaptorValue(ADDR_SWASHPLATE_ANGLE);
    requestWriteConfigChanges(ADDR_SWASHPLATE_ANGLE, val);
}

void MainController::btnCyclicDefault_button_clicked()
{
    int ret = QMessageBox::warning(ui, Res(80), Res(170),
                                    QMessageBox::Ok, QMessageBox::Cancel);
    if (ret == QMessageBox::Cancel)
        return;

    if (sender() == ui->btnCyclicDefault) {
        ui->sliderCyclicLimit->setValue(100);
        ui->sliderCyclicGain->setValue(100);
        ui->sliderCyclicReponse->setValue(100);
        ui->sliderCyclicRoll->setValue(250);
        ui->sliderCyclicFlip->setValue(250);

        requestWriteConfigChanges(ADDR_CYCLIC_LIMIT, 100, true);
        requestWriteConfigChanges(ADDR_CYCLIC_GAIN, 100, true);
        requestWriteConfigChanges(ADDR_CYCLIC_AGILITY, 100, true);
        requestWriteConfigChanges(ADDR_CYCLIC_ROLL, 250, true);
        requestWriteConfigChanges(ADDR_CYCLIC_FLIP, 250, true);
    }
    else {
        ui->editRcDeadband->setText("10");
        ui->editElevatorPrecom->setText("50");
        ui->editPddleSimulate->setText("50");
        ui->editCyclicExpo->setText("0");

        requestWriteConfigChanges(ADDR_RC_DEADBAND, 10, true);
        requestWriteConfigChanges(ADDR_ELEVATOR_PRECOM, 50, true);
        requestWriteConfigChanges(ADDR_PADDLE_SIMULATE, 50, true);
        requestWriteConfigChanges(ADDR_CYCLIC_EXPO, 0, true);
    }
}

void MainController::btnTailDefault_button_clicked()
{

    int ret = QMessageBox::warning(ui, Res(80), Res(183),
                                    QMessageBox::Ok, QMessageBox::Cancel);
    if (ret == QMessageBox::Cancel)
        return;

    ui->sliderTailYaw->setValue(500);
    ui->sliderTailGain->setValue(100);

    ui->editTailAccelerate->setText("50");
    ui->editTailStopGain->setText("50");
    ui->editTailExpo->setText("0");
    ui->editTailCollective->setText("50");
    ui->editTailCyclic->setText("50");
    ui->editTailZeroCol->setText("50");

    requestWriteConfigChanges(ADDR_TAIL_YAW, 500, true);
    requestWriteConfigChanges(ADDR_TAIL_GAIN, 100, true);
    requestWriteConfigChanges(ADDR_TAIL_ACELERATION, 50, true);
    requestWriteConfigChanges(ADDR_TAIL_STOPGAIN, 50, true);
    requestWriteConfigChanges(ADDR_TAIL_EXPO, 0, true);
    requestWriteConfigChanges(ADDR_TAIL_COLLECTIVE, 50, true);
    requestWriteConfigChanges(ADDR_TAIL_CYCLIC, 50, true);
    requestWriteConfigChanges(ADDR_TAIL_ZEROCOL, 50, true);
}

void MainController::cbTxMode_current_index_changed(int index)
{
    switch (index) {
    case 0:
        m_adaptorModel->addAdaptor(ADDR_RX_PITCH,
                new RxMonitorAdaptor(RxMonitorAdaptor::Vertical, ui->rxViewRight));
        m_adaptorModel->addAdaptor(ADDR_RX_RUDDER,
                new RxMonitorAdaptor(RxMonitorAdaptor::Horizontal, ui->rxViewLeft));
        m_adaptorModel->addAdaptor(ADDR_RX_ELEVATOR,
                new RxMonitorAdaptor(RxMonitorAdaptor::Vertical, ui->rxViewLeft));
        m_adaptorModel->addAdaptor(ADDR_RX_AILERONS,
                new RxMonitorAdaptor(RxMonitorAdaptor::Horizontal, ui->rxViewRight));
        break;

    case 1:
        m_adaptorModel->addAdaptor(ADDR_RX_PITCH,
                new RxMonitorAdaptor(RxMonitorAdaptor::Vertical, ui->rxViewLeft));
        m_adaptorModel->addAdaptor(ADDR_RX_RUDDER,
                new RxMonitorAdaptor(RxMonitorAdaptor::Horizontal, ui->rxViewLeft));
        m_adaptorModel->addAdaptor(ADDR_RX_ELEVATOR,
                new RxMonitorAdaptor(RxMonitorAdaptor::Vertical, ui->rxViewRight));
        m_adaptorModel->addAdaptor(ADDR_RX_AILERONS,
                new RxMonitorAdaptor(RxMonitorAdaptor::Horizontal, ui->rxViewRight));
        break;

    case 2:
        m_adaptorModel->addAdaptor(ADDR_RX_PITCH,
                new RxMonitorAdaptor(RxMonitorAdaptor::Vertical, ui->rxViewRight));
        m_adaptorModel->addAdaptor(ADDR_RX_RUDDER,
                new RxMonitorAdaptor(RxMonitorAdaptor::Horizontal, ui->rxViewRight));
        m_adaptorModel->addAdaptor(ADDR_RX_ELEVATOR,
                new RxMonitorAdaptor(RxMonitorAdaptor::Vertical, ui->rxViewLeft));
        m_adaptorModel->addAdaptor(ADDR_RX_AILERONS,
                new RxMonitorAdaptor(RxMonitorAdaptor::Horizontal, ui->rxViewLeft));
        break;

    case 3:
        m_adaptorModel->addAdaptor(ADDR_RX_PITCH,
                new RxMonitorAdaptor(RxMonitorAdaptor::Vertical, ui->rxViewLeft));
        m_adaptorModel->addAdaptor(ADDR_RX_RUDDER,
                new RxMonitorAdaptor(RxMonitorAdaptor::Horizontal, ui->rxViewRight));
        m_adaptorModel->addAdaptor(ADDR_RX_ELEVATOR,
                new RxMonitorAdaptor(RxMonitorAdaptor::Vertical, ui->rxViewRight));
        m_adaptorModel->addAdaptor(ADDR_RX_AILERONS,
                new RxMonitorAdaptor(RxMonitorAdaptor::Horizontal, ui->rxViewLeft));
        break;

    default:
        return;
    }
}

void MainController::handleFlashStatusChanged(int status)
{
    switch (status)
    {
    case FlashHelper::PortCannotOpen:
        ui->hideProgressDialog();
        QMessageBox::critical(ui, Res(109), Res(103), QMessageBox::Ok);
        return;

    case FlashHelper::StartDownloadNewFirmware:
        ui->updateLabelFlashStatus(Res(101));
        ui->progressDialog->changeState(ProgressDialog::BarType);
        break;

    case FlashHelper::DownloadFirmwareError:
        ui->hideProgressDialog();
        QMessageBox::critical(ui, Res(109), Res(102), QMessageBox::Ok);
        return;

    case FlashHelper::StartEraseFlash:
        ui->updateLabelFlashStatus(Res(104));
        ui->progressDialog->changeState(ProgressDialog::RingType);
        break;

    case FlashHelper::StartWriteFirmware:
        ui->progressDialog->changeState(ProgressDialog::BarType);
        ui->updateLabelFlashStatus(Res(105));
        break;

    case FlashHelper::StartReconnect:
        ui->progressDialog->changeState(ProgressDialog::RingType);
        ui->updateLabelFlashStatus(Res(106));
        break;

    case FlashHelper::FlashResultError:
        ui->hideProgressDialog();
        QMessageBox::critical(ui, Res(109), Res(108), QMessageBox::Ok);
        return;

    case FlashHelper::FlashResultOk:
        ui->hideProgressDialog();
        QMessageBox::information(ui, Res(109), Res(107), QMessageBox::Ok);
        return;
    }
}

void MainController::handleFlashProgressChanged(int percent)
{
    ui->progressDialog->setProgressBarValue(percent);
    ui->progressDialog->repaint();
}

void MainController::btnOpen_button_clicked()
{
    QString dir = m_appConfig->get("last_params_dir");
    if (dir.isEmpty()) {
        dir = QCoreApplication::applicationDirPath();
    }
    QString filePath = QFileDialog::getOpenFileName(ui, Res(10), dir, Res(8));

    if (! filePath.isNull() && ! filePath.isEmpty()) {
        dir = filePath.mid(0, filePath.lastIndexOf("/"));
        m_appConfig->put("last_params_dir", dir);

        qint32 ret = m_paramModel->loadParamsFromFile(filePath);
        if (ret != 0) {
            QMessageBox::critical(ui, Res(10), Res(12), QMessageBox::Ok);
            return;
        } else {
            if (m_appMode == Online && m_serialManager->isPortConnected()) {
                ret = QMessageBox::warning(ui, Res(17), Res(19),
                                     QMessageBox::Ok, QMessageBox::Cancel);
                if (ret == QMessageBox::Cancel) return;
            }
        }

        if (m_serialManager->isPortConnected()) {
            m_loadProfileLevel = LOAD_PARAMS_LEVEL_ALL;
            changeAction(Sleep);
            ui->animationWhenLoadFirmwareParams(true);
        }

        foreach (const Parameter* p, m_paramModel->getParamsList()) {
            m_adaptorModel->setAdaptorValue(p->Address, p->Value);
            m_paramModel->setParamValue(p->Address, p->Value);
        }
    }
}

void MainController::btnSave_button_clicked()
{
    QString dir = m_appConfig->get("last_params_dir");
    if (dir.isEmpty()) {
        dir = QCoreApplication::applicationDirPath();
    }
    QString filePath = QFileDialog::getSaveFileName(ui, Res(11), dir, Res(8));

    if (! filePath.isNull() && ! filePath.isEmpty()) {
        dir = filePath.mid(0, filePath.lastIndexOf("/"));
        m_appConfig->put("last_params_dir", dir);

        m_paramModel->saveParamsToFile(filePath);
    }
}

void MainController::btnFirmware_button_clicked()
{
    FirmwareDialog fd(ui);
    int result = fd.exec();

    if (result == QDialog::Accepted) {
        QString dir = m_appConfig->get("last_firmware_dir");
        if (dir.isEmpty()) {
            dir = QCoreApplication::applicationDirPath();
        }
        QString filePath = QFileDialog::getOpenFileName(
                    ui, Res(4), dir, Res(5));

        // check return if selected file not exist or cannot open
        QFile file(filePath);
        if (!file.exists()) return;
        if (!file.open(QIODevice::ReadOnly)) return;

        // save last directory path and selected file name to cache
        dir = filePath.mid(0, filePath.lastIndexOf("/"));
        m_appConfig->put("last_firmware_dir", dir);

        if (m_serialManager->isPortConnected()) {
            this->onDisconnect();
        }

        emit _needUpdateFlash(file.readAll());
        ui->showProgressDialog(ProgressDialog::RingType, Res(2));
    }
}

void MainController::btnInfo_button_clicked()
{
    InfoDialog *infoDlg = new InfoDialog(ui);
    infoDlg->show();
}

void MainController::btnWriteLoadedParams_button_clicked()
{
    QAbstractButton *button = qobject_cast<QAbstractButton*> (sender());
    if (button == ui->btnAcceptWriteLoadedParams) {

        ui->showProgressDialog(ProgressDialog::BarType, Res(9));

        double percent = 0;
        double maxPercent = m_paramModel->size();
        QList<Parameter*> paramsList = m_paramModel->getParamsList();
        foreach (const Parameter* p, paramsList) {
            percent ++;
            ui->progressDialog->setProgressBarValue(
                        (int)(percent / maxPercent * 100));

            // process events in queue to update UI of progress dialog
            QApplication::processEvents();

            if (m_loadProfileLevel == LOAD_PROFILE_PARAMS_LEVEL) {
                switch (p->Address) {
                // only some params need to save with profile
                case ADDR_HELI_SIZE:
                case ADDR_SENSOR_DIRECTION:
                case ADDR_SWASHPLATE:
                case ADDR_ROTATE_DIRECTION:
                case ADDR_SERVO_CH_1:
                case ADDR_SERVO_CH_2:
                case ADDR_SERVO_CH_3:
                case ADDR_SERVO_CH_4:
                case ADDR_SERVO_TYPE:
                case ADDR_COLLECTIVE_DIRECTION:
                case ADDR_TAIL_SERVO:
                case ADDR_TAIL_SERVO_TYPE:
                case ADDR_GOVERNOR_MODE:
                    continue;
                default:
                    break;
                }
            }

            QString text = QString::number(p->Address) +","+ QString::number(p->Value);
            m_serialManager->writeConfigBlocking(text);
        }

        ui->hideProgressDialog();
    }
    else {
        saveProfile(m_currentProfile);
        m_currentProfile = 0;
        ui->cbProfile->setCurrentIndex(m_currentProfile);
    }

    m_loadProfileLevel = 0;
    changeAction(Running);
    ui->animationWhenLoadFirmwareParams(false);
}

void MainController::cbProfile_activated(int index)
{
    if (m_appMode == Online && m_serialManager->isPortConnected()) {
        int ret = QMessageBox::warning(ui, Res(17), Res(18),
                             QMessageBox::Ok, QMessageBox::Cancel);
        if (ret != QMessageBox::Ok) {
            ui->cbProfile->setCurrentIndex(m_currentProfile);
            return;
        }
    }

    m_currentProfile = index;
    if (loadProfile(index)) {
        if (m_serialManager->isPortConnected()) {
            if (m_loadProfileLevel == 0) {
                m_loadProfileLevel = LOAD_PROFILE_PARAMS_LEVEL;
            }
            changeAction(Sleep);
            ui->animationWhenLoadFirmwareParams(true);
        }

        foreach (const Parameter *p, m_paramModel->getParamsList()) {
            m_adaptorModel->setAdaptorValue(p->Address, p->Value);
            m_paramModel->setParamValue(p->Address, p->Value);
        }
    }
}

void MainController::btnSaveProfileAs_clicked()
{
    QMenu menu(ui);
    QPoint point(0, ui->btnSaveProfileAs->height());
    menu.move(ui->btnSaveProfileAs->mapToGlobal(point));
    for (int i = 0; i < 5; i++) {
        menu.addAction("Profile " + QString::number(i + 1));
    }
    QAction *action = menu.exec();
    if (action != 0) {
        const QString txt = action->text();
        int profileId = txt.mid(txt.size() - 2).toInt();
        if (profileId > 0) {
            saveProfile(profileId);
        }
    }
}

void MainController::intializeModels()
{
    ModelBuilder builder(ui);
    m_adaptorModel = builder.buildAdaptorModel();
    m_paramModel = builder.buildParameterModel();

    connect(m_adaptorModel, SIGNAL(_adaptorValueChanged(int,int,bool)),
            this, SLOT(requestWriteConfigChanges(int,int,bool)));

    foreach (QObject* obj, m_adaptorModel->getListAdaptors()) {

        if (qobject_cast<SliderAdaptor*> (obj)) {

            SliderAdaptor* adaptor = qobject_cast<SliderAdaptor*> (obj);
            QSlider* slider = qobject_cast<QSlider*> (adaptor->inputField());

            connect(slider, SIGNAL(valueChanged(int)),
                    ui, SLOT(slider_valueChanged(int)));

            if (qobject_cast<SliderWithLineEditAdaptor*> (obj)) {

                SliderWithLineEditAdaptor *sa =
                        qobject_cast<SliderWithLineEditAdaptor*> (obj);
                QLineEdit *edit = sa->getLineEdit();

                connect(edit, SIGNAL(textEdited(QString)),
                        ui, SLOT(lineEdit_textEdited(QString)));
                connect(edit, SIGNAL(returnPressed()),
                        ui, SLOT(hideLineEditAlert()));
                connect(sa->getLineEditAdaptor(), SIGNAL(_editLostFocus(QObject*)),
                        ui, SLOT(hideLineEditAlert()));
            }
        }
        else if (qobject_cast<LineEditAdaptor*> (obj)) {

            LineEditAdaptor* adaptor = qobject_cast<LineEditAdaptor*> (obj);
            QLineEdit* edit = qobject_cast<QLineEdit*> (adaptor->inputField());

            connect(edit, SIGNAL(textEdited(QString)),
                    ui, SLOT(lineEdit_textEdited(QString)));
            connect(edit, SIGNAL(returnPressed()),
                    ui, SLOT(hideLineEditAlert()));
            connect(adaptor, SIGNAL(_editLostFocus(QObject*)),
                    ui, SLOT(hideLineEditAlert()));
        }
        else if (qobject_cast<CheckableButtonWithVerifyAdaptor*> (obj)) {

            CheckableButtonWithVerifyAdaptor* adaptor =
                    qobject_cast<CheckableButtonWithVerifyAdaptor*> (obj);
            QToolButton *button = qobject_cast<QToolButton*> (
                        adaptor->inputField());

            connect(button, SIGNAL(toggled(bool)),
                    ui, SLOT(servoButtons_toggled(bool)));
            connect(adaptor, SIGNAL(_clicked(QObject*,bool,bool*)),
                    this, SLOT(servoButtons_clicked(QObject*,bool,bool*)));
        }
        else if (qobject_cast<ButtonGroupWithVerifyAdaptor*> (obj)) {

            ButtonGroupWithVerifyAdaptor* adaptor =
                    qobject_cast<ButtonGroupWithVerifyAdaptor*> (obj);
            ButtonGroup* btgroup = qobject_cast<ButtonGroup*> (
                        adaptor->inputField());

            connect(btgroup, SIGNAL(buttonToggled(int,bool)),
                    ui, SLOT(buttonGroup_button_toggled(int,bool)));
            connect(adaptor, SIGNAL(_buttonClicked(QObject*,int,bool*)),
                    this, SLOT(buttonGroup_button_clicked(QObject*,int,bool*)));
        }
        else if (qobject_cast<ComboBoxAdaptor*> (obj)) {

            ComboBoxAdaptor* adaptor = qobject_cast<ComboBoxAdaptor*> (obj);
            QComboBox* cb = qobject_cast<QComboBox*> (adaptor->inputField());

            connect(cb, SIGNAL(currentIndexChanged(int)),
                    ui, SLOT(comboBox_currentIndexChanged(int)));
        }
    }
}

void MainController::registerConnections()
{
    ui->installEventFilter(this);

    // main widgets

    connect(ui->mainTab, SIGNAL(currentChanged(int)),
            this, SLOT(mainTab_current_changed(int)));

    connect(&m_updateUiTimer, SIGNAL(timeout()),
            this, SLOT(mainTimer_timeout()));

    // logic connections

    connect(m_serialManager, SIGNAL(started()),
            m_serialManager, SLOT(tryConnect()));

    connect(this, SIGNAL(_needReconnect()),
            m_serialManager, SLOT(tryReconnect()));

    connect(m_serialManager, SIGNAL(_portStatusChanged(int)),
            this, SLOT(handlePortStatusChanged(int)));

    connect(m_serialManager, SIGNAL(_responseToUpdateUi(QVariant)),
            this, SLOT(handleParamsChanged(QVariant)));

    connect(this, SIGNAL(_needUpdateFlash(QByteArray)),
            m_serialManager, SLOT(startUpdateFirmware(QByteArray)));

    connect(m_serialManager, SIGNAL(_flashStatusChanged(int)),
            this, SLOT(handleFlashStatusChanged(int)));
    connect(m_serialManager, SIGNAL(_flashProgressChanged(int)),
            this, SLOT(handleFlashProgressChanged(int)));

    // footer menu connections

    connect(ui->btnOpen, SIGNAL(clicked()),
            this, SLOT(btnOpen_button_clicked()));
    connect(ui->btnSave, SIGNAL(clicked()),
            this, SLOT(btnSave_button_clicked()));
    connect(ui->btnFirmware, SIGNAL(clicked()),
            this, SLOT(btnFirmware_button_clicked()));
    connect(ui->btnInfo, SIGNAL(clicked()),
            this, SLOT(btnInfo_button_clicked()));

    connect(ui->cbProfile, SIGNAL(activated(int)),
            this, SLOT(cbProfile_activated(int)));
    connect(ui->btnSaveProfileAs, SIGNAL(clicked()),
            this, SLOT(btnSaveProfileAs_clicked()));

    connect(ui->btnAcceptWriteLoadedParams, SIGNAL(clicked()),
            this, SLOT(btnWriteLoadedParams_button_clicked()));
    connect(ui->btnRejectWriteLoadedParams, SIGNAL(clicked()),
            this, SLOT(btnWriteLoadedParams_button_clicked()));

    // tab start connections

    connect(ui->cbTxMode, SIGNAL(currentIndexChanged(int)),
            this, SLOT(cbTxMode_current_index_changed(int)));

    connect(ui->btnSetCenter, SIGNAL(clicked()),
            this, SLOT(btnSetCenter_clicked()));

    connect(ui->btnBind, SIGNAL(clicked()), this, SLOT(btnBind_clicked()));

    // tab swash

    connect(ui->btnSwashViewRotateLeft, SIGNAL(clicked()),
            this, SLOT(btnSwashViewRotateLeft_clicked()));

    connect(ui->btnSwashViewRotateRight, SIGNAL(clicked()),
            this, SLOT(btnSwashViewRotateRight_clicked()));

    // tab cyclic connections

    connect(ui->btnCyclicDefault, SIGNAL(clicked()),
            this, SLOT(btnCyclicDefault_button_clicked()));
    connect(ui->btnCyclicDefaultAdvance, SIGNAL(clicked()),
            this, SLOT(btnCyclicDefault_button_clicked()));

    // tab tail connections

    connect(ui->btnTailDefault, SIGNAL(clicked()),
            this, SLOT(btnTailDefault_button_clicked()));
}

void MainController::setupUi()
{
    ui->registerUiConnections();
    ui->intializeWidgetProperties();
    ui->setToolTipForWidgets();
}

void MainController::changeAction(MainController::Action action)
{
    switch (action) {
    case Sleep:
        if (m_currentAction == Sleep) return;

        m_appMode = Offline;
        if (m_updateUiTimer.isActive()) {
            m_updateUiTimer.stop();
        }

        break;

    case Running:
        if (m_currentAction == Running) return;

        m_appMode = Online;
        m_updateUiCounter = 0;

        if (! m_updateUiTimer.isActive()) {
            m_updateUiTimer.start();
        }
        m_requestManager.resetCounters();

        break;
    }

    m_currentAction = action;
}

void MainController::loadApplicationConfig()
{
    if (m_appConfig->hasKey("txmode")) {
        int index = m_appConfig->get("txmode").toInt();
        if (index < ui->cbTxMode->count())
            ui->cbTxMode->setCurrentIndex(index);
    }
    if (m_appConfig->hasKey("lang")) {
        QString appLanguage = m_appConfig->get("lang");
        for (qint8 i = 0; i < ui->cbLanguage->count(); i++) {
            if (ui->cbLanguage->itemData(i).toString() == appLanguage) {
                ui->cbLanguage->setCurrentIndex(i);
                break;
            }
        }
    }
    if (m_appConfig->hasKey("gyro-off")) {
        ui->checkGyroOff->setChecked(m_appConfig->get("gyro-off").toInt());
    }
}

void MainController::saveApplicationConfig()
{
    m_appConfig->put("txmode", QString::number(ui->cbTxMode->currentIndex()));
    m_appConfig->put("lang", ui->cbLanguage->currentData().toString());
    m_appConfig->put("gyro-off",
                     QString::number((int)ui->checkGyroOff->isChecked()));
    m_appConfig->saveConfigToFile();
}

void MainController::saveProfile(int profileIndex)
{
    if (profileIndex != 0) {
        QString filePath = APP_PROFILE_FILE_PREFIX
                + QString::number(profileIndex)
                + APP_PROFILE_FILE_EXTENSION;
        m_paramModel->saveParamsToFile(filePath);
    }
}

bool MainController::loadProfile(int profileIndex)
{
    if (profileIndex != 0) {
        QString filePath = APP_PROFILE_FILE_PREFIX
                + QString::number(profileIndex)
                + APP_PROFILE_FILE_EXTENSION;
        int ret = m_paramModel->loadParamsFromFile(filePath);
        if (ret != 0) {
            QMessageBox::warning(ui, Res(15), Res(16), QMessageBox::Ok);
            return false;
        }
        return true;
    }
    return false;
}



