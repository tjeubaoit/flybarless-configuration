#ifndef FIRMWAREASKER_H
#define FIRMWAREASKER_H

#include <QString>

class FirmwareAsker
{
public:
    FirmwareAsker();
    ~FirmwareAsker();

    QString getAddressNeedUpdate(int);
    QString getAllAddress();
    void resetCounters();

    int totalRequestError() const;
    void setRequestAccepted();
    void resetErrorCounter();

private:
    void updateCounters(int);

    int *m_updateCounters;
    int m_requestErrorCounter;

};

#endif // FIRMWAREASKER_H
