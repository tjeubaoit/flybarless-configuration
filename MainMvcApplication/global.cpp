#include "global.h"
#include <QString>
#include <QStringList>

void qTextToDouble(QString text, int &left, int &right)
{
    char splitChar = '.';
    if (text.contains(',')) splitChar = ',';
    QStringList list = text.split(splitChar);
    left = list.first().toInt();
    right = 0;
    if (list.length() > 1) {
        right = list.last().toInt();
        if (list.last().length() <= 1) {
            right = right * 10;
        }
    }
}
