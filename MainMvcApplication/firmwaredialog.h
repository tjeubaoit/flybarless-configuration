#ifndef FIRMWAREDIALOG_H
#define FIRMWAREDIALOG_H

#include <QDialog>
#include "progressring.h"

namespace Ui {
class FirmwareDialog;
}

class FirmwareDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FirmwareDialog(QWidget *parent = 0);
    ~FirmwareDialog();

private slots:
    void on_btnFirmware_clicked();

    void on_btnClose_clicked();

    void on_btnCancel_clicked();

private:
    Ui::FirmwareDialog *ui;
    ProgressRing* m_progressRing;    
};

#endif // FIRMWAREDIALOG_H
