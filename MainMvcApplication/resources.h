#ifndef RESOURCES_H
#define RESOURCES_H

// define image resources

#define url_application_background "images/background.png"
#define url_application_icon ":/Apollo.ico"

#define url_checked_icon "images/checked.png"
#define url_splash_screen "images/splash-screen.jpg"

#define url_start_guide "docs/en/start.htm"
#define url_rx_guide "docs/en/rx.htm"
#define url_sensor_guide "docs/en/sensor.htm"
#define url_swash_guide "docs/en/swash.htm"
#define url_servos_guide "docs/en/servos.htm"
#define url_collective_guide "docs/en/collective.htm"
#define url_cyclic_guide "docs/en/cyclic.htm"
#define url_tail_guide "docs/en/tail.htm"
#define url_governor_guide "docs/en/governor.htm"

#define url_xml_strings_en "docs/en/strings.xml"
#define url_xml_strings_fr "docs/fr/strings.xml"
#define url_xml_strings_vi "docs/vi/strings.xml"

// define string resources

#define Res(index) m_res->getValue(index)

// define stylesheet resources

#define style_label_connected "QLabel { border-image:url(images/connected.png); }"
#define style_label_disconnected "QLabel { border-image:url(images/disconnected.png); }"
#define style_slider_outside_ideal_value "QSlider::sub-page:horizontal{\
                background:#F3140E;} QSlider::add-page:vertical{ background:#F3140E; }"

#endif // RESOURCES_H
