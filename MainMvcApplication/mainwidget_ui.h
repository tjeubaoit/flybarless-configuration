#ifndef FORM_H
#define FORM_H

#include "ui_mainwidget_ui.h"
#include "customframe.h"
#include "buttongroup.h"
#include "progressdialog.h"
#include "servosview.h"
#include "rxmonitorview.h"
#include "lineeditalert.h"
#include "doubledirectslider.h"
#include "trimswashcyclic.h"
#include "trimswashcollective.h"
#include "flystyleview.h"
#include "resourcemanager.h"
#include "resources.h"


class Ui_MainWidget : public CustomFrame, public Ui::MainWidget_Ui
{
    Q_OBJECT

public:
    ResourceManager *m_res;

    QButtonGroup *btGroupRadioHeliSize;
    FlyStyleView *flyStyleView;
    RxMonitorView *rxViewLeft;
    RxMonitorView *rxViewRight;
    ButtonGroup *btGroupSensorDirection;
    ButtonGroup *btGroupSwashplate;
    ButtonGroup *btGroupRotateDirection;
    ButtonGroup *btGroupCollectDirection;
    QButtonGroup *btGroupRadioTail;
    AdvanceServosView *swashView;
    AdvanceServosView *servoView;
    DoubleDirectSlider *sliderServoCh1;
    DoubleDirectSlider *sliderServoCh2;
    DoubleDirectSlider *sliderServoCh3;
    DoubleDirectSlider *sliderServoCh4;
    DoubleDirectSlider *sliderTailServo;
    SwashTrimCyclic *trimSwashCyclic;
    SwashTrimCollective *trimSwashCollective;
    LineEditAlert* lbLineEditAlert;
    ProgressDialog *progressDialog;

    explicit Ui_MainWidget();
    ~Ui_MainWidget();

public Q_SLOTS:
    void registerUiConnections();
    void intializeWidgetProperties();
    void setToolTipForWidgets();

    void showLineEditAlert(const QPoint& pos);
    void hideLineEditAlert();

    void showProgressDialog(ProgressDialog::Type state = ProgressDialog::RingType,
                            QString content = "");
    void hideProgressDialog();

    void updateWidgetsPositionOnTabServos(int swashType);
    void updateRelativePositionOnTabServos(const QPoint &point, QAbstractButton* button,
                                            DoubleDirectSlider *sliderServos);

    void updateLabelPortStatus(bool);
    void updateLabelFlashStatus(const QString& text);
    void changeColorWhenSliderExceedIdealValue(QSlider*);
    void setLineEditText(QLineEdit *edit, QString text);

    void animationWhenTabChange(int tabIndex);
    void animationWhenLoadFirmwareParams(bool isAnimToShow);
    void animationWhenBindRx();
    void animationWhenShowAdvance(QStackedWidget*, bool);
    void animationShowGyroOff();

    void comboBox_currentIndexChanged(int);
    void btnChangeAdvanceNormalMode_clicked(bool);
    void btnChangeAdvanceNormalMode_toggled(bool);
    void buttonGroup_button_toggled(int, bool);
    void servoButtons_toggled(bool);
    void slider_valueChanged(int);
    void lineEdit_textEdited(const QString&);
    void rxView_sensor_value_changed(qint32 x, qint32 y);
    void swashView_angle_changed(const QList<int>&);

protected:
    void keyPressEvent(QKeyEvent * event);

private:
    QList<int> getListChannelAngleBySwashType(int);

};

namespace Ui {
    class MainWidget: public Ui_MainWidget {};
}

#endif // FORM_H
