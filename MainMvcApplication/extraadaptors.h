#include "abstractfieldadaptor.h"
#include "doubledirectslider.h"
#include "flystyleview.h"
#include "servosview.h"
#include "trimswashcollective.h"
#include "trimswashcyclic.h"
#include "rxmonitorview.h"
#include "comboboxadaptor.h"
#include "lineeditadaptor.h"
#include "sliderwithlineeditadaptor.h"
#include "checkboxadaptor.h"
#include "checkablebuttonadaptor.h"
#include "buttongroupadaptor.h"
#include "global.h"

#include <QComboBox>
#include <QLineEdit>
#include <QSlider>
#include <QButtonGroup>
#include <buttongroup.h>
#include <QObject>
#include <QEvent>


class ButtonGroupWithVerifyAdaptor : public ButtonGroupAdaptor
{
    Q_OBJECT
public:
    ButtonGroupWithVerifyAdaptor(QObject* field, QObject* parent = 0);

    virtual void setValue(const QVariant&);

Q_SIGNALS:
    void _buttonClicked(QObject *, int, bool*);

protected:
    virtual void buttonClicked(int id);
};


class CheckableButtonWithVerifyAdaptor : public CheckableButtonAdaptor
{
    Q_OBJECT
public:
    CheckableButtonWithVerifyAdaptor(QObject* field, QObject* parent = 0);

Q_SIGNALS:
    void _clicked(QObject *, bool, bool*);

protected:
    virtual void clicked(bool checked);
};


class DoubleDirectSliderAdaptor : public SliderWithLineEditAdaptor
{
public:
    DoubleDirectSliderAdaptor(QObject* field, QObject* extraField,
                              QObject* parent = 0)
        : SliderWithLineEditAdaptor(field, extraField, parent)
    {

    }

    QVariant getValue() const
    {
        return SliderWithLineEditAdaptor::getValue().toInt() + 100;
    }

    void setValue(const QVariant &value)
    {
        SliderWithLineEditAdaptor::setValue(GET_VALUE(value) - 100);
    }
};


class FlyStyleAdaptor : public AbstractFieldAdaptor
{
    Q_OBJECT
public:
    FlyStyleAdaptor(QObject* field, QObject* parent = 0)
        : AbstractFieldAdaptor(field, parent)
    {
        FlyStyleView *fly = qobject_cast<FlyStyleView*> (field);
        connect(fly, SIGNAL(_valueChanged(int)),
                this, SLOT(flyStyleValueChanged(int)));
    }

    QVariant getValue() const
    {
        FlyStyleView *fly = qobject_cast<FlyStyleView*> (m_field);
        return fly->getValue();
    }

    void setValue(const QVariant &value)
    {
        FlyStyleView *fly = qobject_cast<FlyStyleView*> (m_field);
        int val = GET_VALUE(value);
        fly->setValue(val);
    }

protected Q_SLOTS:
    virtual void flyStyleValueChanged(int val)
    {
        emit _adaptorValueChanged(val);
    }
};


class ServoAngleAdaptor : public AbstractFieldAdaptor
{
public:
    ServoAngleAdaptor(QObject* field, QObject* parent = 0)
        : AbstractFieldAdaptor(field, parent)
    {

    }

    QVariant getValue() const
    {
        AdvanceServosView *servos = qobject_cast<AdvanceServosView*> (m_field);
        return servos->angle();
    }

    void setValue(const QVariant &value)
    {
        AdvanceServosView *servos = qobject_cast<AdvanceServosView*> (m_field);
        int angle = GET_VALUE(value);
        servos->setAngle(angle);
    }
};


class SwashTrimCollectiveAdaptor : public AbstractFieldAdaptor
{
    Q_OBJECT
public:
    SwashTrimCollectiveAdaptor(QObject* field, QObject* parent = 0)
        : AbstractFieldAdaptor(field, parent)
    {
        SwashTrimCollective *trim = qobject_cast<SwashTrimCollective*> (m_field);
        connect(trim, SIGNAL(_trimValueChanged(int)),
                this, SLOT(trimValueChanged(int)));
    }

    QVariant getValue() const
    {
        return 0;
    }

    void setValue(const QVariant &value)
    {
        SwashTrimCollective *trim = qobject_cast<SwashTrimCollective*> (m_field);
        int val = GET_VALUE(value);
        trim->setTrimValue(val - 100);
    }

protected Q_SLOTS:
    virtual void trimValueChanged(int val)
    {
        emit _adaptorValueChanged(val + 100);
    }
};


class SwashTrimCyclicAdaptor : public AbstractFieldAdaptor
{
    Q_OBJECT
public:
    SwashTrimCyclicAdaptor(SwashTrimCyclic::TrimType type,
                           SwashTrimCyclic::TrimDirection direction,
                           QObject* field, QObject* parent = 0)
        : AbstractFieldAdaptor(field, parent)
    {
        m_type = type;
        m_direction = direction;

        SwashTrimCyclic *trim = qobject_cast<SwashTrimCyclic*> (field);
        connect(trim, SIGNAL(_trimValueChanged(int,int,int,bool)),
                this, SLOT(trimValueChanged(int,int,int,bool)));
    }

    QVariant getValue() const
    {
        return 0;
    }

    void setValue(const QVariant &value)
    {
        SwashTrimCyclic *trim = qobject_cast<SwashTrimCyclic*> (m_field);
        int val = GET_VALUE(value);
        trim->setTrimValue(m_type, m_direction, val - 100);
    }

protected Q_SLOTS:
    virtual void trimValueChanged(int type, int direction, int val, bool blocking)
    {
        if (m_type == type && m_direction == direction) {
            emit _adaptorValueChanged(val + 100, blocking);
        }
    }

private:
    SwashTrimCyclic::TrimDirection m_direction;
    SwashTrimCyclic::TrimType m_type;
};


class SwashTrimCyclicMonitorAdaptor : public AbstractFieldAdaptor
{
public:
    SwashTrimCyclicMonitorAdaptor(SwashTrimCyclic::MonitorChannel channel,
                                  QObject* field, QObject* parent = 0)
        :  AbstractFieldAdaptor(field, parent)
    {
        m_channel = channel;
    }

    QVariant getValue() const
    {
        return 0;
    }

    void setValue(const QVariant &value)
    {
        SwashTrimCyclic *trim = qobject_cast<SwashTrimCyclic*> (m_field);
        int val = GET_VALUE(value);
        trim->setTrimMonitorValue(m_channel, val);
    }

private:
    SwashTrimCyclic::MonitorChannel m_channel;
};


class RxMonitorAdaptor : public AbstractFieldAdaptor
{
public:
    enum Direction { Horizontal, Vertical };

    RxMonitorAdaptor(RxMonitorAdaptor::Direction direction,
                        QObject* field, QObject* parent = 0)
        : AbstractFieldAdaptor(field, parent)
    {
        m_direction = direction;
    }

    QVariant getValue() const { return 0; }

    void setValue(const QVariant &value)
    {
        int val = GET_VALUE(value);
        RxMonitorView *rx = qobject_cast<RxMonitorView*> (m_field);

        if (m_direction == Horizontal) {
            rx->x = val;
        }
        else if (m_direction == Vertical) {
            rx->y = val;
        }
        else return;

        if (rx->need_move) {
            rx->moveSensor();
        } else {
            rx->need_move = true;
        }
    }

private:
    Direction m_direction;
};


class RxTypeComboBoxAdaptor : public ComboBoxAdaptor
{
    Q_OBJECT
public:
    RxTypeComboBoxAdaptor(QComboBox *cbRxType, QComboBox *cbRxTypeSatel,
                          QObject* parent = 0)
        : ComboBoxAdaptor(cbRxType, parent)
    {
        m_cbRxSatellite = cbRxTypeSatel;
        connect(m_cbRxSatellite, SIGNAL(activated(int)),
                this, SLOT(cbStellite_activated(int)));
    }

    QVariant getValue() const
    {
        QComboBox *cbRxType = qobject_cast<QComboBox*> (m_field);
        if (cbRxType->currentData().toInt() != Fbl::Satellite) {
            return cbRxType->currentData();
        }
        else return m_cbRxSatellite->currentData();
    }

    void setValue(const QVariant &value)
    {
        int val = GET_VALUE(value);
        if (val == Fbl::DSM2 || val == Fbl::DSMX) {
            QComboBox *cbRxType = qobject_cast<QComboBox*> (m_field);
            cbRxType->setCurrentIndex(cbRxType->count() - 1);

            int satelliteIndex = (val = Fbl::DSM2) ? 0: 1;
            m_cbRxSatellite->setCurrentIndex(satelliteIndex);
        }
        else ComboBoxAdaptor::setValue(value);
    }

protected Q_SLOTS:
    void cbStellite_activated(int)
    {
        emit _adaptorValueChanged(getValue());
    }

private:
    QComboBox *m_cbRxSatellite;
};


class GearRatioLineEditAdaptor : public LineEditAdaptor
{
public:
    enum Location { Left, Right };

    GearRatioLineEditAdaptor(GearRatioLineEditAdaptor::Location location,
                             QObject* field, QObject* parent = 0)
        : LineEditAdaptor(field, parent)
    {
        m_location = location;
    }

    QVariant getValue() const
    {
        QLineEdit* line = qobject_cast<QLineEdit*> (m_field);
        int left = 0, right = 0;
        qTextToDouble(line->text(), left, right);

        return (m_location == Left) ? left : right;
    }

    void setValue(const QVariant &value)
    {
        QLineEdit* edit = qobject_cast<QLineEdit*> (m_field);
        int left = 0, right = 0;
        qTextToDouble(edit->text(), left, right);
        const int val = GET_VALUE(value);

        if (m_location == Left) {
            QString text = QString::number(right);
            FILL(text , 2);
            edit->setText(QString::number(val) +"."+ text);
        }
        else {
            QString text = QString::number(val);
            FILL(text, 2);
            edit->setText(QString::number(left) +"."+ text);
        }
    }

protected:
    virtual void emitValueChanged()
    {
        emit _adaptorValueChanged(getValue(), true);
    }

private:
    Location m_location;
};


class MaxHeadSpeedLineEditAdaptor : public LineEditAdaptor
{
public:
    MaxHeadSpeedLineEditAdaptor(QObject* field, QObject* parent = 0)
        : LineEditAdaptor(field, parent)
    {

    }

    QVariant getValue() const
    {
        QLineEdit* edit = qobject_cast<QLineEdit*> (m_field);
        int val = edit->text().toInt();
        val = qRound((double)val / 10);
        edit->setText(QString::number(val * 10));

        return val;
    }

    void setValue(const QVariant &value)
    {
        QLineEdit* edit = qobject_cast<QLineEdit*> (m_field);
        int val = GET_VALUE(value);
        QString text = QString::number(val * 10);
        if (text != edit->text()) {
            edit->setText(text);
        }
    }
};


class GovMotorOffSliderWithLineEditAdaptor : public SliderWithLineEditAdaptor
{
public:
    GovMotorOffSliderWithLineEditAdaptor(QObject* field, QObject* parent = 0)
        : SliderWithLineEditAdaptor(field, parent)
    {

    }

    QVariant getValue() const
    {
        QSlider *slider = qobject_cast<QSlider*> (m_field);
        return (slider->value() * (-1));
    }

    void setValue(const QVariant &value)
    {
        QSlider *slider = qobject_cast<QSlider*> (m_field);
        int val = GET_VALUE(value);
        slider->setValue(val * (-1));
    }
};
