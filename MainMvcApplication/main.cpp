
#include <QApplication>
#include <QSplashScreen>
#include <QIcon>
#include <QStandardPaths>
#include <QDir>

#ifdef Q_OS_WIN32
#   include "windows.h"
#endif

#include "global.h"
#include "resources.h"
#include "maincontroller.h"
#include "resourcemanager.h"
#include "appconfigmanager.h"
#include "maincontroller.h"

#include <iostream>

int main(int argc, char *argv[])
{
#ifdef RUN_WITH_PYTHON
    if (argc < 2) {
        return 0;
    }
    else {
        QString argument(argv[1]);
        if (argument != "0") {
            return 0;
        }
    }
#endif

    QApplication app(argc, argv);
    app.setWindowIcon(QIcon(url_application_icon));
    QString path = QStandardPaths::standardLocations(QStandardPaths::DataLocation).at(0);
    QDir dir(path);
    if (!dir.exists()) {
        dir.mkpath(path);
    }

    QSplashScreen splash(QPixmap(url_splash_screen), Qt::WindowStaysOnTopHint);
    splash.show();

    AppConfigManager appConfig(APP_CONFIGURATION_FILE_PATH);
    QString resourceUrl = url_xml_strings_en;
    if (appConfig.get("lang") == "fr") {
        resourceUrl = url_xml_strings_fr;
    }
    else if (appConfig.get("lang") == "vi") {
        resourceUrl = url_xml_strings_vi;
    }

    ResourceManager *res = ResourceManager::instance(resourceUrl);
    splash.showMessage(res->getValue(3), Qt::AlignRight | Qt::AlignBottom, Qt::white);

    MainController controller(&appConfig);
    QTimer::singleShot(2000, &controller, SLOT(showMainWindow()));
    QTimer::singleShot(2000, &splash, SLOT(hide()));

    return app.exec();
}
