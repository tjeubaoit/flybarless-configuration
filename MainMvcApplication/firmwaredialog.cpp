#include "firmwaredialog.h"
#include "ui_firmwaredialog.h"
#include "resourcemanager.h"
#include <QMessageBox>
#include <QTimer>

FirmwareDialog::FirmwareDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FirmwareDialog)
{
    ui->setupUi(this);
    setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog);

#ifdef RELEASE_VERSION
    m_progressRing = new ProgressRing(this);
    m_progressRing->move(ui->lbLatestFirmware->x() + 4,
                         ui->lbLatestFirmware->y() + 4);
    m_progressRing->setFixedSize(22, 22);
    m_progressRing->start();

    QTimer::singleShot(3000, m_progressRing, SLOT(stop()));
#endif
}

FirmwareDialog::~FirmwareDialog()
{
    delete ui;
}

void FirmwareDialog::on_btnFirmware_clicked()
{
#ifdef RELEASE_VERSION
    ResourceManager *res = ResourceManager::instance();
    int ret = QMessageBox::question(parentWidget(), res->getValue(109),
                                    res->getValue(110),
                                    QMessageBox::Yes, QMessageBox::No);
    if (ret == QMessageBox::Yes) {
        QDialog::done(QDialog::Accepted);
    }
#else
    QDialog::done(QDialog::Accepted);
#endif
}

void FirmwareDialog::on_btnClose_clicked()
{
    QDialog::done(QDialog::Rejected);
}

void FirmwareDialog::on_btnCancel_clicked()
{
    QDialog::done(QDialog::Rejected);
}
