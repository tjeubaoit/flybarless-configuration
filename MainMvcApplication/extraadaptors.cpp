#include "extraadaptors.h"

ButtonGroupWithVerifyAdaptor::ButtonGroupWithVerifyAdaptor(
        QObject* field, QObject* parent)
    : ButtonGroupAdaptor(field, parent)
{

}

void ButtonGroupWithVerifyAdaptor::setValue(const QVariant &value)
{
    ButtonGroupAdaptor::setValue(value);
    ButtonGroup* btgroup = qobject_cast<ButtonGroup*> (m_field);

    int id = GET_VALUE(value);
    if (btgroup->lastButtonCheckedId() != id) {
        btgroup->setLastButtonChecked(btgroup->button(id));
    }
}

void ButtonGroupWithVerifyAdaptor::buttonClicked(int id)
{
    ButtonGroup *btGroup = qobject_cast<ButtonGroup*> (m_field);
    if (btGroup->lastButtonCheckedId() == id) {
        return;
    }
    else m_readOnly = true;

    bool isAccepted = false;
    emit _buttonClicked(m_field, id, &isAccepted);
    if (isAccepted) {
        btGroup->setLastButtonChecked(btGroup->button(id));
        emit _adaptorValueChanged(getValue());
    }
    else if (btGroup->lastButtonChecked() != 0) {
        btGroup->lastButtonChecked()->setChecked(true);
    }

    m_readOnly = false;
}

CheckableButtonWithVerifyAdaptor::CheckableButtonWithVerifyAdaptor(
        QObject* field, QObject* parent)
    : CheckableButtonAdaptor(field, parent)
{

}

void CheckableButtonWithVerifyAdaptor::clicked(bool checked)
{
    m_readOnly = true;

    bool isAccepted = false;
    emit _clicked(m_field, checked, &isAccepted);
    if (isAccepted) {
        emit _adaptorValueChanged(getValue());
    } else {
        QAbstractButton *btt = qobject_cast<QAbstractButton*> (m_field);
        btt->toggle();
    }

    m_readOnly = false;
}
