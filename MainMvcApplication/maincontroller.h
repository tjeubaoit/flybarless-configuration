#ifndef MAINCONTROLLER_H
#define MAINCONTROLLER_H

#include <QTimer>
#include "firmwareasker.h"

#define GYRO_OFF_DELAY 4
#define BIND_RX_DELAY 20
#define SET_CENTRE_DELAY 20
#define TAB_COUNTER_DELAY 20

#define MAX_ERROR 3

#define TIMER_UPDATE_UI 50
#define COUNTER_MAX_VALUE 200

#define MAX_TIME_WAIT_THREAD_DESTROY 1000

#define LOAD_PARAMS_LEVEL_ALL 2
#define LOAD_PROFILE_PARAMS_LEVEL 1


class SerialManager;
class AppConfigManager;
class ResourceManager;
class ParameterModel;
class AdaptorModel;

namespace Ui { class MainWidget; }

class MainController : public QObject
{
    Q_OBJECT
    Q_ENUMS(Action)
    Q_ENUMS(AppRunMode)

public:
    enum Action
    {
        Sleep = 0,
        Running = 1
    };

    enum AppRunMode
    {
        Online = 1,
        Offline = 0
    };

    explicit MainController(AppConfigManager* config);
    ~MainController();

signals:
    void _needUpdateFlash(const QByteArray&);
    void _needReconnect();

public slots:
    void showMainWindow();

    void handleParamsChanged(const QVariant&);
    void handlePortStatusChanged(int);

    void handleFlashStatusChanged(int);
    void handleFlashProgressChanged(int);

protected:
    bool eventFilter(QObject *obj, QEvent *event);

    void onConnect();
    void onDisconnect();

private slots:

    void buttonGroup_button_clicked(QObject*, int, bool*);
    void servoButtons_clicked(QObject*, bool, bool*);

    void cbTxMode_current_index_changed(int);
    void btnSetCenter_clicked();
    void btnBind_clicked();

    void btnSwashViewRotateLeft_clicked();
    void btnSwashViewRotateRight_clicked();

    void btnCyclicDefault_button_clicked();
    void btnTailDefault_button_clicked();

    void mainTab_current_changed(int);

    void btnOpen_button_clicked();
    void btnSave_button_clicked();
    void btnFirmware_button_clicked();
    void btnInfo_button_clicked();
    void btnWriteLoadedParams_button_clicked();
    void cbProfile_activated(int);
    void btnSaveProfileAs_clicked();

    void requestReadAllAddress();
    void mainTimer_timeout();
    void requestWriteConfigChanges(int, int = -1, bool = false);
    void requestSendGyroOff();

private:
    void intializeModels();
    void registerConnections();
    void setupUi();

    void changeAction(Action action);

    void loadApplicationConfig();
    void saveApplicationConfig();

    bool loadProfile(int profileIndex);
    void saveProfile(int profileIndex);

    Ui::MainWidget *ui;

    AppConfigManager *m_appConfig;
    ResourceManager *m_res;

    SerialManager* m_serialManager;

    AdaptorModel* m_adaptorModel;
    ParameterModel* m_paramModel;

    FirmwareAsker m_requestManager;

    Action m_currentAction;
    AppRunMode m_appMode;

    QTimer m_updateUiTimer;
    qint32 m_updateUiCounter;

    int m_currentProfile;
    int m_loadProfileLevel;

};

#endif // MAINCONTROLLER_H
