#include "infodialog.h"
#include "ui_infodialog.h"

InfoDialog::InfoDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::InfoDialog)
{
    ui->setupUi(this);
    setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog);
}

InfoDialog::~InfoDialog()
{
    delete ui;
}

void InfoDialog::on_btnClose_clicked()
{
    hide();
}
