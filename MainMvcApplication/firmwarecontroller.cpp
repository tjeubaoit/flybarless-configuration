#include "firmwarecontroller.h"
#include "flashhelper.h"
#include "serialmanager.h"
#include "resourcemanager.h"
#include "resources.h"
#include "mainwidget_ui.h"

#include <QtSerialPort/QSerialPort>
#include <QMessageBox>
#include <QThread>


FirmwareController::FirmwareController(Ui::MainWidget *ui, QObject *parent) :
    QObject(parent),
    m_res(ResourceManager::instance()),
    ui(ui)
{

}

void FirmwareController::startUpdateFirmware(SerialManager *serialManager,
                                             const QByteArray &data)
{
    FlashHelper flashHelper;
    flashHelper.setPortManager(serialManager);
    flashHelper.setFlashData(data);

    connect(&flashHelper, SIGNAL(_flashProgressChanged(int)),
            this, SLOT(handleFlashProgressChanged(int)));
    connect(&flashHelper, SIGNAL(_flashStatusChanged(int)),
            this, SLOT(handleFlashStatusChanged(int)));

    flashHelper.moveToThread(&m_thread);
    connect(&m_thread, SIGNAL(started()),
            &flashHelper, SLOT(updateFlash()));

    ui->showProgressDialog(ProgressDialog::RingType, Res(2));
    m_thread.start();
    m_eventLoop.exec();
}

void FirmwareController::handleFlashStatusChanged(int status)
{
    switch (status)
    {
    case FlashHelper::PortCannotOpen:
        ui->hideProgressDialog();
        QMessageBox::critical(ui, Res(109), Res(103), QMessageBox::Ok);
        return;

    case FlashHelper::StartDownloadNewFirmware:
        ui->updateLabelFlashStatus(Res(101));
        ui->progressDialog->changeState(ProgressDialog::BarType);
        break;

    case FlashHelper::DownloadFirmwareError:
        ui->hideProgressDialog();
        QMessageBox::critical(ui, Res(109), Res(102), QMessageBox::Ok);
        return;

    case FlashHelper::StartEraseFlash:
        ui->updateLabelFlashStatus(Res(104));
        ui->progressDialog->changeState(ProgressDialog::RingType);
        break;

    case FlashHelper::StartWriteFirmware:
        ui->progressDialog->changeState(ProgressDialog::BarType);
        ui->updateLabelFlashStatus(Res(105));
        break;

    case FlashHelper::StartReconnect:
        ui->progressDialog->changeState(ProgressDialog::RingType);
        ui->updateLabelFlashStatus(Res(106));
        break;

    case FlashHelper::FlashResultError:
        ui->hideProgressDialog();
        QMessageBox::critical(ui, Res(109), Res(108), QMessageBox::Ok);
        return;

    case FlashHelper::FlashResultOk:
        ui->hideProgressDialog();
        QMessageBox::information(ui, Res(109), Res(107), QMessageBox::Ok);
        return;
    }
}

void FirmwareController::handleFlashProgressChanged(int percent)
{
    ui->progressDialog->setProgressBarValue(percent);
    ui->progressDialog->repaint();
}

void FirmwareController::flashUpdateFinish()
{
    m_thread.quit();
    m_eventLoop.exit(0);
}
