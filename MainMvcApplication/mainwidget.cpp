﻿#include "mainwidget.h"
#include "mainwidget_ui.h"
#include "global.h"
#include "resources.h"
#include "address.h"
#include "vtekinterface.h"
#include "serialmanager.h"
#include "filehelper.h"
#include "flashhelper.h"
#include "firmwaredialog.h"
#include "connectionhelper.h"
#include "appconfigmanager.h"
#include "resourcemanager.h"

#include <QMessageBox>
#include <QToolTip>
#include <QFileDialog>
#include <QKeyEvent>

#ifdef FLYBARLESS_DEBUG
#   include <QDebug>
#endif

static const QString FBL_Addr_List[MAX_TAB_COUNT] =
{
    ":0121,0122",
    "0111,0112,0113,0114,0115,0116,0117:0101",
    ":0127",
    ":0203,0124,0209,0210,0211",
    "0217,0218,0219,0220:0204,0205,0206,0207,0132,0133,0134,0131,0125,0222,0223",
    "0217,0218,0219,0220:0136,0137,0138,0139,0140",
    ":0301,0302,0300,0305,0306,0310,0311,0312,0313",
    ":0126,0135,0207,0212,0216,0303,0304,0314,0315,0316,0317,0318,0319",
    ":0511,0512,0520,0521,0522,0523,0524,0525,0526,0527,0528,0529"
};

MainWidget::MainWidget(AppConfigManager *config)
    : ui(new Ui::MainWidget),
      m_appConfig(config),
      m_res(ResourceManager::instance()),
      m_serialManager(new SerialManager),
      m_currentAction(Sleep),
      m_tabUpdateCounter(new qint32[MAX_TAB_COUNT]),
      m_errorCounter(0),
      m_currentProfile(0),
      m_LoadProfileLevel(0),
      m_applicationMode(Online)
{
    this->registerConnections();
    this->intializeWidgetHashValues();
    this->loadApplicationConfig();

    m_updateUiTimer.setSingleShot(false);
    m_updateUiTimer.setInterval(TIMER_UPDATE_UI);

    ui->mainTab->setCurrentIndex(Ui::MainWidget::TabStart);
    ui->updateLabelPortStatus(false);

    m_serialManager->setHelloPairString(HELLO_REQUEST, HELLO_RESPONSE);
    m_serialManager->addProductId(PRODUCT_ID);
    m_serialManager->addVendorId(VENDOR_ID);
    m_serialManager->start(QThread::HighPriority);
}

MainWidget::~MainWidget()
{
    changeAction(Sleep);
    saveApplicationConfig();
    saveProfile(m_currentProfile);

    if (m_serialManager->isRunning()) {
        m_serialManager->quit();
        m_serialManager->wait(MAX_TIME_WAIT_THREAD_DESTROY);
    }
    m_serialManager->deleteLater();

    delete[] m_tabUpdateCounter;

    delete ui;
}

void MainWidget::show()
{
    ui->show();
}

bool MainWidget::eventFilter(QObject *obj, QEvent *event)
{
    if (qobject_cast<QLineEdit*> (obj)) {
        QLineEdit *edit = qobject_cast<QLineEdit*> (obj);
        if (event->type() == QEvent::FocusOut) {
            if (edit->isModified()) {
                edit->setText(m_tmpLineEditText);
                ui->hideLineEditAlert();
                m_stableWidgets.remove(edit);
            }
       }
        else if (event->type() == QEvent::FocusIn) {
            m_tmpLineEditText = edit->text();
        }       
    }
    else if (event->type() == QEvent::MouseButtonPress) {
        QWidget *widget =  QApplication::focusWidget();
        if (widget) {
            widget->clearFocus();
        }
    }

    return false;
}

void MainWidget::onConnect()
{
    requestReadAllAddress();

    ui->updateLabelPortStatus(true);
    changeAction(Running);

    m_errorCounter = 0;
}

void MainWidget::onDisconnect()
{
    ui->updateLabelPortStatus(false);
    changeAction(Sleep);
}

void MainWidget::handleParamsChanged(const QVariant& data)
{
    m_errorCounter = (m_errorCounter > 0) ? (m_errorCounter - 1) : 0;

    QStringList list = data.toString().split(',');
    qint32 addr = list.first().toInt();
    qint32 value = list.last().toInt();

    updateParamsCache(addr, value);

    // truong hop dac biet voi 2 combo box rx type
    if (addr == ADDR_RX_TYPE && (value == Ui::MainWidget::DSM2
                                 || value == Ui::MainWidget::DSMX)) {
        if (!isStableWidget(ui->cbReceiverType, ADDR_RX_TYPE)) {
            ui->cbReceiverType->setCurrentIndex(
                        ui->cbReceiverType->count() - 1);
            int satelliteIndex = (value == Ui::MainWidget::DSM2) ? 0 : 1;
            ui->cbSatelliteType->setCurrentIndex(satelliteIndex);
        }
    }
    else if (addr == ADDR_GEAR_RATIO_LEFT) {
        if (!isStableWidget(ui->editGearRatio, ADDR_GEAR_RATIO_LEFT)) {
            int left = 0, right = 0;
            textToDouble(ui->editGearRatio->text(), left, right);
            QString text = QString::number(right);
            FILL(text , 2);
            ui->setLineEditText(
                        ui->editGearRatio, QString::number(value) +"."+ text);
        }
    }
    else if (addr == ADDR_GEAR_RATIO_RIGHT) {
        if (!isStableWidget(ui->editGearRatio, ADDR_GEAR_RATIO_RIGHT)) {
            int left = 0, right = 0;
            textToDouble(ui->editGearRatio->text(), left, right);
            QString text = QString::number(value);
            FILL(text, 2);
            ui->setLineEditText(
                        ui->editGearRatio, QString::number(left) +"."+ text);
        }
    }
    else if (addr == ADDR_MAX_HEADSPEED) {
        if (!isStableWidget(ui->editMaxHeadspeed, ADDR_MAX_HEADSPEED)) {
            ui->setLineEditText(
                        ui->editMaxHeadspeed, QString::number(value * 10));
        }
    }
    else if (addr == ADDR_GOVERNOR_MOTOR_OFF) {
        if (!isStableWidget(ui->sliderGovernorMotorOff, ADDR_GOVERNOR_MOTOR_OFF)) {
            ui->sliderGovernorMotorOff->setValue(value * (-1));
        }
    }
    else {
        QObject *obj = m_widgetIdHash.value(addr);
        if (!isStableWidget(obj, addr)) {
            VtekInterface *inf = VtekInterface::createVtekObject(obj);
            if (inf != 0) {
                inf->setValue(data);
                delete inf;
            }
        }
    }
}

void MainWidget::handlePortStatusChanged(int status)
{
    switch (status) {
    case SerialManager::PortConnected:
        this->onConnect();
        break;

    case SerialManager::PortDisconneted:
        this->onDisconnect();
        break;
    }
}

void MainWidget::rxView_sensor_value_changed(qint32 x, qint32 y)
{
    QString xStr, yStr;

    if (x < RX_MIN_VAL || x > RX_MAX_VAL) {
        xStr = "∞";
    }
    else {
        xStr = QString::number(x - RX_CENTER_VAL);
    }

    if (y < RX_MIN_VAL || y > RX_MAX_VAL) {
        yStr = "∞";
    }
    else {
        yStr = QString::number(y - RX_CENTER_VAL);
    }

    RxMonitorView *view = qobject_cast<RxMonitorView*> (sender());
    if (view->getLocation() == RX_LOCATION_LEFT) {
        ui->lbRxLeft_x->setText(xStr);
        ui->lbRxLeft_y->setText(yStr);
    }
    else if (view->getLocation() == RX_LOCATION_RIGHT) {
        ui->lbRxRight_x->setText(xStr);
        ui->lbRxRight_y->setText(yStr);
    }
}

void MainWidget::requestReadValuesOnTab()
{
    // check fbl device is connected before request send
    if (m_errorCounter > MAX_ERROR) {
        emit _needReconnect();
        return;
    }

    this->requestSendGyroOff();

    int tabIndex = ui->mainTab->currentIndex();
    // check permisson send request read params by a counter
    if (m_updateUiCounter < 0) {
        m_updateUiCounter ++;
        return;
    }
    else if (tabIndex == Ui::MainWidget::TabRx) {
        if (!ui->rxViewLeft->animFinished()
                || !ui->rxViewRight->animFinished()) {

            return;
        }
    }

    QStringList cmdList = FBL_Addr_List[tabIndex].split(":");
    if (m_tabUpdateCounter[tabIndex] % TAB_COUNTER_DELAY == 0) {
        // moi lan gui 1 request (moi 1s) tang error counter len 1
        // moi lan nhan response phan hoi
        // (trong ham handleFirmwareParamsChange)
        // se giam error counter di 1
        // neu error counter > max_error thi coi nhu disconnect
        m_errorCounter++;
        m_serialManager->readMultipleAddress(cmdList.last());
    }
    m_serialManager->readMultipleAddress(cmdList.first());

    this->updateTabCounter(tabIndex);
}

void MainWidget::requestReadAllAddress()
{
    for (qint8 i = 0; i < MAX_TAB_COUNT; i++) {
        QStringList list = FBL_Addr_List[i].split(':');

        m_serialManager->readMultipleAddress(list.first());
        m_serialManager->readMultipleAddress(list.last());
    }
}

void MainWidget::requestWriteConfigChanges()
{
    QObject *widget = sender();
    if (widget) {
        requestWriteConfigChanges(widget);
    }
}

void MainWidget::requestWriteConfigChanges(QObject *widget, int key,
                                           int val, bool isBlocking)
{
    // if key don't exist, try to found
    if (key < 0) { // default value = -1
        key = findKeyByObject(widget);
    }
    // if value don't exist, try to found
    if (key > 0 && val < 0) { // default value = -1
        VtekInterface *inf = VtekInterface::createVtekObject(widget);
        if (inf) {
            val = inf->getValue(key).toInt();
            delete inf;
        }
    }
    if (key > 0 && val >= 0) {
        updateParamsCache(key, val);
        if (m_currentAction == Sleep) {
            return;
        }

        // insert addr that has changed to write cache
        m_waitToWriteKeys.insert(key);
        QString text = QString::number(key) + "," + QString::number(val);

        // clear all signals posted in queue of SerialWriter
        m_serialManager->clearAllSignalsInReader();


        if (isBlocking) {
            m_serialManager->writeConfigBlocking(text);
        } else {
            m_serialManager->writeConfig(text);
        }
    }
}

void MainWidget::requestWriteConfigChanges(QObject *objToWrite, bool isBlocking)
{
    requestWriteConfigChanges(objToWrite, -1, -1, isBlocking);
}

void MainWidget::requestSendGyroOff()
{
    static int gyroOffCounter = 0;
    int tabIndex = ui->mainTab->currentIndex();

    // check send request gyro off
    if (ui->checkGyroOff->isChecked()
            && (gyroOffCounter % GYRO_OFF_DELAY == 0)) {

        if (tabIndex >= 3 && tabIndex <= 7)
            emit requestWriteConfigChanges(
                    ui->checkGyroOff, ADDR_CHECK_GYRO_OFF, 1);
    }

    if (++gyroOffCounter >= COUNTER_MAX_VALUE) {
        gyroOffCounter = 1;
    }
}

void MainWidget::mainTab_current_changed(int index)
{
    m_updateUiCounter = 0; // khi chuyen tab, reset bo dem ve 0

    // xoa cac signal chua thuc hien trong queue cua SerialWriter
    m_serialManager->clearAllSignalsInReader();

    m_stableWidgets.clear();

    ui->animationWhenTabChange(index);
    ui->animationShowGyroOff();
}

void MainWidget::sliderPressed()
{
    m_stableWidgets.insert(sender());
}

void MainWidget::sliderReleased()
{
    m_stableWidgets.remove(sender());
    if (sender() == ui->sliderGovernorMotorOff) {
        requestWriteConfigChanges(ui->sliderGovernorMotorOff,
                                  ADDR_GOVERNOR_MOTOR_OFF,
                                  ui->sliderGovernorMotorOff->value() * (-1));
    }
    else requestWriteConfigChanges(sender());
}

void MainWidget::slider_action_triggered(int action)
{
    if (action >= 1 && action <= 4) {
        m_stableWidgets.insert(sender());
    }
}

void MainWidget::slider_value_changed(int value)
{
    QSlider *slider = qobject_cast<QSlider*> (sender());

    if (m_stableWidgets.contains(slider) && !slider->isSliderDown()) {
        if (slider == ui->sliderGovernorMotorOff) {
            requestWriteConfigChanges(slider,
                                      ADDR_GOVERNOR_MOTOR_OFF, value * (-1));
        } else {
            requestWriteConfigChanges(slider);
        }
        m_stableWidgets.remove(slider);
    }

    // tab rx

    if (slider == ui->sliderRx1) {
        ui->lbSlider1_ms->setText(QString::number(value) + " µs");
    }
    else if (slider == ui->sliderRx2) {
        ui->lbSlider2_ms->setText(QString::number(value) + " µs");
    }
    else if (slider == ui->sliderRx3) {
        ui->lbSlider3_ms->setText(QString::number(value) + " µs");
    }

    // tab collective

    else if (slider == ui->sliderCollectiveTravel) {
        ui->editCollectiveTravel->setText(QString::number(value));
        ui->changeColorWhenSliderExceedIdealValue(slider);
    }

    // tab cyclic

    else if (slider == ui->sliderCyclicLimit) {
        ui->editCyclicLimit->setText(QString::number(value));
        ui->changeColorWhenSliderExceedIdealValue(slider);
    }
    else if (slider == ui->sliderCyclicGain) {
        ui->editCyclicGain->setText(QString::number(value));
    }
    else if (slider == ui->sliderCyclicReponse) {
        ui->editCyclicResponse->setText(QString::number(value));
    }
    else if (slider == ui->sliderCyclicFlip) {
        ui->editCyclicFlip->setText(QString::number(value));
    }
    else if (slider == ui->sliderCyclicRoll) {
        ui->editCyclicRoll->setText(QString::number(value));
    }

    // tab tail

    else if (slider == ui->sliderTailTravel1) {
        ui->editTailTravel1->setText(QString::number(value));
        ui->changeColorWhenSliderExceedIdealValue(slider);
    }
    else if (slider == ui->sliderTailTravel2) {
        ui->editTailTravel2->setText(QString::number(value));
        ui->changeColorWhenSliderExceedIdealValue(slider);
    }
    else if (slider == ui->sliderTailYaw) {
        ui->editTailYaw->setText(QString::number(value));
    }
    else if (slider == ui->sliderTailGain) {
        ui->editTailGain->setText(QString::number(value));
    }

    // tab governor

    else if (slider == ui->sliderGovernoMaxThrottle) {
        ui->editGovernorMaxThrottle->setText(QString::number(value));
    }
    else if (slider == ui->sliderGovernorMotorOff) {
        ui->editGovernorMotorOff->setText(QString::number(value));
    }
}

void MainWidget::lineEdit_text_edited(const QString &)
{
    QLineEdit *edit = qobject_cast<QLineEdit*> (sender());
    m_stableWidgets.insert(edit);

    QPoint point(-120, edit->height() + 2);
    ui->showLineEditAlert(edit->mapTo(ui, point));
}

void MainWidget::lineEdit_return_pressed()
{
    QLineEdit* line = qobject_cast<QLineEdit*> (sender());
    line->setModified(false);
    m_tmpLineEditText = line->text();

    if (line != NULL) {
        int val = line->text().toInt();

        // tab collective

        if (line == ui->editCollectiveTravel) {
            ui->sliderCollectiveTravel->setValue(val);
            requestWriteConfigChanges(ui->sliderCollectiveTravel);
        }

        // tab cyclic

        else if (line == ui->editCyclicLimit) {
            ui->sliderCyclicLimit->setValue(val);
            requestWriteConfigChanges(ui->sliderCyclicLimit);
        }
        else if (line == ui->editCyclicGain) {
            ui->sliderCyclicGain->setValue(val);
            requestWriteConfigChanges(ui->sliderCyclicGain);
        }
        else if (line == ui->editCyclicResponse) {
            ui->sliderCyclicReponse->setValue(val);
            requestWriteConfigChanges(ui->sliderCyclicReponse);
        }
        else if (line == ui->editCyclicRoll) {
            ui->sliderCyclicRoll->setValue(val);
            requestWriteConfigChanges(ui->sliderCyclicRoll);
        }
        else if (line == ui->editCyclicFlip) {
            ui->sliderCyclicFlip->setValue(val);
            requestWriteConfigChanges(ui->sliderCyclicFlip);
        }

        // tab tail

        else if (line == ui->editTailTravel1) {
            ui->sliderTailTravel1->setValue(val);
            requestWriteConfigChanges(ui->sliderTailTravel1);
        }
        else if (line == ui->editTailTravel2) {
            ui->sliderTailTravel2->setValue(val);
            requestWriteConfigChanges(ui->sliderTailTravel2);
        }
        else if (line == ui->editTailYaw) {
            ui->sliderTailYaw->setValue(val);
            requestWriteConfigChanges(ui->sliderTailYaw);
        }
        else if (line == ui->editTailGain) {
            ui->sliderTailGain->setValue(val);
            requestWriteConfigChanges(ui->sliderTailGain);
        }

        // tab governor

        else if (line == ui->editGovernorMaxThrottle) {
            ui->sliderGovernoMaxThrottle->setValue(val);
            requestWriteConfigChanges(ui->sliderGovernoMaxThrottle);
        }
        else if (line == ui->editGovernorMotorOff) {
            ui->sliderGovernorMotorOff->setValue(val);
            requestWriteConfigChanges(ui->sliderGovernorMotorOff,
                                      ADDR_GOVERNOR_MOTOR_OFF, val * (-1));
        }
        else if (line == ui->editMaxHeadspeed) {
            val = qRound((double)val / 10);
            requestWriteConfigChanges(
                        ui->editMaxHeadspeed, ADDR_MAX_HEADSPEED, val);
            line->setText(QString::number(val * 10));
        }
        else if (line == ui->editGearRatio) {
            int left = 0, right = 0;
            textToDouble(line->text(), left, right);

            requestWriteConfigChanges(
                        ui->editGearRatio, ADDR_GEAR_RATIO_LEFT, left, true);
            requestWriteConfigChanges(
                        ui->editGearRatio, ADDR_GEAR_RATIO_RIGHT, right, true);
        }

        // voi QLineEdit don le thi gui yeu cau write
        else {
            requestWriteConfigChanges(line);
        }
    }

    ui->hideLineEditAlert();
    m_stableWidgets.remove(line);
}

void MainWidget::buttonGroup_button_clicked(int id)
{
    ButtonGroup *btGroup = qobject_cast<ButtonGroup*> (sender());
    QAbstractButton *button = btGroup->button(id);
    if (btGroup->lastButtonCheckedId() == id) {
        return;
    }
    m_stableWidgets.insert(btGroup);

    QString msgText;
    if (button == ui->btnSensorDirection1) msgText = Res(130);
    else if (button == ui->btnSensorDirection2) msgText = Res(131);
    else if (button == ui->btnSensorDirection3) msgText = Res(132);
    else if (button == ui->btnSensorDirection4) msgText = Res(133);
    else if (button == ui->btnSensorDirection5) msgText = Res(134);
    else if (button == ui->btnSensorDirection6) msgText = Res(135);
    else if (button == ui->btnSensorDirection7) msgText = Res(136);
    else if (button == ui->btnSensorDirection8) msgText = Res(137);

    else if (button == ui->btnSwashplate1) msgText = Res(140);
    else if (button == ui->btnSwashplate2) msgText = Res(141);
    else if (button == ui->btnSwashplate3) msgText = Res(142);
    else if (button == ui->btnSwashplate4) msgText = Res(143);
    else if (button == ui->btnSwashplate5) msgText = Res(144);

    else if (button == ui->btnRotateDirection1) msgText = Res(146);
    else if (button == ui->btnRotateDirection2) msgText = Res(147);

    else if (button == ui->btnCollectiveDirection1) msgText = Res(160);
    else if (button == ui->btnCollectiveDirection2) msgText = Res(161);

    QString msgTitle;
    if (btGroup == ui->btGroupSensorDirection) msgTitle = Res(138);
    else if (btGroup == ui->btGroupSwashplate) msgTitle = Res(145);
    else if (btGroup == ui->btGroupCollectDirection) msgTitle = Res(162);
    else if (btGroup == ui->btGroupRotateDirection) msgTitle = Res(148);

    int ret = QMessageBox::question(ui, msgTitle, msgText,
                                    QMessageBox::Ok, QMessageBox::Cancel);
    if (QMessageBox::Ok == ret) {
        btGroup->setLastButtonChecked(btGroup->button(id));
        requestWriteConfigChanges(btGroup);

        if (btGroup == ui->btGroupSwashplate) {
            ui->swashView->setAngle(0);
            requestWriteConfigChanges(ui->swashView);
        }
    }
    else {
        if (btGroup->lastButtonChecked() != NULL) {
            btGroup->lastButtonChecked()->setChecked(true);
        }
    }
    m_stableWidgets.remove(btGroup);
}

void MainWidget::checkableToolButton_clicked(bool check)
{
    QToolButton *btt = qobject_cast<QToolButton*> (sender());
    m_stableWidgets.insert(btt);

    QString msgTitle = Res(152);
    QString msgText = (check) ? Res(150) : Res(151);
    if (btt == ui->btnTailServo) {
        msgTitle = Res(182);
        msgText = (check) ? Res(180) : Res(181);
    }

    int ret = QMessageBox::question(ui, msgTitle, msgText,
                                    QMessageBox::Ok, QMessageBox::Cancel);
    if (QMessageBox::Ok == ret) {
        requestWriteConfigChanges(btt);
    } else btt->toggle();
    m_stableWidgets.remove(btt);
}

void MainWidget::cbReceiverType_activated(int)
{
    if (ui->cbReceiverType->currentData().toInt() != Ui::MainWidget::Satellite) {
        requestWriteConfigChanges(ui->cbReceiverType);
    }
    else {
        qint32 key = ADDR_RX_TYPE;
        qint32 val = ui->cbSatelliteType->currentData().toInt();
        requestWriteConfigChanges(ui->cbSatelliteType, key, val);
    }
}

void MainWidget::btnSetCenter_clicked()
{
    qint32 key = ADDR_RX_SETCENTER;
    qint32 val = 1;
    this->requestWriteConfigChanges(ui->btnSetCenter, key, val);
    m_updateUiCounter = -SET_CENTRE_DELAY;

    ui->showProgressDialog(ProgressDialog::RingType, Res(2), Res(122));
    QTimer::singleShot(SET_CENTRE_DELAY * TIMER_UPDATE_UI,
                       ui, SLOT(hideProgressDialog()));
}

void MainWidget::btnBind_clicked()
{
    int ret = QMessageBox::warning(ui, Res(120), Res(121),
                             QMessageBox::Yes, QMessageBox::No);
    if (ret == QMessageBox::No) {
        return;
    }

    qint32 key = ADDR_RX_BIND;
    qint32 val =
            (Ui::MainWidget::DSM2 == ui->cbSatelliteType->currentData().toInt())
            ? 3 : 9;
    this->requestWriteConfigChanges(ui->btnBind, key, val);
    m_updateUiCounter = -BIND_RX_DELAY;
}

void MainWidget::swashView_angle_changed(const QList<int>& list)
{
    ui->servoView->setAngle(list.at(0));

    ui->lbAngleCh1->setText(QString::number(list.at(1)));
    ui->lbAngleCh2->setText(QString::number(list.at(2)));
    ui->lbAngleCh3->setText(QString::number(list.at(3)));
    ui->lbAngleCh4->setText(QString::number(list.at(4)));
}

void MainWidget::btnSwashViewRotateLeft_clicked()
{
    ui->swashView->rotateLeft();
    requestWriteConfigChanges(ui->swashView);
}

void MainWidget::btnSwashViewRotateRight_clicked()
{
    ui->swashView->rotateRight();
    requestWriteConfigChanges(ui->swashView);
}

void MainWidget::trimSwashCyclic_value_changed(int type, int direction,
                                               int val, bool isBlocking)
{
    if (type == SwashTrimCyclic::Cyclic) {
        if (direction == SwashTrimCyclic::Horizontal) {
            requestWriteConfigChanges(
                        ui->trimSwashCyclic, ADDR_SWASH_TRIM_X,
                        val + 100, isBlocking);
        }
        else if (direction == SwashTrimCyclic::Vertical) {
            requestWriteConfigChanges(
                        ui->trimSwashCyclic, ADDR_SWASH_TRIM_Y,
                        val + 100, isBlocking);
        }
    }
    else if (type == SwashTrimCyclic::MaxCollective) {
        if (direction == SwashTrimCyclic::Horizontal) {
            requestWriteConfigChanges(
                    ui->trimSwashCyclic, ADDR_SWASH_TRIM_MAX_COL_X,
                    val + 100, isBlocking);
        }
        else if (direction == SwashTrimCyclic::Vertical) {
            requestWriteConfigChanges(
                    ui->trimSwashCyclic, ADDR_SWASH_TRIM_MAX_COL_Y,
                    val + 100, isBlocking);
        }
    }
    else if (type == SwashTrimCyclic::MinCollective) {
        if (direction == SwashTrimCyclic::Horizontal) {
            requestWriteConfigChanges(
                    ui->trimSwashCyclic, ADDR_SWASH_TRIM_MIN_COL_X,
                    val + 100, isBlocking);
        }
        else if (direction == SwashTrimCyclic::Vertical) {
            requestWriteConfigChanges(
                    ui->trimSwashCyclic, ADDR_SWASH_TRIM_MIN_COL_Y,
                    val + 100, isBlocking);
        }
    }
}

void MainWidget::trimSwashCollective_value_changed(int val)
{
    requestWriteConfigChanges(
                ui->trimSwashCollective, ADDR_COLLECTIVE_TRIM, val + 100);
}

void MainWidget::btnCyclicDefault_button_clicked()
{
    int ret = QMessageBox::warning(ui, Res(80), Res(170),
                                    QMessageBox::Ok, QMessageBox::Cancel);
    if (ret == QMessageBox::Cancel)
        return;

    if (sender() == ui->btnCyclicDefault) {
        ui->sliderCyclicLimit->setValue(100);
        ui->sliderCyclicGain->setValue(100);
        ui->sliderCyclicReponse->setValue(100);
        ui->sliderCyclicRoll->setValue(250);
        ui->sliderCyclicFlip->setValue(250);

        requestWriteConfigChanges(
                    ui->sliderCyclicLimit, ADDR_CYCLIC_LIMIT, 100, true);
        requestWriteConfigChanges(
                    ui->sliderCyclicGain, ADDR_CYCLIC_GAIN, 100, true);
        requestWriteConfigChanges(
                    ui->sliderCyclicReponse, ADDR_CYCLIC_AGILITY, 100, true);
        requestWriteConfigChanges(
                    ui->sliderCyclicRoll, ADDR_CYCLIC_ROLL, 250, true);
        requestWriteConfigChanges(
                    ui->sliderCyclicFlip, ADDR_CYCLIC_FLIP, 250, true);
    }
    else {
        ui->editRcDeadband->setText("10");
        ui->editElevatorPrecom->setText("50");
        ui->editPddleSimulate->setText("50");
        ui->editCyclicExpo->setText("0");

        requestWriteConfigChanges(
                    ui->editRcDeadband, ADDR_RC_DEADBAND, 10, true);
        requestWriteConfigChanges(
                    ui->editElevatorPrecom, ADDR_ELEVATOR_PRECOM, 50, true);
        requestWriteConfigChanges(
                    ui->editPddleSimulate, ADDR_PADDLE_SIMULATE, 50, true);
        requestWriteConfigChanges(
                    ui->editCyclicExpo, ADDR_CYCLIC_EXPO, 0, true);
    }
}

void MainWidget::btnTailDefault_button_clicked()
{

    int ret = QMessageBox::warning(ui, Res(80), Res(183),
                                    QMessageBox::Ok, QMessageBox::Cancel);
    if (ret == QMessageBox::Cancel)
        return;

    ui->sliderTailYaw->setValue(500);
    ui->sliderTailGain->setValue(100);

    ui->editTailAccelerate->setText("50");
    ui->editTailStopGain->setText("50");
    ui->editTailExpo->setText("0");
    ui->editTailCollective->setText("50");
    ui->editTailCyclic->setText("50");
    ui->editTailZeroCol->setText("50");

    requestWriteConfigChanges(
                ui->sliderTailYaw, ADDR_TAIL_YAW, 500, true);
    requestWriteConfigChanges(
                ui->sliderTailGain, ADDR_TAIL_GAIN, 100, true);
    requestWriteConfigChanges(
                ui->editTailAccelerate, ADDR_TAIL_ACELERATION, 50, true);
    requestWriteConfigChanges(
                ui->editTailStopGain, ADDR_TAIL_STOPGAIN, 50, true);
    requestWriteConfigChanges(
                ui->editTailExpo, ADDR_TAIL_EXPO, 0, true);
    requestWriteConfigChanges(
                ui->editTailCollective, ADDR_TAIL_COLLECTIVE, 50, true);
    requestWriteConfigChanges(
                ui->editTailCyclic, ADDR_TAIL_CYCLIC, 50, true);
    requestWriteConfigChanges(
                ui->editTailZeroCol, ADDR_TAIL_ZEROCOL, 50, true);
}

void MainWidget::cbTxMode_current_index_changed(int index)
{
    switch (index) {
    case 0:
        m_widgetIdHash.insert(ADDR_RX_ELEVATOR, ui->rxViewLeft);
        m_widgetIdHash.insert(ADDR_RX_RUDDER, ui->rxViewLeft);
        m_widgetIdHash.insert(ADDR_RX_PITCH, ui->rxViewRight);
        m_widgetIdHash.insert(ADDR_RX_AILERONS, ui->rxViewRight);
        break;

    case 1:
        m_widgetIdHash.insert(ADDR_RX_PITCH, ui->rxViewLeft);
        m_widgetIdHash.insert(ADDR_RX_RUDDER, ui->rxViewLeft);
        m_widgetIdHash.insert(ADDR_RX_ELEVATOR, ui->rxViewRight);
        m_widgetIdHash.insert(ADDR_RX_AILERONS, ui->rxViewRight);
        break;

    case 2:
        m_widgetIdHash.insert(ADDR_RX_ELEVATOR, ui->rxViewLeft);
        m_widgetIdHash.insert(ADDR_RX_AILERONS, ui->rxViewLeft);
        m_widgetIdHash.insert(ADDR_RX_PITCH, ui->rxViewRight);
        m_widgetIdHash.insert(ADDR_RX_RUDDER, ui->rxViewRight);
        break;

    case 3:
        m_widgetIdHash.insert(ADDR_RX_PITCH, ui->rxViewLeft);
        m_widgetIdHash.insert(ADDR_RX_AILERONS, ui->rxViewLeft);
        m_widgetIdHash.insert(ADDR_RX_ELEVATOR, ui->rxViewRight);
        m_widgetIdHash.insert(ADDR_RX_RUDDER, ui->rxViewRight);
        break;

    default:
        return;
    }
}

void MainWidget::handleFlashStatusChanged(int status)
{
    switch (status)
    {
    case FlashHelper::PortCannotOpen:
        ui->hideProgressDialog();
        QMessageBox::critical(ui, Res(109), Res(103), QMessageBox::Ok);
        return;

    case FlashHelper::StartDownloadNewFirmware:
        ui->updateLabelFlashStatus(Res(101));
        ui->progressDialog->changeState(ProgressDialog::BarType);
        break;

    case FlashHelper::DownloadFirmwareError:
        ui->hideProgressDialog();
        QMessageBox::critical(ui, Res(109), Res(102), QMessageBox::Ok);
        return;

    case FlashHelper::StartEraseFlash:
        ui->updateLabelFlashStatus(Res(104));
        ui->progressDialog->changeState(ProgressDialog::RingType);
        ui->progressDialog->setTitle(Res(2));
        break;

    case FlashHelper::StartWriteFirmware:
        ui->updateLabelFlashStatus(Res(105));
        ui->progressDialog->changeState(ProgressDialog::BarType);
        break;

    case FlashHelper::StartReconnect:
        ui->updateLabelFlashStatus(Res(106));
        ui->progressDialog->changeState(ProgressDialog::RingType);
        ui->progressDialog->setTitle(Res(2));
        break;

    case FlashHelper::FlashResultError:
        ui->hideProgressDialog();
        QMessageBox::critical(ui, Res(109), Res(108), QMessageBox::Ok);
        return;

    case FlashHelper::FlashResultOk:
        ui->hideProgressDialog();
        QMessageBox::information(ui, Res(109), Res(107), QMessageBox::Ok);
        return;
    }
}

void MainWidget::handleFlashProgressChanged(int percent)
{
    ui->progressDialog->setProgressBarValue(percent);
}

void MainWidget::updateSoftware()
{

}

void MainWidget::btnOpen_button_clicked()
{
    QString dir = m_appConfig->get("last_params_dir");
    if (dir.isEmpty()) {
        dir = QCoreApplication::applicationDirPath();
    }
    QString filePath = QFileDialog::getOpenFileName(ui, Res(10), dir, Res(8));

    if (! filePath.isNull() && ! filePath.isEmpty()) {
        dir = filePath.mid(0, filePath.lastIndexOf("/"));
        m_appConfig->put("last_params_dir", dir);

        qint32 ret = qLoadParametersFromFile(filePath, &m_fblParamsCache);
        if (ret != 0) {
            QMessageBox::critical(ui, Res(10), Res(12), QMessageBox::Ok);
            return;
        } else {
            if (m_applicationMode == Online && m_serialManager->isPortConnected()) {
                ret = QMessageBox::warning(ui, Res(17), Res(19),
                                     QMessageBox::Ok, QMessageBox::Cancel);
                if (ret == QMessageBox::Cancel) return;
            }
        }

        if (m_serialManager->isPortConnected()) {
            m_LoadProfileLevel = LOAD_PARAMS_LEVEL_ALL;
            changeAction(Sleep);
            ui->animationWhenLoadFirmwareParams(true);
        }

        foreach (qint32 key, m_fblParamsCache.keys()) {
            qint32 value = m_fblParamsCache.value(key);
            handleParamsChanged(QString::number(key) + "," + QString::number(value));
        }
    }
}

void MainWidget::btnSave_button_clicked()
{
    QString dir = m_appConfig->get("last_params_dir");
    if (dir.isEmpty()) {
        dir = QCoreApplication::applicationDirPath();
    }
    QString filePath = QFileDialog::getSaveFileName(ui, Res(11), dir, Res(8));

    if (! filePath.isNull() && ! filePath.isEmpty()) {
        dir = filePath.mid(0, filePath.lastIndexOf("/"));
        m_appConfig->put("last_params_dir", dir);

        qint8 ret = qSaveParametersToFile(filePath, m_fblParamsCache);
        if (ret != 0) {
            qDebug() << "save configuration values to file error";
        }
    }
}

void MainWidget::btnFirmware_button_clicked()
{
    FirmwareDialog fd(ui);
    int result = fd.exec();

    if (result == QDialog::Accepted) {
        QString dir = m_appConfig->get("last_firmware_dir");
        if (dir.isEmpty()) {
            dir = QCoreApplication::applicationDirPath();
        }
        QString filePath = QFileDialog::getOpenFileName(ui, Res(4), dir, Res(5));

        // check return if selected file not exist or cannot open
        QFile file(filePath);
        if (!file.exists()) return;
        if (!file.open(QIODevice::ReadOnly)) return;

        // save last directory path and selected file name to cache
        dir = filePath.mid(0, filePath.lastIndexOf("/"));
        m_appConfig->put("last_firmware_dir", dir);

        if (m_serialManager->isPortConnected()) {
            this->onDisconnect();
        }

        emit _needUpdateFlash(file.readAll());
        ui->showProgressDialog(ProgressDialog::RingType, Res(2));
    }
}

void MainWidget::btnInfo_button_clicked()
{

}

void MainWidget::btnWriteLoadedParams_button_clicked()
{
    QAbstractButton *button = qobject_cast<QAbstractButton*> (sender());
    if (button == ui->btnAcceptWriteLoadedParams) {
        double percent = 0;
        double maxPercent = m_fblParamsCache.keys().size();

        ui->showProgressDialog(ProgressDialog::BarType, Res(9), Res(9));

        foreach (const qint32 key, m_fblParamsCache.keys()) {
            percent ++;
            ui->progressDialog->setProgressBarValue(
                        (int)(percent / maxPercent * 100));

            // process events in queue to update UI of progress dialog
            QApplication::processEvents();

            if (m_LoadProfileLevel == LOAD_PARAMS_LEVEL_PROFILE) {
                switch (key) {
                // only some params need to save with profile
                case ADDR_HELI_SIZE:
                case ADDR_SENSOR_DIRECTION:
                case ADDR_SWASHPLATE:
                case ADDR_ROTATE_DIRECTION:
                case ADDR_SERVO_CH_1:
                case ADDR_SERVO_CH_2:
                case ADDR_SERVO_CH_3:
                case ADDR_SERVO_CH_4:
                case ADDR_SERVO_TYPE:
                case ADDR_COLLECTIVE_DIRECTION:
                case ADDR_TAIL_SERVO:
                case ADDR_TAIL_SERVO_TYPE:
                case ADDR_GOVERNOR_MODE:
                    continue;

                default:
                    break;
                }
            }

            QString text = QString::number(key) + ","
                    + QString::number(m_fblParamsCache.value(key));
            m_serialManager->writeConfigBlocking(text);
        }

        ui->hideProgressDialog();
    }
    else {
        saveProfile(m_currentProfile);
        m_currentProfile = 0;
        ui->cbProfile->setCurrentIndex(m_currentProfile);
    }

    m_LoadProfileLevel = 0;
    changeAction(Running);
    ui->animationWhenLoadFirmwareParams(false);
}

void MainWidget::cbProfile_activated(int index)
{
    if (m_applicationMode == Online && m_serialManager->isPortConnected()) {
        int ret = QMessageBox::warning(ui, Res(17), Res(18),
                             QMessageBox::Ok, QMessageBox::Cancel);
        if (ret != QMessageBox::Ok) {
            ui->cbProfile->setCurrentIndex(m_currentProfile);
            return;
        }
    }

    saveProfile(m_currentProfile);
    loadProfile(index);
    m_currentProfile = index;
}

void MainWidget::intializeWidgetHashValues()
{
    // tab start

    m_widgetIdHash.insert(ADDR_HELI_SIZE, ui->btGroupRadioHeliSize);
    m_widgetIdHash.insert(ADDR_FLY_TYPE, ui->flyStyleView);

    // tab rx

    m_widgetIdHash.insert(ADDR_RX_TYPE, ui->cbReceiverType);
    m_widgetIdHash.insert(ADDR_RX_BIND, ui->btnBind);

    m_widgetIdHash.insert(ADDR_RX_ELEVATOR, ui->rxViewLeft);
    m_widgetIdHash.insert(ADDR_RX_RUDDER, ui->rxViewLeft);
    m_widgetIdHash.insert(ADDR_RX_PITCH, ui->rxViewRight);
    m_widgetIdHash.insert(ADDR_RX_AILERONS, ui->rxViewRight);

    m_widgetIdHash.insert(ADDR_RX_THROTTLE, ui->sliderRx1);
    m_widgetIdHash.insert(ADDR_RX_GEAR, ui->sliderRx2);
    m_widgetIdHash.insert(ADDR_RX_AUX_2, ui->sliderRx3);

    m_widgetIdHash.insert(ADDR_RX_SETCENTER, ui->btnSetCenter);

    // tab sensor

    m_widgetIdHash.insert(ADDR_SENSOR_DIRECTION, ui->btGroupSensorDirection);

    // tab swash

    m_widgetIdHash.insert(ADDR_SWASHPLATE, ui->btGroupSwashplate);
    m_widgetIdHash.insert(ADDR_ROTATE_DIRECTION, ui->btGroupRotateDirection);
    m_widgetIdHash.insert(ADDR_CYCLIC_RING, ui->editCyclicRing);
    m_widgetIdHash.insert(ADDR_CHECK_CYCLIC_RING, ui->checkCyclicRing);
    m_widgetIdHash.insert(ADDR_SWASHPLATE_ANGLE, ui->swashView);

    // tab servos

    m_widgetIdHash.insert(ADDR_SERVO_CH_1, ui->btnServoCh1);
    m_widgetIdHash.insert(ADDR_SERVO_CH_2, ui->btnServoCh2);
    m_widgetIdHash.insert(ADDR_SERVO_CH_3, ui->btnServoCh3);
    m_widgetIdHash.insert(ADDR_SERVO_CH_4, ui->btnServoCh4);

    m_widgetIdHash.insert(ADDR_SERVO_TRIM_CH_1, ui->sliderServoCh1);
    m_widgetIdHash.insert(ADDR_SERVO_TRIM_CH_2, ui->sliderServoCh2);
    m_widgetIdHash.insert(ADDR_SERVO_TRIM_CH_3, ui->sliderServoCh3);
    m_widgetIdHash.insert(ADDR_SERVO_TRIM_CH_4, ui->sliderServoCh4);

    m_widgetIdHash.insert(ADDR_SERVO_TYPE, ui->cbDigtalAnalogServos);

    m_widgetIdHash.insert(ADDR_SWASH_TRIM_X, ui->trimSwashCyclic);
    m_widgetIdHash.insert(ADDR_SWASH_TRIM_Y, ui->trimSwashCyclic);
    m_widgetIdHash.insert(ADDR_SWASH_TRIM_MONITOR_1, ui->trimSwashCyclic);
    m_widgetIdHash.insert(ADDR_SWASH_TRIM_MONITOR_2, ui->trimSwashCyclic);
    m_widgetIdHash.insert(ADDR_SWASH_TRIM_MONITOR_3, ui->trimSwashCyclic);
    m_widgetIdHash.insert(ADDR_SWASH_TRIM_MONITOR_4, ui->trimSwashCyclic);

    // tab collective

    m_widgetIdHash.insert(ADDR_COLLECTIVE_DIRECTION, ui->btGroupCollectDirection);
    m_widgetIdHash.insert(ADDR_COLLECTIVE_TRAVEL, ui->sliderCollectiveTravel);

    m_widgetIdHash.insert(ADDR_PITCH_PUMP, ui->editPitchpump);
    m_widgetIdHash.insert(ADDR_CHECK_PITCH_PUMP, ui->checkPitchpump);
    m_widgetIdHash.insert(ADDR_COLLECTIVE_TRIM, ui->trimSwashCollective);

    // tab cyclic

    m_widgetIdHash.insert(ADDR_CYCLIC_LIMIT, ui->sliderCyclicLimit);
    m_widgetIdHash.insert(ADDR_CYCLIC_GAIN, ui->sliderCyclicGain);
    m_widgetIdHash.insert(ADDR_CYCLIC_AGILITY, ui->sliderCyclicReponse);
    m_widgetIdHash.insert(ADDR_CYCLIC_ROLL, ui->sliderCyclicRoll);
    m_widgetIdHash.insert(ADDR_CYCLIC_FLIP, ui->sliderCyclicFlip);

    m_widgetIdHash.insert(ADDR_RC_DEADBAND, ui->editRcDeadband);
    m_widgetIdHash.insert(ADDR_ELEVATOR_PRECOM, ui->editElevatorPrecom);
    m_widgetIdHash.insert(ADDR_PADDLE_SIMULATE, ui->editPddleSimulate);
    m_widgetIdHash.insert(ADDR_CYCLIC_EXPO, ui->editCyclicExpo);

    // tab tail

    m_widgetIdHash.insert(ADDR_TAIL_SERVO_TYPE, ui->btGroupRadioTail);
    m_widgetIdHash.insert(ADDR_TAIL_SERVO, ui->btnTailServo);
    m_widgetIdHash.insert(ADDR_TAIL_SERVO_TRIM, ui->sliderTailServo);
    m_widgetIdHash.insert(ADDR_TAIL_TAIL_TRAVEL_1, ui->sliderTailTravel1);
    m_widgetIdHash.insert(ADDR_TAIL_TAIL_TRAVEL_2, ui->sliderTailTravel2);
    m_widgetIdHash.insert(ADDR_TAIL_YAW, ui->sliderTailYaw);
    m_widgetIdHash.insert(ADDR_TAIL_GAIN, ui->sliderTailGain);
    m_widgetIdHash.insert(ADDR_TAIL_ACELERATION, ui->editTailAccelerate);
    m_widgetIdHash.insert(ADDR_TAIL_STOPGAIN, ui->editTailStopGain);
    m_widgetIdHash.insert(ADDR_TAIL_EXPO, ui->editTailExpo);
    m_widgetIdHash.insert(ADDR_TAIL_COLLECTIVE, ui->editTailCollective);
    m_widgetIdHash.insert(ADDR_TAIL_CYCLIC, ui->editTailCyclic);
    m_widgetIdHash.insert(ADDR_TAIL_ZEROCOL, ui->editTailZeroCol);

    // tab governor

    m_widgetIdHash.insert(ADDR_GOVERNOR_MODE, ui->cbGovernorMode);
    m_widgetIdHash.insert(ADDR_GAS_SERVOS_TYPE, ui->cbGasServosType);
    m_widgetIdHash.insert(ADDR_THROTTLE_SERVO_REVERSE, ui->checkThrottleServosReverse);
    m_widgetIdHash.insert(ADDR_GOVERNOR_MOTOR_OFF, ui->sliderGovernorMotorOff);
    m_widgetIdHash.insert(ADDR_GOVERNOR_MAX_THROTTLE, ui->sliderGovernoMaxThrottle);
    m_widgetIdHash.insert(ADDR_MAX_HEADSPEED, ui->editMaxHeadspeed);
    m_widgetIdHash.insert(ADDR_GEAR_RATIO_LEFT, ui->editGearRatio);
    m_widgetIdHash.insert(ADDR_GEAR_RATIO_RIGHT, ui->editGearRatio);
    m_widgetIdHash.insert(ADDR_SENSOR_CONFIG, ui->editSensorConfig);
    m_widgetIdHash.insert(ADDR_AUTO_ROTATION_BAILOUT, ui->checkAutoBaillout);
    m_widgetIdHash.insert(ADDR_IDLE_DURING_BAILOUT, ui->checkIdleDuringBailout);

    // init m_fblParams hashtable

    foreach (const qint32 key, m_widgetIdHash.keys()) {
        switch (key) {
        // ignore some read only address
        case ADDR_RX_AILERONS:
        case ADDR_RX_AUX_2:
        case ADDR_RX_ELEVATOR:
        case ADDR_RX_GEAR:
        case ADDR_RX_PITCH:
        case ADDR_RX_RUDDER:
        case ADDR_RX_THROTTLE:
        case ADDR_RX_BIND:
        case ADDR_RX_SETCENTER:
        case ADDR_SWASH_TRIM_MONITOR_1:
        case ADDR_SWASH_TRIM_MONITOR_2:
        case ADDR_SWASH_TRIM_MONITOR_3:
        case ADDR_SWASH_TRIM_MONITOR_4:
            continue;

        default:
            VtekInterface *obj =
                    VtekInterface::createVtekObject(m_widgetIdHash[key]);
            if (obj != NULL) {
                m_fblParamsCache.insert(key, obj->getValue(key).toInt());
            } else {
                m_fblParamsCache.insert(key, 0);
            }
            break;
        }
    }
}

void MainWidget::registerConnections()
{
    // main widgets

    ui->installEventFilter(this);

    connect(ui->mainTab, SIGNAL(currentChanged(int)),
            this, SLOT(mainTab_current_changed(int)));

    connect(&m_updateUiTimer, SIGNAL(timeout()),
            this, SLOT(requestReadValuesOnTab()));

    // logic connections

    connect(m_serialManager, SIGNAL(started()),
            m_serialManager, SLOT(tryConnect()));

    connect(this, SIGNAL(_needReconnect()),
            m_serialManager, SLOT(tryReconnect()));

    connect(m_serialManager, SIGNAL(_portStatusChanged(int)),
            this, SLOT(handlePortStatusChanged(int)));

    connect(m_serialManager, SIGNAL(_responseToUpdateUi(QVariant)),
            this, SLOT(handleParamsChanged(QVariant)));

    connect(this, SIGNAL(_needUpdateFlash(QByteArray)),
            m_serialManager, SLOT(startUpdateFirmware(QByteArray)));

    connect(m_serialManager, SIGNAL(_flashStatusChanged(int)),
            this, SLOT(handleFlashStatusChanged(int)));
    connect(m_serialManager, SIGNAL(_flashProgressChanged(int)),
            this, SLOT(handleFlashProgressChanged(int)));

    // footer menu connections

    connect(ui->btnOpen, SIGNAL(clicked()), this, SLOT(btnOpen_button_clicked()));
    connect(ui->btnSave, SIGNAL(clicked()), this, SLOT(btnSave_button_clicked()));
    connect(ui->btnFirmware, SIGNAL(clicked()),
            this, SLOT(btnFirmware_button_clicked()));
    connect(ui->btnInfo, SIGNAL(clicked()), this, SLOT(btnInfo_button_clicked()));

    connect(ui->cbProfile, SIGNAL(activated(int)),
            this, SLOT(cbProfile_activated(int)));

    connect(ui->btnAcceptWriteLoadedParams, SIGNAL(clicked()),
            this, SLOT(btnWriteLoadedParams_button_clicked()));
    connect(ui->btnRejectWriteLoadedParams, SIGNAL(clicked()),
            this, SLOT(btnWriteLoadedParams_button_clicked()));

    // tab start connections

    connect(ui->btGroupRadioHeliSize, SIGNAL(buttonClicked(int)),
            this, SLOT(requestWriteConfigChanges()));

    connect(ui->flyStyleView, SIGNAL(_valueChanged(int)),
            this, SLOT(requestWriteConfigChanges()));

    // tab rx connections

    qRegisterSliderSignalHandler(ui->sliderRx1, this);
    qRegisterSliderSignalHandler(ui->sliderRx2, this);
    qRegisterSliderSignalHandler(ui->sliderRx3, this);

    connect(ui->rxViewLeft, SIGNAL(_sensorValueChanged(qint32, qint32)),
            this, SLOT(rxView_sensor_value_changed(qint32, qint32)));

    connect(ui->rxViewRight, SIGNAL(_sensorValueChanged(qint32, qint32)),
            this, SLOT(rxView_sensor_value_changed(qint32, qint32)));

    connect(ui->cbReceiverType, SIGNAL(activated(int)),
            this, SLOT(cbReceiverType_activated(int)));

    connect(ui->cbSatelliteType, SIGNAL(activated(int)),
            this, SLOT(cbReceiverType_activated(int)));

    connect(ui->cbTxMode, SIGNAL(currentIndexChanged(int)),
            this, SLOT(cbTxMode_current_index_changed(int)));

    connect(ui->btnSetCenter, SIGNAL(clicked()),
            this, SLOT(btnSetCenter_clicked()));

    connect(ui->btnBind, SIGNAL(clicked()), this, SLOT(btnBind_clicked()));


    // tab sensor connections

    connect(ui->btGroupSensorDirection, SIGNAL(buttonClicked(int)),
            this, SLOT(buttonGroup_button_clicked(int)));

    // tab swash connections

    connect(ui->btGroupSwashplate, SIGNAL(buttonClicked(int)),
            this, SLOT(buttonGroup_button_clicked(int)));

    connect(ui->btGroupRotateDirection, SIGNAL(buttonClicked(int)),
           this, SLOT(buttonGroup_button_clicked(int)));

    connect(ui->swashView, SIGNAL(_angleChanged(QList<int>)),
            this, SLOT(swashView_angle_changed(QList<int>)));

    connect(ui->btnSwashViewRotateLeft, SIGNAL(clicked()),
            this, SLOT(btnSwashViewRotateLeft_clicked()));

    connect(ui->btnSwashViewRotateRight, SIGNAL(clicked()),
            this, SLOT(btnSwashViewRotateRight_clicked()));

    qRegisterLineEditSignalHandler(ui->editCyclicRing, this);
    connect(ui->checkCyclicRing, SIGNAL(clicked(bool)),
            this, SLOT(requestWriteConfigChanges()));
    connect(ui->checkCyclicRing, SIGNAL(toggled(bool)),
            ui->editCyclicRing, SLOT(setEnabled(bool)));

    // tab servos connections

    qRegisterDoubleSliderSignalHandler(ui->sliderServoCh1, this);
    qRegisterDoubleSliderSignalHandler(ui->sliderServoCh2, this);
    qRegisterDoubleSliderSignalHandler(ui->sliderServoCh3, this);
    qRegisterDoubleSliderSignalHandler(ui->sliderServoCh4, this);

    connect(ui->btnServoCh1, SIGNAL(clicked(bool)),
            this, SLOT(checkableToolButton_clicked(bool)));

    connect(ui->btnServoCh2, SIGNAL(clicked(bool)),
            this, SLOT(checkableToolButton_clicked(bool)));

    connect(ui->btnServoCh3, SIGNAL(clicked(bool)),
            this, SLOT(checkableToolButton_clicked(bool)));

    connect(ui->btnServoCh4, SIGNAL(clicked(bool)),
            this, SLOT(checkableToolButton_clicked(bool)));

    connect(ui->cbDigtalAnalogServos, SIGNAL(activated(int)),
            this, SLOT(requestWriteConfigChanges()));

    connect(ui->checkGyroOff, SIGNAL(clicked()),
            this,SLOT(requestWriteConfigChanges()));

    connect(ui->trimSwashCyclic, SIGNAL(_trimValueChanged(int,int,int,bool)),
            this, SLOT(trimSwashCyclic_value_changed(int,int,int,bool)));
    connect(ui->trimSwashCyclic, SIGNAL(_monitorChannelValueChanged(int,int)),
            ui->trimSwashCollective, SLOT(setTrimMonitorValue(int,int)));

    // tab collective connections

    connect(ui->btGroupCollectDirection, SIGNAL(buttonClicked(int)),
            this, SLOT(buttonGroup_button_clicked(int)));

    connect(ui->trimSwashCollective, SIGNAL(_trimValueChanged(int)),
            this, SLOT(trimSwashCollective_value_changed(int)));

    qRegisterSliderSignalHandler(ui->sliderCollectiveTravel, this);
    qRegisterLineEditSignalHandler(ui->editCollectiveTravel, this);

    qRegisterLineEditSignalHandler(ui->editPitchpump, this);
    connect(ui->checkPitchpump, SIGNAL(clicked(bool)),
            this, SLOT(requestWriteConfigChanges()));
    connect(ui->checkPitchpump, SIGNAL(toggled(bool)),
            ui->editPitchpump, SLOT(setEnabled(bool)));

    // tab cyclic connections

    connect(ui->btnCyclicDefault, SIGNAL(clicked()),
            this, SLOT(btnCyclicDefault_button_clicked()));
    connect(ui->btnCyclicDefaultAdvance, SIGNAL(clicked()),
            this, SLOT(btnCyclicDefault_button_clicked()));

    qRegisterSliderSignalHandler(ui->sliderCyclicLimit, this);
    qRegisterLineEditSignalHandler(ui->editCyclicLimit, this);

    qRegisterSliderSignalHandler(ui->sliderCyclicGain, this);
    qRegisterLineEditSignalHandler(ui->editCyclicGain, this);

    qRegisterSliderSignalHandler(ui->sliderCyclicReponse, this);
    qRegisterLineEditSignalHandler(ui->editCyclicResponse, this);

    qRegisterSliderSignalHandler(ui->sliderCyclicRoll, this);
    qRegisterLineEditSignalHandler(ui->editCyclicRoll, this);

    qRegisterSliderSignalHandler(ui->sliderCyclicFlip, this);
    qRegisterLineEditSignalHandler(ui->editCyclicFlip, this);

    qRegisterLineEditSignalHandler(ui->editCyclicExpo, this);
    qRegisterLineEditSignalHandler(ui->editRcDeadband, this);
    qRegisterLineEditSignalHandler(ui->editElevatorPrecom, this);
    qRegisterLineEditSignalHandler(ui->editPddleSimulate, this);

    // tab tail connections

    connect(ui->btnTailDefault, SIGNAL(clicked()),
            this, SLOT(btnTailDefault_button_clicked()));

    connect(ui->btGroupRadioTail, SIGNAL(buttonClicked(int)),
            this, SLOT(requestWriteConfigChanges()));

    connect(ui->btnTailServo, SIGNAL(clicked(bool)),
            this, SLOT(checkableToolButton_clicked(bool)));

    qRegisterDoubleSliderSignalHandler(ui->sliderTailServo, this);

    qRegisterSliderSignalHandler(ui->sliderTailTravel1, this);
    qRegisterLineEditSignalHandler(ui->editTailTravel1, this);

    qRegisterSliderSignalHandler(ui->sliderTailTravel2, this);
    qRegisterLineEditSignalHandler(ui->editTailTravel2, this);

    qRegisterSliderSignalHandler(ui->sliderTailYaw, this);
    qRegisterLineEditSignalHandler(ui->editTailYaw, this);

    qRegisterSliderSignalHandler(ui->sliderTailGain, this);
    qRegisterLineEditSignalHandler(ui->editTailGain, this);

    qRegisterLineEditSignalHandler(ui->editTailAccelerate, this);
    qRegisterLineEditSignalHandler(ui->editTailStopGain, this);
    qRegisterLineEditSignalHandler(ui->editTailExpo, this);
    qRegisterLineEditSignalHandler(ui->editTailCollective, this);
    qRegisterLineEditSignalHandler(ui->editTailCyclic, this);
    qRegisterLineEditSignalHandler(ui->editTailZeroCol, this);

    // tab governor connections

    connect(ui->cbGovernorMode, SIGNAL(activated(int)),
            this, SLOT(requestWriteConfigChanges()));

    connect(ui->cbGasServosType, SIGNAL(activated(int)),
            this,SLOT(requestWriteConfigChanges()));

    connect(ui->checkThrottleServosReverse, SIGNAL(clicked()),
            this, SLOT(requestWriteConfigChanges()));

    qRegisterSliderSignalHandler(ui->sliderGovernorMotorOff, this);
    qRegisterLineEditSignalHandler(ui->editGovernorMotorOff, this);

    qRegisterSliderSignalHandler(ui->sliderGovernoMaxThrottle, this);
    qRegisterLineEditSignalHandler(ui->editGovernorMaxThrottle, this);

    qRegisterLineEditSignalHandler(ui->editMaxHeadspeed, this);
    qRegisterLineEditSignalHandler(ui->editGearRatio, this);
    qRegisterLineEditSignalHandler(ui->editSensorConfig, this);

    connect(ui->checkAutoBaillout, SIGNAL(clicked()),
            this, SLOT(requestWriteConfigChanges()));

    connect(ui->checkIdleDuringBailout, SIGNAL(clicked()),
            this, SLOT(requestWriteConfigChanges()));
}

int MainWidget::findKeyByObject(void *obj)
{
    foreach (const int k, m_widgetIdHash.keys()) {
        if (m_widgetIdHash[k] == obj) {
            return k;
        }
    }
    return -1;
}

void MainWidget::textToDouble(QString text, int &left, int &right)
{
    char splitChar = '.';
    if (text.contains(',')) splitChar = ',';
    QStringList list = text.split(splitChar);
    left = list.first().toInt();
    right = 0;
    if (list.length() > 1) {
        right = list.last().toInt();
        if (list.last().length() <= 1) {
            right = right * 10;
        }
    }
}

void MainWidget::updateParamsCache(qint32 key, qint32 value)
{
    if (m_fblParamsCache.contains(key)) {
        m_fblParamsCache.insert(key, value);
    }
}

bool MainWidget::isStableWidget(QObject *obj, int addr)
{
    if (m_stableWidgets.contains(obj) || m_waitToWriteKeys.contains(addr)) {
        m_waitToWriteKeys.remove(addr);
        return true;
    }
    return false;
}

void MainWidget::changeAction(MainWidget::Action action)
{
    switch (action) {
    case Sleep:
        if (m_currentAction == Sleep) return;

        m_applicationMode = Offline;
        if (m_updateUiTimer.isActive()) {
            m_updateUiTimer.stop();
        }

        break;

    case Running:
        if (m_currentAction == Running) return;

        m_applicationMode = Online;
        m_updateUiCounter = 0;

        if (! m_updateUiTimer.isActive()) {
            m_updateUiTimer.start();
        }
        resetTabCounter();

        break;
    }

    m_currentAction = action;
}

void MainWidget::updateTabCounter(int index)
{
    for (qint8 i = 0; i < MAX_TAB_COUNT; i++) {
        if (i == index) {
            m_tabUpdateCounter[index] += 1;
            if (m_tabUpdateCounter[index] >= COUNTER_MAX_VALUE) {
                m_tabUpdateCounter[index] = 1;
            }
            continue;
        }
        m_tabUpdateCounter[i] = 0;
    }
}

void MainWidget::resetTabCounter()
{
    for (qint8 i = 0; i < MAX_TAB_COUNT; i++) {
        m_tabUpdateCounter[i] = 0;
    }
}

void MainWidget::loadApplicationConfig()
{
    if (m_appConfig->hasKey("txmode")) {
        int index = m_appConfig->get("txmode").toInt();
        if (index < ui->cbTxMode->count())
            ui->cbTxMode->setCurrentIndex(index);
    }
    if (m_appConfig->hasKey("lang")) {
        QString appLanguage = m_appConfig->get("lang");
        for (qint8 i = 0; i < ui->cbLanguage->count(); i++) {
            if (ui->cbLanguage->itemData(i).toString() == appLanguage) {
                ui->cbLanguage->setCurrentIndex(i);
                break;
            }
        }
    }
    if (m_appConfig->hasKey("gyro-off")) {
        ui->checkGyroOff->setChecked(m_appConfig->get("gyro-off").toInt());
    }
}

void MainWidget::saveApplicationConfig()
{
    m_appConfig->put("txmode", QString::number(ui->cbTxMode->currentIndex()));
    m_appConfig->put("lang", ui->cbLanguage->currentData().toString());
    m_appConfig->put("gyro-off", QString::number((int)ui->checkGyroOff->isChecked()));
    m_appConfig->saveConfigToFile();
}

bool MainWidget::saveProfile(int profileIndex)
{
    if (profileIndex != 0) {
        QString filePath = APP_PROFILE_FILE_PREFIX
                + QString::number(profileIndex)
                + APP_PROFILE_FILE_EXTENSION;
        int ret = qSaveParametersToFile(filePath, m_fblParamsCache);
        if (ret != 0) {
            qDebug() << "save profile error";
            return false;
        }
    }
    return true;
}

bool MainWidget::loadProfile(int profileIndex)
{
    if (profileIndex != 0) {
        QString filePath = APP_PROFILE_FILE_PREFIX
                + QString::number(profileIndex)
                + APP_PROFILE_FILE_EXTENSION;
        int ret = qLoadParametersFromFile(filePath, &m_fblParamsCache);
        if (ret != 0) {
            QMessageBox::warning(ui, Res(15), Res(16), QMessageBox::Ok);
            return false;
        }

        if (m_serialManager->isPortConnected()) {
            if (m_LoadProfileLevel == 0) {
                m_LoadProfileLevel = LOAD_PARAMS_LEVEL_PROFILE;
            }
            changeAction(Sleep);
            ui->animationWhenLoadFirmwareParams(true);
        }

        foreach (qint32 key, m_fblParamsCache.keys()) {
            qint32 value = m_fblParamsCache.value(key);
            handleParamsChanged(QString::number(key) + "," + QString::number(value));
        }
    }
    return true;
}


