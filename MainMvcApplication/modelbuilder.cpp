#include "modelbuilder.h"
#include "address.h"
#include "mainwidget_ui.h"
#include "adaptormodel.h"
#include "parametermodel.h"
#include "extraadaptors.h"
#include "trimswashcyclic.h"
#include "trimswashcollective.h"

ModelBuilder::ModelBuilder(Ui_MainWidget *ui)
    : ui(ui),
      m_paramModel(new ParameterModel),
      m_adaptorModel(new AdaptorModel)
{
    intializeWidgetHashValues();
}

ParameterModel *ModelBuilder::buildParameterModel()
{
    foreach (const int addr, m_hash.keys()) {
        switch (addr) {
        // ignore some read only address
        case ADDR_RX_AILERONS:
        case ADDR_RX_AUX_2:
        case ADDR_RX_ELEVATOR:
        case ADDR_RX_GEAR:
        case ADDR_RX_PITCH:
        case ADDR_RX_RUDDER:
        case ADDR_RX_THROTTLE:
        case ADDR_SWASH_TRIM_MONITOR_1:
        case ADDR_SWASH_TRIM_MONITOR_2:
        case ADDR_SWASH_TRIM_MONITOR_3:
        case ADDR_SWASH_TRIM_MONITOR_4:
            break;

        default:
            Parameter *p = new Parameter;
            p->Address = addr;
            p->Value = m_adaptorModel->getAdaptorValue(addr);
            m_paramModel->insertParameter(addr, p);
        }
    }

    return m_paramModel;
}

AdaptorModel *ModelBuilder::buildAdaptorModel()
{
    foreach (const int addr, m_hash.keys()) {
        AbstractFieldAdaptor *adaptor = this->createAdaptor(addr, m_hash[addr]);
        m_adaptorModel->addAdaptor(addr, adaptor);
    }

    return m_adaptorModel;
}

/**
 * implements abstract factory method
 * @brief ModelBuilder::createAdaptor
 * @param addr
 * @param obj
 * @return
 */
AbstractFieldAdaptor *ModelBuilder::createAdaptor(int addr, QObject *obj)
{
    switch (addr)
    {
    case ADDR_FLY_TYPE:
        return new FlyStyleAdaptor(obj);

    case ADDR_RX_TYPE:
        return new RxTypeComboBoxAdaptor(ui->cbReceiverType, ui->cbSatelliteType);

    case ADDR_RX_PITCH:
        return new RxMonitorAdaptor(RxMonitorAdaptor::Vertical, ui->rxViewLeft);
    case ADDR_RX_RUDDER:
        return new RxMonitorAdaptor(RxMonitorAdaptor::Horizontal, ui->rxViewLeft);
    case ADDR_RX_ELEVATOR:
        return new RxMonitorAdaptor(RxMonitorAdaptor::Vertical, ui->rxViewRight);
    case ADDR_RX_AILERONS:
        return new RxMonitorAdaptor(RxMonitorAdaptor::Horizontal, ui->rxViewRight);

    case ADDR_SWASHPLATE_ANGLE:
        return new ServoAngleAdaptor(obj);

    case ADDR_SWASH_TRIM_X:
        return new SwashTrimCyclicAdaptor(
                    SwashTrimCyclic::Cyclic, SwashTrimCyclic::Horizontal, obj);
    case ADDR_SWASH_TRIM_Y:
        return new SwashTrimCyclicAdaptor(
                    SwashTrimCyclic::Cyclic, SwashTrimCyclic::Vertical, obj);
    case ADDR_SWASH_TRIM_MIN_COL_X:
        return new SwashTrimCyclicAdaptor(
                    SwashTrimCyclic::MinCollective, SwashTrimCyclic::Horizontal, obj);
    case ADDR_SWASH_TRIM_MIN_COL_Y:
        return new SwashTrimCyclicAdaptor(
                    SwashTrimCyclic::MinCollective, SwashTrimCyclic::Vertical, obj);
    case ADDR_SWASH_TRIM_MAX_COL_X:
        return new SwashTrimCyclicAdaptor(
                    SwashTrimCyclic::MaxCollective, SwashTrimCyclic::Horizontal, obj);
    case ADDR_SWASH_TRIM_MAX_COL_Y:
        return new SwashTrimCyclicAdaptor(
                    SwashTrimCyclic::MaxCollective, SwashTrimCyclic::Vertical, obj);
    case ADDR_SWASH_TRIM_MONITOR_1:
        return new SwashTrimCyclicMonitorAdaptor(SwashTrimCyclic::Channel1, obj);
    case ADDR_SWASH_TRIM_MONITOR_2:
        return new SwashTrimCyclicMonitorAdaptor(SwashTrimCyclic::Channel2, obj);
    case ADDR_SWASH_TRIM_MONITOR_3:
        return new SwashTrimCyclicMonitorAdaptor(SwashTrimCyclic::Channel3, obj);
    case ADDR_SWASH_TRIM_MONITOR_4:
        return new SwashTrimCyclicMonitorAdaptor(SwashTrimCyclic::Channel4, obj);

    case ADDR_COLLECTIVE_TRAVEL:
        return new SliderWithLineEditAdaptor(obj, ui->editCollectiveTravel);
    case ADDR_COLLECTIVE_TRIM:
        return new SwashTrimCollectiveAdaptor(obj);

    case ADDR_CYCLIC_LIMIT:
        return new SliderWithLineEditAdaptor(obj, ui->editCyclicLimit);
    case ADDR_CYCLIC_GAIN:
        return new SliderWithLineEditAdaptor(obj, ui->editCyclicGain);
    case ADDR_CYCLIC_AGILITY:
        return new SliderWithLineEditAdaptor(obj, ui->editCyclicResponse);
    case ADDR_CYCLIC_ROLL:
        return new SliderWithLineEditAdaptor(obj, ui->editCyclicRoll);
    case ADDR_CYCLIC_FLIP:
        return new SliderWithLineEditAdaptor(obj, ui->editCyclicFlip);

    case ADDR_TAIL_TAIL_TRAVEL_1:
        return new SliderWithLineEditAdaptor(obj, ui->editTailTravel1);
    case ADDR_TAIL_TAIL_TRAVEL_2:
        return new SliderWithLineEditAdaptor(obj, ui->editTailTravel2);
    case ADDR_TAIL_YAW:
        return new SliderWithLineEditAdaptor(obj, ui->editTailYaw);
    case ADDR_TAIL_GAIN:
        return new SliderWithLineEditAdaptor(obj, ui->editTailGain);

    case ADDR_GOVERNOR_MAX_THROTTLE:
        return new SliderWithLineEditAdaptor(obj, ui->editGovernorMaxThrottle);
    case ADDR_GOVERNOR_MOTOR_OFF:
        return new GovMotorOffSliderWithLineEditAdaptor(obj, ui->editGovernorMotorOff);
    case ADDR_MAX_HEADSPEED:
        return new MaxHeadSpeedLineEditAdaptor(obj);
    case ADDR_GEAR_RATIO_LEFT:
        return new GearRatioLineEditAdaptor(GearRatioLineEditAdaptor::Left, obj);
    case ADDR_GEAR_RATIO_RIGHT:
        return new GearRatioLineEditAdaptor(GearRatioLineEditAdaptor::Right, obj);

    default:
        if (qobject_cast<QLineEdit*> (obj)) {
            return new LineEditAdaptor(obj);
        }
        else if (qobject_cast<QSlider*> (obj)) {
            return new SliderAdaptor(obj);
        }
        else if (qobject_cast<QCheckBox*> (obj)) {
            return new CheckBoxAdaptor(obj);
        }
        else if (qobject_cast<QComboBox*> (obj)) {
            return new ComboBoxAdaptor(obj);
        }
        else if (qobject_cast<DoubleDirectSlider*> (obj)) {
            DoubleDirectSlider* dbSlider =
                    qobject_cast<DoubleDirectSlider*> (obj);
            return new DoubleDirectSliderAdaptor(dbSlider->getSlider(),
                                                 dbSlider->getLineEdit());
        }
        else if (qobject_cast<ButtonGroup*> (obj)) {
            return new ButtonGroupWithVerifyAdaptor(obj);
        }
        else if (qobject_cast<QButtonGroup*> (obj)) {
            return new ButtonGroupAdaptor(obj);
        }
        else if (qobject_cast<QToolButton*> (obj)) {
            return new CheckableButtonWithVerifyAdaptor(obj);
        }
        else {
            qDebug() << addr;
            return 0;
        }
    }
}

void ModelBuilder::intializeWidgetHashValues()
{
    // tab start

    m_hash.insert(ADDR_HELI_SIZE, ui->btGroupRadioHeliSize);
    m_hash.insert(ADDR_FLY_TYPE, ui->flyStyleView);

    // tab rx

    m_hash.insert(ADDR_RX_TYPE, ui->cbReceiverType);

    m_hash.insert(ADDR_RX_ELEVATOR, ui->rxViewLeft);
    m_hash.insert(ADDR_RX_RUDDER, ui->rxViewLeft);
    m_hash.insert(ADDR_RX_PITCH, ui->rxViewRight);
    m_hash.insert(ADDR_RX_AILERONS, ui->rxViewRight);

    m_hash.insert(ADDR_RX_THROTTLE, ui->sliderRx1);
    m_hash.insert(ADDR_RX_GEAR, ui->sliderRx2);
    m_hash.insert(ADDR_RX_AUX_2, ui->sliderRx3);

    // tab sensor

    m_hash.insert(ADDR_SENSOR_DIRECTION, ui->btGroupSensorDirection);

    // tab swash

    m_hash.insert(ADDR_SWASHPLATE, ui->btGroupSwashplate);
    m_hash.insert(ADDR_ROTATE_DIRECTION, ui->btGroupRotateDirection);
    m_hash.insert(ADDR_CYCLIC_RING, ui->editCyclicRing);
    m_hash.insert(ADDR_CHECK_CYCLIC_RING, ui->checkCyclicRing);
    m_hash.insert(ADDR_SWASHPLATE_ANGLE, ui->swashView);

    // tab servos

    m_hash.insert(ADDR_SERVO_CH_1, ui->btnServoCh1);
    m_hash.insert(ADDR_SERVO_CH_2, ui->btnServoCh2);
    m_hash.insert(ADDR_SERVO_CH_3, ui->btnServoCh3);
    m_hash.insert(ADDR_SERVO_CH_4, ui->btnServoCh4);

    m_hash.insert(ADDR_SERVO_TRIM_CH_1, ui->sliderServoCh1);
    m_hash.insert(ADDR_SERVO_TRIM_CH_2, ui->sliderServoCh2);
    m_hash.insert(ADDR_SERVO_TRIM_CH_3, ui->sliderServoCh3);
    m_hash.insert(ADDR_SERVO_TRIM_CH_4, ui->sliderServoCh4);

    m_hash.insert(ADDR_SERVO_TYPE, ui->cbDigtalAnalogServos);

    m_hash.insert(ADDR_SWASH_TRIM_X, ui->trimSwashCyclic);
    m_hash.insert(ADDR_SWASH_TRIM_Y, ui->trimSwashCyclic);
    m_hash.insert(ADDR_SWASH_TRIM_MIN_COL_X, ui->trimSwashCyclic);
    m_hash.insert(ADDR_SWASH_TRIM_MIN_COL_Y, ui->trimSwashCyclic);
    m_hash.insert(ADDR_SWASH_TRIM_MAX_COL_X, ui->trimSwashCyclic);
    m_hash.insert(ADDR_SWASH_TRIM_MAX_COL_Y, ui->trimSwashCyclic);

    m_hash.insert(ADDR_SWASH_TRIM_MONITOR_1, ui->trimSwashCyclic);
    m_hash.insert(ADDR_SWASH_TRIM_MONITOR_2, ui->trimSwashCyclic);
    m_hash.insert(ADDR_SWASH_TRIM_MONITOR_3, ui->trimSwashCyclic);
    m_hash.insert(ADDR_SWASH_TRIM_MONITOR_4, ui->trimSwashCyclic);

    // tab collective

    m_hash.insert(ADDR_COLLECTIVE_DIRECTION, ui->btGroupCollectDirection);
    m_hash.insert(ADDR_COLLECTIVE_TRAVEL, ui->sliderCollectiveTravel);

    m_hash.insert(ADDR_PITCH_PUMP, ui->editPitchpump);
    m_hash.insert(ADDR_CHECK_PITCH_PUMP, ui->checkPitchpump);
    m_hash.insert(ADDR_COLLECTIVE_TRIM, ui->trimSwashCollective);

    // tab cyclic

    m_hash.insert(ADDR_CYCLIC_LIMIT, ui->sliderCyclicLimit);
    m_hash.insert(ADDR_CYCLIC_GAIN, ui->sliderCyclicGain);
    m_hash.insert(ADDR_CYCLIC_AGILITY, ui->sliderCyclicReponse);
    m_hash.insert(ADDR_CYCLIC_ROLL, ui->sliderCyclicRoll);
    m_hash.insert(ADDR_CYCLIC_FLIP, ui->sliderCyclicFlip);

    m_hash.insert(ADDR_RC_DEADBAND, ui->editRcDeadband);
    m_hash.insert(ADDR_ELEVATOR_PRECOM, ui->editElevatorPrecom);
    m_hash.insert(ADDR_PADDLE_SIMULATE, ui->editPddleSimulate);
    m_hash.insert(ADDR_CYCLIC_EXPO, ui->editCyclicExpo);

    // tab tail

    m_hash.insert(ADDR_TAIL_SERVO_TYPE, ui->btGroupRadioTail);
    m_hash.insert(ADDR_TAIL_SERVO, ui->btnTailServo);
    m_hash.insert(ADDR_TAIL_SERVO_TRIM, ui->sliderTailServo);
    m_hash.insert(ADDR_TAIL_TAIL_TRAVEL_1, ui->sliderTailTravel1);
    m_hash.insert(ADDR_TAIL_TAIL_TRAVEL_2, ui->sliderTailTravel2);
    m_hash.insert(ADDR_TAIL_YAW, ui->sliderTailYaw);
    m_hash.insert(ADDR_TAIL_GAIN, ui->sliderTailGain);
    m_hash.insert(ADDR_TAIL_ACELERATION, ui->editTailAccelerate);
    m_hash.insert(ADDR_TAIL_STOPGAIN, ui->editTailStopGain);
    m_hash.insert(ADDR_TAIL_EXPO, ui->editTailExpo);
    m_hash.insert(ADDR_TAIL_COLLECTIVE, ui->editTailCollective);
    m_hash.insert(ADDR_TAIL_CYCLIC, ui->editTailCyclic);
    m_hash.insert(ADDR_TAIL_ZEROCOL, ui->editTailZeroCol);

    // tab governor

    m_hash.insert(ADDR_GOVERNOR_MODE, ui->cbGovernorMode);
    m_hash.insert(ADDR_GAS_SERVOS_TYPE, ui->cbGasServosType);
    m_hash.insert(ADDR_THROTTLE_SERVO_REVERSE, ui->checkThrottleServosReverse);
    m_hash.insert(ADDR_GOVERNOR_MOTOR_OFF, ui->sliderGovernorMotorOff);
    m_hash.insert(ADDR_GOVERNOR_MAX_THROTTLE, ui->sliderGovernoMaxThrottle);
    m_hash.insert(ADDR_MAX_HEADSPEED, ui->editMaxHeadspeed);
    m_hash.insert(ADDR_GEAR_RATIO_LEFT, ui->editGearRatio);
    m_hash.insert(ADDR_GEAR_RATIO_RIGHT, ui->editGearRatio);
    m_hash.insert(ADDR_SENSOR_CONFIG, ui->editSensorConfig);
    m_hash.insert(ADDR_AUTO_ROTATION_BAILOUT, ui->checkAutoBaillout);
    m_hash.insert(ADDR_IDLE_DURING_BAILOUT, ui->checkIdleDuringBailout);
}
