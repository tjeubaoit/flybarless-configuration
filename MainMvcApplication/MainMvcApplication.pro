#-------------------------------------------------
#
# Project created by QtCreator 2014-08-27T09:20:54
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = fblmain
TEMPLATE = app

DEFINES += DEVELOPER_VERSION

SOURCES += main.cpp \
    adaptormodel.cpp \
    parametermodel.cpp \
    firmwareasker.cpp \
    firmwaredialog.cpp \
    mainwidget_ui.cpp \
    modelbuilder.cpp \
    maincontroller.cpp \
    global.cpp \
    extraadaptors.cpp \
    infodialog.cpp

HEADERS  += \
    adaptormodel.h \
    parameter.h \
    parametermodel.h \
    firmwareasker.h \
    address.h \
    firmwaredialog.h \
    global.h \
    mainwidget_ui.h \
    pre_compiled.h \
    resources.h \
    modelbuilder.h \
    maincontroller.h \
    extraadaptors.h \
    infodialog.h

RESOURCES += \
    fbl.qrc

FORMS += \
    firmwaredialog.ui \
    mainwidget_ui.ui \
    infodialog.ui

RC_ICONS = Apollo.ico


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../AdaptorViews/release/ -lvtadaptorviews
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../AdaptorViews/debug/ -lvtadaptorviews
else:unix: LIBS += -L$$OUT_PWD/../AdaptorViews/ -lvtadaptorviews

INCLUDEPATH += $$PWD/../AdaptorViews
DEPENDPATH += $$PWD/../AdaptorViews

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../CustomWidgets/release/ -lvtcustomwidgets
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../CustomWidgets/debug/ -lvtcustomwidgets
else:unix: LIBS += -L$$OUT_PWD/../CustomWidgets/ -lvtcustomwidgets

INCLUDEPATH += $$PWD/../CustomWidgets
DEPENDPATH += $$PWD/../CustomWidgets

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../CustomFrame/release/ -lvtcustomframe
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../CustomFrame/debug/ -lvtcustomframe
else:unix: LIBS += -L$$OUT_PWD/../CustomFrame/ -lvtcustomframe

INCLUDEPATH += $$PWD/../CustomFrame
DEPENDPATH += $$PWD/../CustomFrame

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../IOHelper/release/ -lvtio
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../IOHelper/debug/ -lvtio
else:unix: LIBS += -L$$OUT_PWD/../IOHelper/ -lvtio

INCLUDEPATH += $$PWD/../IOHelper
DEPENDPATH += $$PWD/../IOHelper

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../SerialManager/release/ -lvtserial
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../SerialManager/debug/ -lvtserial
else:unix: LIBS += -L$$OUT_PWD/../SerialManager/ -lvtserial

INCLUDEPATH += $$PWD/../SerialManager
DEPENDPATH += $$PWD/../SerialManager
