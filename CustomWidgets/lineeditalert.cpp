#include "lineeditalert.h"
#include <QTimer>
#include <QObjectData>

#define TOOLTIP_INTERVAL 250

class LineEditAlert::LineEditAlertPrivate {
public:
    LineEditAlertPrivate()
        : tooltipCounter(0)
    {
        tooltipTimer.setSingleShot(false);
    }

    int tooltipCounter;
    QTimer tooltipTimer;
};

LineEditAlert::LineEditAlert(QWidget *parent) :
    QLabel(parent),
    d(new LineEditAlertPrivate)
{
    setStyleSheet("QLabel { background-image:url(images/tooltip.png);"
                    "padding-left:20px; color:#fefefe;padding-top:7px;"
                  "font-family:UTM Avo; font-size:13px;}");
    QLabel::hide();

    connect(&(d->tooltipTimer), SIGNAL(timeout()),
            this, SLOT(tooltipTimer_timeout()));
}

LineEditAlert::~LineEditAlert()
{
    if (d->tooltipTimer.isActive()) {
        d->tooltipTimer.stop();
    }

    delete d;
}

void LineEditAlert::showMe()
{
    if (!d->tooltipTimer.isActive()) {
        d->tooltipCounter = 0;
        d->tooltipTimer.start(TOOLTIP_INTERVAL);
    }
    show();
}

void LineEditAlert::hideMe()
{
    if (d->tooltipTimer.isActive()) {
        d->tooltipTimer.stop();
    }
    hide();
}

void LineEditAlert::tooltipTimer_timeout()
{
    if (d->tooltipCounter % 2 == 0) {
        setStyleSheet("QLabel { background-image:url(images/tooltip.png);"
                        "padding-left:20px; color:#fefefe;padding-top:7px;"
                        "font-family:UTM Avo; font-size:13px;}");
        update();
    }
    else {
        setStyleSheet("QLabel { background-image:url(images/tooltip.png);"
                        "padding-left:20px; color:#ff2b2b;padding-top:7px;"
                        "font-family:UTM Avo; font-size:13px;}");
        update();
    }

    if (d->tooltipCounter >= 10) {
        d->tooltipCounter = 1;
    }
    else {
        d->tooltipCounter ++;
    }
}
