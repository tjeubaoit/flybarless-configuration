#include "rxmonitorview.h"
#include "math.h"
#include <QPropertyAnimation>
#include <QObjectData>

class RxMonitorView::RxMonitorViewPrivate {
public:
    RxMonitorViewPrivate(int w, int h, bool animFinish)
        : rxWidth(w),
          rxHeight(h),
          animFinish(animFinish)
    {
    }

    int rxWidth, rxHeight;
    bool animFinish;
    int location;
    int min, max;

    SensorItem *sensor;
};

RxMonitorView::RxMonitorView(int width, int height,
                             int sensorWidth, int sensorHeight,
                             QWidget *parent)
    : QGraphicsView(parent),
      x(0),
      y(0),
      need_move(true),
      d(new RxMonitorViewPrivate(width, height, true))
{
    setFixedSize(d->rxWidth + 10, d->rxHeight + 10);

    setRenderHint(QPainter::Antialiasing, true);
    setDragMode(RubberBandDrag);
    setOptimizationFlags(DontSavePainterState);
    setViewportUpdateMode(SmartViewportUpdate);
    setTransformationAnchor(AnchorUnderMouse);
    setInteractive(false);

    RxItem *rx = new RxItem(d->rxWidth, d->rxHeight);
    rx->setPos(5, 5);

    d->sensor = new SensorItem(sensorWidth, sensorHeight);
    d->sensor->setPos(5 + d->rxWidth/2, 5 + d->rxHeight/2);

    QGraphicsScene *scene = new QGraphicsScene;
    scene->addItem(rx);
    scene->addItem(d->sensor);
    setScene(scene);
}

RxMonitorView::~RxMonitorView()
{
    delete d;
}

void RxMonitorView::moveSensor()
{
    need_move = false;
    emit _sensorValueChanged((int) x, (int) y);

    if ((x + y < 2*d->max) && (x + y > 2*d->min)) {
        qreal dx = (d->location == RX_LOCATION_RIGHT)
                ? (x - d->min) / (d->max - d->min) * d->rxWidth
                : (d->max - x) / (d->max - d->min) * d->rxWidth;
        qreal dy = (d->max - y) / (d->max - d->min) * d->rxHeight;

        if (dx < 10 || dx > width() - 10) {
            return;
        }
        if (dy < 10 || dy > height() - 10) {
            return;
        }

        dx += 5;
        dy += 5;

        int duration = 0;
        qreal distance = sqrt(pow(d->sensor->pos().x() - dx, 2)
                + pow(d->sensor->pos().y() - dy, 2));
        duration = (distance / d->rxWidth) * 100;

        d->animFinish = false;

        QPropertyAnimation *animation = new QPropertyAnimation(d->sensor, "pos");
        connect(animation, SIGNAL(finished()), this, SLOT(handleAnimFinished()));

        animation->setDuration(duration);
        animation->setStartValue(d->sensor->pos());
        animation->setEndValue(QPointF(dx, dy));
        animation->start(QAbstractAnimation::DeleteWhenStopped);
    }
}

void RxMonitorView::handleAnimFinished()
{
    d->animFinish = true;
}

void RxMonitorView::setLocation(int location)
{
    d->location = location;
}

int RxMonitorView::getLocation() const
{
    return d->location;
}

bool RxMonitorView::animFinished() const
{
    return d->animFinish;
}

void RxMonitorView::setRangeValue(int min, int max)
{
    d->min = min;
    d->max = max;
    x = y = (max - min) / 2 + min;
}

