#include "progressring.h"
#include "progressring_p.h"
#include <QPainter>


ProgressRing::ProgressRing(QWidget *parent) :
    QLabel(parent),
    d(new ProgressRingPrivate)
{
    connect(&d->rotateTimer, SIGNAL(timeout()),
            this, SLOT(rotateTimer_timeout()));
}

ProgressRing::~ProgressRing()
{
    if (d->pixmap != 0) {
        delete d->pixmap;
    }

    delete d;
}

void ProgressRing::setPixmapUrl(const QString &url)
{
    d->pixmap = new QPixmap(url);
}

void ProgressRing::setSpeed(double speed)
{
    d->speed = speed;
}

void ProgressRing::start()
{
    if (!d->rotateTimer.isActive()) {
        d->rotateTimer.start(4);
    }

    QLabel::show();
}

void ProgressRing::stop()
{
    if (d->rotateTimer.isActive()) {
        d->rotateTimer.stop();
    }

    QLabel::hide();
}

void ProgressRing::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::SmoothPixmapTransform);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setPen(Qt::NoPen);

    painter.save();

    if (d->pixmap != NULL) {
        painter.translate(d->pixmap->width() / 2, d->pixmap->height() / 2);
        painter.rotate(d->currentAngle);
        painter.drawPixmap(d->pixmap->width()/-2, d->pixmap->height()/-2,
                           *d->pixmap);
    }
    else {
        QPointF center(width()/2, height()/2);
        QConicalGradient gr(center, d->currentAngle);
        gr.setColorAt(0, QColor("#232323"));
        gr.setColorAt(1, QColor("#dedede"));
        painter.setBrush(gr);
        painter.drawEllipse(center, width()/2, height()/2);

        painter.setBrush(QColor("#f5f5f5"));
        painter.drawEllipse(center, width()/2 - 4, height()/2 - 4);        
    }

    painter.restore();
}

void ProgressRing::rotateTimer_timeout()
{
    d->currentAngle += d->speed;
    if (d->currentAngle >= 360) {
        d->currentAngle = 0;
    }
    update();
}
