#ifndef SERVOSITEMS_H
#define SERVOSITEMS_H

#include <QGraphicsItem>


class ServosItem : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)

public:
    explicit ServosItem(int parentWidth, int parentHeight);
    void setAngle(double angle);
    double getAngle() const;

    void resetChannelDefaultAngle(const QList<int>&);

signals:
    void _angleChanged(const QList<int>&);

protected:
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;

private:
    int m_servosWidth, m_servosHeight;
    double m_angle;
    double m_radius;
    QPointF m_center;
    QList<QPointF> m_channelPosList;
    QList<int> m_channelAngleList;

    void drawBackground(QPainter *painter);
    void drawChannelItems(QPainter *painter);

    QPointF getPositionByAngle(double angle);
};

#endif // SERVOSITEMS_H
