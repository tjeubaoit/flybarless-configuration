#ifndef BUTTONGROUP_H
#define BUTTONGROUP_H

#include <QButtonGroup>
#include "customwidgets_global.h"

class QLabel;
class QAbstractButton;

class CUSTOMWIDGETS_EXPORT ButtonGroup : public QButtonGroup {
    Q_OBJECT

public:
    explicit ButtonGroup(const QString&, QWidget*);

    QAbstractButton* lastButtonChecked();
    int lastButtonCheckedId() const;
    void setLastButtonChecked(QAbstractButton *lastButtonChecked = NULL);

    QLabel *checkedIcon() const;

    void setCheckIconSize(const QSize&);
    void setCheckIconSize(int, int);

private:
    class ButtonGroupPrivate;
    ButtonGroupPrivate* const d;

};

#endif // BUTTONGROUP_H
