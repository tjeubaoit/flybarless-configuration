#include "flystyleview.h"
#include "ui_flystyleview.h"
#include <QObjectData>

class FlyStyleView::FlyStyleViewPrivate {
public:
    FlyStyleViewPrivate() : value(0) {}

    int value;
};

FlyStyleView::FlyStyleView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FlyStyleView),
    d(new FlyStyleViewPrivate)
{
    ui->setupUi(this);
    ui->horizontalSlider->setEnabled(false);
}

FlyStyleView::~FlyStyleView()
{
    delete ui;
    delete d;
}

void FlyStyleView::setValue(int val)
{
    d->value = val;
    ui->horizontalSlider->setValue(val);

    switch (d->value) {
    case 1: ui->btnBeginner->setChecked(true); break;
    case 2: ui->btnSport->setChecked(true); break;
    case 3: ui->btnS3C->setChecked(true); break;
    case 4: ui->btn3D->setChecked(true); break;
    case 5: ui->btnHard3D->setChecked(true); break;
    default: return;
    }
}

int FlyStyleView::getValue() const
{
    return d->value;
}

void FlyStyleView::on_btnBeginner_clicked()
{
    d->value = 1;
    ui->horizontalSlider->setValue(1);
    emit _valueChanged(d->value);
}

void FlyStyleView::on_btnSport_clicked()
{
    d->value = 2;
    ui->horizontalSlider->setValue(2);
    emit _valueChanged(d->value);
}

void FlyStyleView::on_btnS3C_clicked()
{
    d->value = 3;
    ui->horizontalSlider->setValue(3);
    emit _valueChanged(d->value);
}

void FlyStyleView::on_btn3D_clicked()
{
    d->value = 4;
    ui->horizontalSlider->setValue(4);
    emit _valueChanged(d->value);
}

void FlyStyleView::on_btnHard3D_clicked()
{
    d->value = 5;
    ui->horizontalSlider->setValue(5);
    emit _valueChanged(d->value);
}
