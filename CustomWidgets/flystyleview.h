#ifndef FLYSTYLEVIEW_H
#define FLYSTYLEVIEW_H

#include <QWidget>
#include "customwidgets_global.h"

namespace Ui {
class FlyStyleView;
}

class CUSTOMWIDGETS_EXPORT FlyStyleView : public QWidget
{
    Q_OBJECT

public:
    explicit FlyStyleView(QWidget *parent = 0);
    ~FlyStyleView();

public slots:
    void setValue(int);
    int getValue() const;

signals:
    void _valueChanged(int);

private slots:
    void on_btnBeginner_clicked();

    void on_btnSport_clicked();

    void on_btnS3C_clicked();

    void on_btn3D_clicked();

    void on_btnHard3D_clicked();

private:
    Ui::FlyStyleView *ui;

    class FlyStyleViewPrivate;
    FlyStyleViewPrivate* const d;

};
#endif // FLYSTYLEVIEW_H
