#include "buttongroup.h"
#include <QLabel>
#include <QObjectData>
#include <QAbstractButton>

class ButtonGroup::ButtonGroupPrivate {
public:
    QAbstractButton *lastButtonChecked;
    QLabel *checkedIcon;
};

ButtonGroup::ButtonGroup(const QString& url, QWidget *parent)
    : QButtonGroup(parent),
      d(new ButtonGroupPrivate)
{    
    d->checkedIcon = new QLabel(parent);
    d->checkedIcon->setFixedSize(32, 32);
    d->checkedIcon->setPixmap(QPixmap(url));

    d->lastButtonChecked = NULL;
}

QAbstractButton *ButtonGroup::lastButtonChecked()
{
    return d->lastButtonChecked;
}

int ButtonGroup::lastButtonCheckedId() const
{
    return id(d->lastButtonChecked);
}

void ButtonGroup::setLastButtonChecked(QAbstractButton *lastButtonChecked)
{
    if (lastButtonChecked != NULL) {
        d->lastButtonChecked = lastButtonChecked;

        QPoint point(lastButtonChecked->pos().x()
                     + lastButtonChecked->width() - 22,
                     lastButtonChecked->pos().y() - 13);
        d->checkedIcon->move(point);
        d->checkedIcon->show();
    }
    else {
        d->lastButtonChecked = NULL;
        d->checkedIcon->hide();
    }
}

QLabel *ButtonGroup::checkedIcon() const
{
    return d->checkedIcon;
}

void ButtonGroup::setCheckIconSize(const QSize &size)
{
    setCheckIconSize(size.width(), size.height());
}

void ButtonGroup::setCheckIconSize(int w, int h)
{
    setCheckIconSize(w, h);
}
