#ifndef TRIMSWASH_H
#define TRIMSWASH_H

#include <QWidget>
#include "customwidgets_global.h"
#include "trimswashbase.h"


namespace Ui {
class TrimSwash;
}

class CUSTOMWIDGETS_EXPORT SwashTrimCyclic : public AbstractTrimSwash
{
    Q_OBJECT

public:
    enum TrimDirection
    {
        Horizontal = 0,
        Vertical = 1
    };

    enum TrimType
    {
        Cyclic = 0,
        MaxCollective = 1,
        MinCollective = 2
    };

    explicit SwashTrimCyclic(QWidget *parent = 0);
    ~SwashTrimCyclic();

    void setToolTipForClearButton(const QString&);

public slots:
    void setTrimMonitorRange(int, int);
    void setTrimMonitorValue(int, int);

    void setTrimValue(TrimType, TrimDirection, int);
    void setTrimRange(int, int);

signals:
        void _trimValueChanged(int, int, int, bool = false);
        void _monitorChannelValueChanged(int, int);

private slots:

        void on_btnClear_clicked();

        void on_btnNorth2_clicked();

        void on_btnNorth1_clicked();

        void on_btnWest2_clicked();

        void on_btnWest1_clicked();

        void on_btnEast2_clicked();

        void on_btnEast1_clicked();

        void on_btnSouth2_clicked();

        void on_btnSouth1_clicked();

        void on_btnTrimTypeLeft_clicked();

        void on_btnTrimTypeRight_clicked();

private:
    class SwashTrimCyclicPrivate;
    Q_DECLARE_PRIVATE(SwashTrimCyclic)

    void updateVisibleByTrimType();
    void updateTrimValue(int direction, int dVal);

    void updateTrimCollectiveMonitorValue();
    QString getHeaderWithCurrentTrimType();

    Ui::TrimSwash *ui;
};

#endif // TRIMSWASH_H
