#ifndef PROGRESSDIALOG_H
#define PROGRESSDIALOG_H

#include <QDialog>
#include "customwidgets_global.h"

namespace Ui {
class ProgressDialog;
}

class CUSTOMWIDGETS_EXPORT ProgressDialog : public QDialog
{
    Q_OBJECT

public:
    enum Type
    {
        RingType = 0,
        BarType = 1
    };

    explicit ProgressDialog(QWidget *parent = 0);
    ~ProgressDialog();

    void changeState(int = 0);

    void show();
    void hide();

    QString content() const;
    void setContent(const QString&);

    int progressBarValue() const;
    void setProgressBarValue(int);

private:
    Ui::ProgressDialog *ui;

    class ProgressDialogPrivate;
    ProgressDialogPrivate* const d;

};

#endif // PROGRESSDIALOG_H
