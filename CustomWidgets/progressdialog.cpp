#include "progressdialog.h"
#include "ui_progressdialog.h"
#include "strings.h"
#include "progressring.h"
#include <QGraphicsOpacityEffect>


class ProgressDialog::ProgressDialogPrivate
{
public:
    ProgressRing* progressRing;
    QFrame *blurBackground;
};

ProgressDialog::ProgressDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProgressDialog),
    d(new ProgressDialogPrivate)
{
    ui->setupUi(this);

    setWindowFlags(Qt::Dialog | Qt::FramelessWindowHint);
    setWindowModality(Qt::WindowModal);

    QGraphicsOpacityEffect *effect = new QGraphicsOpacityEffect();
    effect->setOpacity(0.6);

    d->blurBackground = new QFrame(parent);
    d->blurBackground->setGeometry(0, 0, parent->width(), parent->height());
    d->blurBackground->setStyleSheet("QFrame {background:#191919;border:none;}");
    d->blurBackground->setGraphicsEffect(effect);
    d->blurBackground->hide();

    d->progressRing = new ProgressRing(ui->stackedWidget->widget(0));
    d->progressRing->setGeometry(15, 12, 32, 32);
    d->progressRing->start();
}

ProgressDialog::~ProgressDialog()
{
    delete ui;
    delete d;
}

void ProgressDialog::changeState(int index)
{
    ui->stackedWidget->setCurrentIndex(index);
    if (index == RingType) {
        ui->progressBar->setValue(0);
    }
    else if (index == BarType) {
        ui->progressBar->setValue(0);
    }
}

void ProgressDialog::show()
{
    d->blurBackground->show();

    QWidget * parent = parentWidget();
    move(parent->x() + parent->width()/2 - width()/2,
         parent->y() + parent->height()/2 - height()/2);

    QWidget::show();
}

void ProgressDialog::hide()
{
    if (d->blurBackground != NULL) {
        d->blurBackground->hide();
    }

    QWidget::hide();
}

QString ProgressDialog::content() const
{
    return (ui->stackedWidget->currentIndex() == 0)
            ? ui->lbContent->text() : ui->lbContent_2->text();
}

void ProgressDialog::setContent(const QString &content)
{
    if (!content.isEmpty()) {
        if (ui->stackedWidget->currentIndex() == 0) {
            ui->lbContent->setText(content);
        } else {
            ui->lbContent_2->setText(content);
        }
    }
}

int ProgressDialog::progressBarValue() const
{
    return ui->progressBar->value();
}

void ProgressDialog::setProgressBarValue(int val)
{
    ui->progressBar->setValue(val);
}
