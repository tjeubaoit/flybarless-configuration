#include "doubledirectslider.h"
#include "ui_doubledirectslider.h"
#include <QSlider>
#include <QProgressBar>
#include <QLineEdit>
#include <QObjectData>


DoubleDirectSlider::DoubleDirectSlider(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DoubleDirectSlider)
{
    ui->setupUi(this);

    connect(ui->slider, SIGNAL(valueChanged(int)),
            this, SLOT(updateProgressOnSlider()));
}

DoubleDirectSlider::~DoubleDirectSlider()
{
    delete ui;
}

void DoubleDirectSlider::setSliderValue(int val)
{
    ui->slider->setValue(val);
}

int DoubleDirectSlider::sliderValue() const
{
    return ui->slider->value();
}

QSlider *DoubleDirectSlider::getSlider()
{
    return ui->slider;
}

QLineEdit *DoubleDirectSlider::getLineEdit()
{
    return ui->edit;
}

void DoubleDirectSlider::setSliderRange(int min, int max)
{
    int range = (max - min) / 2;

    ui->slider->setRange(min, max);
    ui->edit->setValidator(new QIntValidator(min, max));
    ui->progressUp->setRange(0, range);
    ui->progressDown->setRange(0, range);

    updateProgressOnSlider();
}

void DoubleDirectSlider::updateProgressOnSlider()
{
    int middle = (ui->slider->maximum() - ui->slider->minimum())/2
            + ui->slider->minimum();

    if (ui->slider->value() > middle) {
        ui->progressUp->setValue(ui->slider->value() - middle);
        ui->progressDown->setValue(0);
    }
    else if (ui->slider->value() < middle) {
        ui->progressUp->setValue(0);
        ui->progressDown->setValue(middle - ui->slider->value());
    }
    else {
        ui->progressUp->setValue(0);
        ui->progressDown->setValue(0);
    }
}
