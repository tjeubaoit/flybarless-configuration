TEMPLATE = subdirs

SUBDIRS += \
    CustomFrame \
    CustomWidgets \
    IOHelper \
    SerialManager \
    MainMvcApplication \
    AdaptorViews
