
#include <QtGui>

#include "custombutton.h"

CustomButton::CustomButton(CustomButton::ButtonType type, QWidget *parent) :
    QAbstractButton(parent),
    m_Type   (type),
    m_State  (STATE_NORMAL),
    m_Normal (NULL),
    m_Hovered(NULL),
    m_Clicked(NULL)
{

}

CustomButton::~CustomButton()
{
    delete m_Normal ;
    delete m_Hovered;
    delete m_Clicked;
}

void CustomButton::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);

    InitPixmaps();
}

void CustomButton::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);

    QPainter painter(this);

    if(isEnabled())
    {
        switch(m_State)
        {
        case STATE_NORMAL:
            if(m_Normal  != NULL) painter.drawPixmap(0, 0, *m_Normal );
            break;
        case STATE_HOVERED:
            if(m_Hovered != NULL) painter.drawPixmap(0, 0, *m_Hovered);
            break;
        case STATE_CLICKED:
            if(m_Clicked != NULL) painter.drawPixmap(0, 0, *m_Clicked);
            break;
        }
    }
    else
    {
        if(m_Normal != NULL) painter.drawPixmap(0, 0, *m_Normal);
    }
}


void CustomButton::enterEvent(QEvent *event)
{
    Q_UNUSED(event);

    m_State = STATE_HOVERED;

    update();
}

void CustomButton::leaveEvent(QEvent *event)
{
    Q_UNUSED(event);

    m_State = STATE_NORMAL;

    update();
}

void CustomButton::mousePressEvent(QMouseEvent *event)
{
    QAbstractButton::mousePressEvent(event);

    m_State = STATE_CLICKED;

    update();
}

void CustomButton::mouseReleaseEvent(QMouseEvent *event)
{
    QAbstractButton::mouseReleaseEvent(event);

    if(underMouse())
        m_State = STATE_HOVERED;
    else
        m_State = STATE_NORMAL;

    update();
}

void CustomButton::InitPixmaps()
{
    // Delete previous button
    InitPixmap(&m_Normal );
    InitPixmap(&m_Hovered);
    InitPixmap(&m_Clicked);

    switch(m_Type)
    {
    case BUTTON_MINIMIZE:
        InitMinimize();
        break;
    case BUTTON_MAXIMIZE:
        InitMaximize();
        break;
    case BUTTON_CLOSE:
        InitClose();
        break;
    }
}

void CustomButton::InitPixmap(QPixmap **pixmap)
{
    delete *pixmap;

    *pixmap = new QPixmap(size());

    (*pixmap)->fill(Qt::transparent);
}

void CustomButton::InitMinimize()
{
    QPainter painter;
    QPen pen;
    pen.setWidth(3);
    pen.setColor("#F7F7F7");

    painter.begin(m_Normal);
    painter.setPen(pen);
    painter.drawLine(PADDING_X, height() - 8,
                     width() - PADDING_X, height() - 8);
    painter.end();

    painter.begin(m_Hovered);
    painter.setPen(pen);
    painter.fillRect(QRectF(0, 0, width(), height()), QColor("#e67e22"));
    painter.drawLine(PADDING_X, height() - 8,
                     width() - PADDING_X, height() - 8);
    painter.end();

    painter.begin(m_Clicked);
    painter.setPen(pen);
    painter.fillRect(QRectF(0, 0, width(), height()), QColor("#d35400"));
    painter.drawLine(PADDING_X, height() - 8,
                     width() - PADDING_X, height() - 8);
    painter.end();
}

void CustomButton::InitMaximize()
{

}

void CustomButton::InitClose()
{
    QPainter painter;
    QPen pen;
    pen.setWidth(2);
    pen.setColor("#F7F7F7");

    painter.begin(m_Normal);
    painter.setPen(pen);
    painter.drawLine(PADDING_X, PADDING_Y*3,
                     width() - PADDING_X, height() - PADDING_Y);
    painter.drawLine(width() - PADDING_X, PADDING_Y*3,
                     PADDING_X, height() - PADDING_Y);
    painter.end();

    painter.begin(m_Hovered);
    painter.setPen(pen);
    painter.fillRect(QRectF(0, 0, width(), height()), QColor("#e67e22"));
    painter.drawLine(PADDING_X, PADDING_Y*3,
                     width() - PADDING_X, height() - PADDING_Y);
    painter.drawLine(width() - PADDING_X, PADDING_Y*3,
                     PADDING_X, height() - PADDING_Y);
    painter.end();

    painter.begin(m_Clicked);
    painter.setPen(pen);
    painter.fillRect(QRectF(0, 0, width(), height()), QColor("#d35400"));
    painter.drawLine(PADDING_X, PADDING_Y*3,
                     width() - PADDING_X, height() - PADDING_Y);
    painter.drawLine(width() - PADDING_X, PADDING_Y*3,
                     PADDING_X, height() - PADDING_Y);
    painter.end();
}
