
#include <QtGui>
#include <QDesktopWidget>

#include "customframe.h"

CustomFrame::CustomFrame(QWidget *parent) :
    QWidget(parent),
    m_Cache(new QPixmap),
    m_SizeGrip(this),
    m_TitleBar(this)
{
    m_TitleBar.move(0, 0);

    m_SizeGrip.setStyleSheet("image: none");

    setWindowFlags(Qt::FramelessWindowHint | Qt::WindowMinimizeButtonHint);

    connect(this, SIGNAL(WindowTitleChanged(QString)),
            &m_TitleBar, SLOT(UpdateWindowTitle(QString)));

#if QT_VERSION >= 0x040500
#ifdef Q_WS_X11
    if(x11Info().isCompositingManagerRunning())
        setAttribute(Qt::WA_TranslucentBackground);
#else
    setAttribute(Qt::WA_TranslucentBackground);
#endif
#endif
}

CustomFrame::~CustomFrame()
{
    delete m_Cache;
}

void CustomFrame::setWindowTitle(const QString &title)
{
    QWidget::window()->setWindowTitle(title);

    emit WindowTitleChanged(title);
}

void CustomFrame::showEvent(QShowEvent *event)
{
    Q_UNUSED(event);
    CenterOnScreen();
}

void CustomFrame::paintEvent(QPaintEvent *event)
{
    Q_UNUSED (event);

    if(m_Cache != NULL)
    {
        QPainter painter(this);
        painter.drawPixmap(0, 0, *m_Cache);
#if QT_VERSION >= 0x040500
        if(!testAttribute(Qt::WA_TranslucentBackground))
            setMask(m_Cache->mask());
#else
        setMask(m_Cache->mask());
#endif
    }
}

void CustomFrame::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);

    delete m_Cache;
    m_Cache = new QPixmap(size());
    m_Cache->fill(Qt::transparent);
    QPainter painter(m_Cache);

    QRectF frame(0, 0, width(), height());

    if (backgroundUrl.isEmpty()) {
        QColor startColor(backgroundColorHexCode);
        QColor endColor(backgroundColorHexCode);

        QLinearGradient gr(0, 0, 0, height());
        gr.setColorAt(0, startColor);
        gr.setColorAt(1, endColor);

        painter.setPen(Qt::NoPen);
        painter.setBrush(gr);
        painter.drawRect(frame);
    }
    else {
        QPixmap pixmap(backgroundUrl);
        painter.drawPixmap(frame, pixmap, pixmap.rect());
    }

    m_SizeGrip.move(width() - 32, height() - 32);
    m_SizeGrip.resize(32, 32);
}

QString CustomFrame::getBackgroundColorHexCode() const
{
    return backgroundColorHexCode;
}

void CustomFrame::setBackgroundColorHexCode(const QString &value)
{
    backgroundColorHexCode = value;
}

void CustomFrame::setBackgroundImage(const QString &url)
{
    backgroundUrl = url;
}

void CustomFrame::setWindowSize(int width, int height)
{
    setFixedSize(width, height);
}

void CustomFrame::setWindowTitleSize(int width, int height)
{
    m_TitleBar.setFixedSize(width, height);
}

void CustomFrame::CenterOnScreen()
{
    QDesktopWidget screen;

    QRect screenGeom = screen.screenGeometry(this);

    int screenCenterX = screenGeom.center().x();
    int screenCenterY = screenGeom.center().y();

    move(screenCenterX - width () / 2,
        screenCenterY - height() / 2);
}
