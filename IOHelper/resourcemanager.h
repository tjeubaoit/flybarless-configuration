#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H

#include <QObject>
#include <QHash>
#include "iohelper_global.h"


/**
 * @brief The ResourceManager class
 * use Singleton Design Pattern
 */
class IOHELPERSHARED_EXPORT ResourceManager : public QObject
{
    Q_OBJECT
public:
    ~ResourceManager();

    QString getValue(int key);

    static ResourceManager* instance(const QString& = "");

protected:
    ResourceManager(const QString&);

private:
    class ResourceManagerPrivate;
    ResourceManagerPrivate* const d;

};

#endif // RESOURCEMANAGER_H
