#ifndef DOWNLOADHELPER_H
#define DOWNLOADHELPER_H

#include "iohelper_global.h"
#include <QObject>

class DownloadHelperPrivate;

class IOHELPERSHARED_EXPORT DownloadHelper : public QObject
{
    Q_OBJECT
public:
    enum State {
        ConnectingState,
        GetFileLenState,
        GetShaCksState,
        GetFileDateState,
        ClosingState
    };

    explicit DownloadHelper(const QString&, int, QObject *parent = 0);
    ~DownloadHelper();

    void setServerAddress(const QString&, int);

signals:
    void _firmwareDownloaded(const QByteArray&);
    void _progressChanged(int);

public slots:
    void startDownload();
    void closeConnection();

private:
    DownloadHelperPrivate* const d;
    friend class DownloadHelperPrivate;

};

#endif // DOWNLOADHELPER_H
