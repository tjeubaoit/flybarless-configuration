#include "appconfigmanager.h"
#include <QFile>
#include <QTextStream>
#include <QStringList>


class AppConfigManager::AppConfigManagerPrivate
{
public:
    QHash<QString, QString> hash;
    QString filePath;
};

AppConfigManager::AppConfigManager(const QString &filePath)
    : d(new AppConfigManagerPrivate)
{
    d->filePath = filePath;

    QFile file(d->filePath);
    if (!file.exists())
        return;
    if (file.open(QIODevice::ReadOnly)) {
        QTextStream stream(&file);
        while (!stream.atEnd()) {
            QStringList line = stream.readLine().split("?");
            if (line.length() > 1)
                d->hash.insert(line.at(0), line.at(1));
        }
        file.close();
    }
}

AppConfigManager::~AppConfigManager()
{
    delete d;
}

void AppConfigManager::saveConfigToFile()
{
    QFile file(d->filePath);
    if (file.open(QIODevice::WriteOnly)) {
        foreach (const QString &key, d->hash.keys()) {
            QTextStream stream(&file);
            stream << key << "?" << d->hash.value(key) << "\r\n";
        }
        file.close();
    }
}

QString AppConfigManager::get(const QString &key)
{
    return d->hash.value(key);
}

bool AppConfigManager::hasKey(const QString &key)
{
    return d->hash.contains(key);
}

void AppConfigManager::put(const QString &key, const QString &val)
{
    d->hash.insert(key, val);
}
