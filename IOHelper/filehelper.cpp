#include "filehelper.h"
#include <QString>
#include <QTextStream>
#include <QCryptographicHash>
#include <QDebug>
#include <QFile>

#define hashSha256(bytes) QCryptographicHash::hash(bytes, QCryptographicHash::Sha256)

QString qLoadHtmlString(const QString &fileName)
{
    QFile file(fileName);
    QString html;

    if (file.exists() && file.open(QIODevice::ReadOnly)) {
        QTextStream stream(&file);
        html = stream.readAll();
    }
    return html;
}

int qSaveParametersToFile(const QString &filePath,
                          const QMap<qint32, qint32> &dict)
{
    QFile file(filePath);
    if (file.open(QIODevice::WriteOnly)) {

        QByteArray bytes;

        foreach (int key, dict.keys()) {
            char first = (char)((key >> 8) & 0xFF);
            char second = (char)(key & 0xFF);
            bytes.append(first).append(second);

            int value = dict.value(key);
            first = (char)((value >> 8) & 0xFF);
            second = (char)(value & 0xFF);
            bytes.append(first).append(second);
        }

        QByteArray hash = hashSha256(bytes);
        file.write(hash);
        file.write(bytes);

        file.flush();
        file.close();

        return 0;
    }
    return -1;
}

int qLoadParametersFromFile(const QString &filePath, QMap<qint32, qint32>* dict)
{
    QFile file(filePath);

    if (file.exists() && file.open(QIODevice::ReadOnly)) {
        QByteArray checksum = file.read(32);
        QByteArray data = file.readAll();
        file.close();

        if (hashSha256(data) == QString::fromUtf8(checksum)) {
            for (int i = 0; i <= data.size() - 4; i = i + 4) {
                int key = (data[i] << 8) | (data[i+1] & 0x00FF);
                int value = (data[i+2] << 8) | (data[i+3] & 0x00FF);
                dict->insert(key, value);
            }
            return 0;
        }
    }
    return -1;
}

