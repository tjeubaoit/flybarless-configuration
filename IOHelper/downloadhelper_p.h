#ifndef DOWNLOADHELPER_P_H
#define DOWNLOADHELPER_P_H

#include <QByteArray>
#include <QTcpSocket>
#include <QEventLoop>

class DownloadHelper;

class DownloadHelperPrivate : public QObject
{
    Q_OBJECT

public:
    DownloadHelperPrivate(DownloadHelper *q_ptr, const QString& addr, int port)
        : serverAddr(addr),
          serverPort(port),
          q(q_ptr)
    {
        qRegisterMetaType<QAbstractSocket::SocketError>("QAbstractSocket::SocketError");

        connect(&socket, SIGNAL(hostFound()), this, SLOT(server_hostFound()));
        connect(&socket, SIGNAL(connected()), this, SLOT(client_connected()));
        connect(&socket, SIGNAL(disconnected()), this, SLOT(client_disconected()));
        connect(&socket, SIGNAL(readyRead()), this, SLOT(dataAvailable()));
        connect(&socket, SIGNAL(error(QAbstractSocket::SocketError)),
                this, SLOT(socket_error(QAbstractSocket::SocketError)));
    }

    QString serverAddr;
    int serverPort;

    QTcpSocket socket;
    QByteArray buffer;
    QEventLoop eventLoop;

    DownloadHelper* const q;

public slots:
    void server_hostFound();
    void dataAvailable();
    void client_connected();
    void client_disconected();
    void socket_error(const QAbstractSocket::SocketError&);
};

#endif // DOWNLOADHELPER_P_H
