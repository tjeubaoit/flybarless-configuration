#include "downloadhelper.h"
#include "downloadhelper_p.h"
#include <QDebug>
#include <QFile>
#include <QThread>

#define BUFFER_LENGTH 1024

void DownloadHelperPrivate::server_hostFound()
{
    qDebug() << "host found";
}

void DownloadHelperPrivate::dataAvailable()
{
    while (!socket.atEnd()) {
        QByteArray bytes = socket.read(BUFFER_LENGTH);
        buffer.append(bytes);

        qDebug() << "buffer length" << buffer.length();

        emit q->_progressChanged(qRound((buffer.length()/25140.0d) * 100));
    }
}

void DownloadHelperPrivate::client_connected()
{
    qDebug() << "client connected to " << serverAddr
             << "at port" << serverPort;
}

void DownloadHelperPrivate::client_disconected()
{
    qDebug() << "client disconnected to " << serverAddr
             << "at port" << serverPort;

    if (buffer.length() > 0) {
        emit q->_firmwareDownloaded(buffer);
    }

    eventLoop.exit(0);
}

void DownloadHelperPrivate::socket_error(const QAbstractSocket::SocketError &)
{
    qDebug() << "socket error" << socket.errorString();
}


/**
 * @brief DownloadHelper::DownloadHelper
 * @param addr
 * @param port
 * @param parent
 */
DownloadHelper::DownloadHelper(const QString &addr, int port, QObject *parent)
    : QObject(parent),
      d(new DownloadHelperPrivate(this, addr, port))
{
}

DownloadHelper::~DownloadHelper()
{
    closeConnection();
    delete d;
}

void DownloadHelper::setServerAddress(const QString &addr, int port)
{
    d->serverAddr = addr;
    d->serverPort = port;
}

void DownloadHelper::startDownload()
{
    qDebug() << "start connect to server";
    d->socket.connectToHost(d->serverAddr, d->serverPort);

    if (! d->eventLoop.isRunning()) {
        d->eventLoop.exec();
    }
}

void DownloadHelper::closeConnection()
{
    qDebug() << "close connection";
    d->socket.close();
}
