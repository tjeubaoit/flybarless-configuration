#ifndef SERIALBASE_H
#define SERIALBASE_H

#include <QThread>
#include "serialmanager_global.h"

class AbstractSerialReader;
class AbstractSerialWritter;
class QSerialPort;

class SERIALMANAGERSHARED_EXPORT AbstractSerialManager : public QThread
{
    Q_OBJECT

public:
    virtual bool isPortConnected() const = 0;

Q_SIGNALS:
    void _portStatusChanged(int);
    void _responseToUpdateUi(const QVariant&);

    void _flashProgressChanged(int);
    void _flashStatusChanged(int);

public slots:
    virtual void startUpdateFirmware(const QByteArray&) = 0;

    virtual void readSingleAddress(const QVariant&) = 0;
    virtual void readMultipleAddress(const QVariant&) = 0;
    virtual void writeConfig(const QVariant&) = 0;
    virtual void writeConfigBlocking(const QVariant&) = 0;
};


class SERIALMANAGERSHARED_EXPORT AbstractSerialWritter : public QThread
{
    Q_OBJECT

public:
    virtual QSerialPort* getPort() const = 0;
    virtual void setPort(QSerialPort *) = 0;
    virtual void setPortMutex(QMutex* portMutex) = 0;

public slots:
    virtual void readSingleAddress(const QVariant&) = 0;
    virtual void readMultipleAddress(const QVariant&) = 0;
    virtual void writeConfig(const QVariant&) = 0;
    virtual void writeConfigBlocking(const QVariant&) = 0;
};


class SERIALMANAGERSHARED_EXPORT AbstractSerialReader : public QThread
{
    Q_OBJECT

public:
    virtual QSerialPort* getPort() const = 0;
    virtual void setPort(QSerialPort *) = 0;
    virtual void setPortMutex(QMutex* portMutex) = 0;

Q_SIGNALS:
    void _responseToUpdateUi(const QVariant&);

public slots:
    virtual void readPortDataToBuffer() = 0;
};

#endif // SERIALBASE_H
