#include "serialreader.h"
#include <QDebug>
#include <QTextStream>
#include <QDataStream>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QMutexLocker>
#include <QSerialPort>

#define BUFFER_LENGTH 1024

class SerialReader::SerialReaderPrivate
{
public:
    QByteArray buffer;
    QSerialPort *comPort;
    QMutex *portMutex;
};

SerialReader::SerialReader(QObject *) :
    d(new SerialReaderPrivate)
{
    moveToThread(this);
}

SerialReader::~SerialReader()
{
    delete d;
}

QSerialPort *SerialReader::getPort() const
{
    return d->comPort;
}

void SerialReader::setPort(QSerialPort *port)
{
    d->comPort = port;
}

void SerialReader::readPortDataToBuffer()
{
    while (!d->comPort->atEnd()) {
        char bytes[128];
        int bytesRead = d->comPort->read(bytes, 128);

        if (bytesRead > 0) {
            d->buffer.append(bytes, bytesRead);
        } else {
            qDebug() << "readline error";
        }
    }

    while (d->buffer.contains("\n")) {
        int index = d->buffer.indexOf("\n");
        QByteArray line = d->buffer.mid(0, index + 1).simplified();

        QRegularExpression re("^[\\d][\\d][\\d][\\d],[\\d][\\d]?[\\d]?[\\d]?$");
        QRegularExpressionMatch match = re.match(line);

        if (match.capturedLength() == line.size()) {
            qDebug() << "emit" << line;
            emit _responseToUpdateUi(line);
        }

        d->buffer.remove(0, index + 1);
    }
}

void SerialReader::setPortMutex(QMutex *portMutex)
{
    d->portMutex = portMutex;
}

