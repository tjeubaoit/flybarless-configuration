#ifndef SERIALABSTRACTFACTORY_H
#define SERIALABSTRACTFACTORY_H

#include "serialmanager_global.h"

class AbstractSerialReader;
class AbstractSerialWritter;

/**
 * @brief The SerialAbstractFactory class
 * apply Abstract Factory Pattern
 */
class SERIALMANAGERSHARED_EXPORT AbstractSerialFactory
{
public:
    // factory methods
    virtual AbstractSerialReader* createReader() = 0;
    virtual AbstractSerialWritter* createWritter() = 0;
};

#endif // SERIALABSTRACTFACTORY_H
