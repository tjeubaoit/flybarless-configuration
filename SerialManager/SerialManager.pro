#-------------------------------------------------
#
# Project created by QtCreator 2014-08-13T15:22:11
#
#-------------------------------------------------

QT      += core serialport
QT       -= gui

TARGET = vtserial
TEMPLATE = lib

DEFINES += SERIALMANAGER_LIBRARY

SOURCES +=  serialwritter.cpp \
    serialreader.cpp \
    serialmanager.cpp \
    flashhelper.cpp

HEADERS +=  serialwritter.h \
        serialreader.h \
        serialmanager.h \
        serialmanager_p.h \
        serialmanager_global.h \
    flashhelper.h \
    serialbase.h \
    serialabstractfactory.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../IOHelper/release/ -lvtio
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../IOHelper/debug/ -lvtio
else:unix: LIBS += -L$$OUT_PWD/../IOHelper/ -lvtio

INCLUDEPATH += $$PWD/../IOHelper
DEPENDPATH += $$PWD/../IOHelper
