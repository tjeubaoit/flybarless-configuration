#ifndef SERIALMANAGER_GLOBAL_H
#define SERIALMANAGER_GLOBAL_H

#include <QtCore/qglobal.h>
#include <QMutex>

#if defined(SERIALMANAGER_LIBRARY)
#  define SERIALMANAGERSHARED_EXPORT Q_DECL_EXPORT
#else
#  define SERIALMANAGERSHARED_EXPORT Q_DECL_IMPORT
#endif

#ifndef NO_USE_GLOBAL_FUNCTIONS
#   define READ_MESSAGE(addr) "*"+addr+"?#"
#   define WRITE_MESSAGE(addr, value) "*"+addr+"="+value+"#"
#   define SLEEP(delay) QThread::currentThread()->msleep(delay);
#   define FILL(text, len) while (text.length() < len) { text.insert(0, "0"); }
#   define TO_BYTES(str) QString(str).toUtf8();
#endif

class SyncHelper
{
public:
    static QMutex g_mutex;
};

#endif // SERIALMANAGER_GLOBAL_H
