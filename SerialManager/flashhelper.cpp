#include "flashhelper.h"
#include "downloadhelper.h"
#include "serialmanager_global.h"
#include <QFile>
#include <QTextStream>
#include <QThread>
#include <QCoreApplication>
#include <QDebug>
#include <QSerialPort>

class FlashHelperPrivate
{
public:
    FlashHelperPrivate(FlashHelper *q_ptr)
        : m_currentFlashPercent(0),
          q(q_ptr)
    {
    }

    QSerialPort *m_port;
    QByteArray m_buffer;
    int m_currentFlashPercent;

    FlashHelper* const q;

public slots:
    void write4Byte(QByteArray,char,char);
    bool writeFlashDataToDevice();
};

void FlashHelperPrivate::write4Byte(QByteArray data, char mesClass, char mesID)
{
    char cka = 0, ckb = 0;
    QByteArray tmpArr;
    tmpArr.resize(11);
    tmpArr[0] = 181;
    tmpArr[1] =  98;
    tmpArr[2] = mesClass;
    tmpArr[3] = mesID;
    tmpArr[4] = 4;

    for(int i = 0; i < 4; i++) {
        tmpArr[5+i] = data[i];
    }

    for(int i = 0; i < 7; i++) {
        cka += tmpArr[2+i];
        ckb += cka;
    }

    tmpArr[9] = cka;
    tmpArr[10] = ckb;

    m_port->write(tmpArr);
    m_port->waitForBytesWritten(30);
}

bool FlashHelperPrivate::writeFlashDataToDevice()
{
    if (m_buffer.isNull() || m_buffer.isEmpty()) {
        qDebug() << "buffer empty, return";
        return false;
    }
    qDebug() << "buffer size" << m_buffer.size();

    emit q->_flashStatusChanged(FlashHelper::StartEraseFlash);

    QString tmpStr;
    int timeout = 0;
    m_port->write("*goto3007\r", 10);
    m_port->waitForReadyRead(100);

    do {
        while (!m_port->atEnd()) {
            tmpStr.append(m_port->readAll());
        }
        if (tmpStr.contains("booted"))
            break;

        m_port->write("$boot?\r", 7);
        m_port->waitForReadyRead(10);
        timeout++;
        qDebug() << "timeout counter" << timeout;
    }
    while (timeout < 10);

    if (!tmpStr.contains("booted")) {
        emit q->_flashStatusChanged(FlashHelper::FlashResultError);
        return false;
    }
    qDebug() << "go to boot success";

    QByteArray fileLength;
    fileLength.resize(4);
    int len = m_buffer.length();
    qDebug() << len;

    fileLength[0] = (len/4 >> 24);
    fileLength[1] = (len/4 >> 16) & 0xff;
    fileLength[2] = (len/4 >> 8) & 0xff;
    fileLength[3] = (len/4 & 0xff);
    write4Byte(fileLength, 0x0E, 0x0E);

    m_port->write("$erase?\r", 8);
    SLEEP(5000);

    emit q->_flashStatusChanged(FlashHelper::StartWriteFirmware);

    QByteArray bytes;
    bytes.resize(4);
    m_currentFlashPercent = 0;

    for (int i = 0; i < len; i += 4) {
        for (int j = 0; j < 4; j++) {
            bytes[j] = m_buffer[i+j];
        }
        write4Byte(bytes, 0x01, 0x02);

        int percent = qRound((double)i / len * 100);
        if (percent - m_currentFlashPercent == 1) {
            m_currentFlashPercent = percent;
            emit q->_flashProgressChanged(percent);
        }
    }
    write4Byte(fileLength, 0xEE, 0xEE);

    return true;
}


/**
 * @brief FlashHelper::FlashHelper
 * @param parent
 */
FlashHelper::FlashHelper(QObject *parent) :
    QObject(parent),
    d(new FlashHelperPrivate(this))
{
}

FlashHelper::~FlashHelper()
{
    delete d;
}

void FlashHelper::setFlashData(const QByteArray& data)
{
    qDebug() << "get flash data";
    d->m_buffer = data;
}

void FlashHelper::setPort(QSerialPort *port)
{
    d->m_port = port;
}

bool FlashHelper::updateFlash(const QByteArray& bytes)
{
//    emit _flashStatusChanged(StartDownloadNewFirmware);

//    DownloadHelper downloader("localhost", 9999);

//    connect(&downloader, SIGNAL(_firmwareDownloaded(QByteArray)),
//            this, SLOT(setFlashData(QByteArray)));
//    connect(&downloader, SIGNAL(_progressChanged(int)),
//            this, SIGNAL(_flashProgressChanged(int)));

//    downloader.startDownload();

    d->m_buffer = bytes;
    return d->writeFlashDataToDevice();
}

void FlashHelper::emitFlashStatus(int status)
{
    emit _flashStatusChanged(status);
}


