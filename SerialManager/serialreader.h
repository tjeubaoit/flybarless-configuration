#ifndef SERIALREADER_H
#define SERIALREADER_H

#include <QThread>
#include <QByteArray>
#include "serialbase.h"

class QSerialPort;
class QMutex;

class SerialReader : public AbstractSerialReader
{
    Q_OBJECT

public:
    SerialReader(QObject *parent = 0);
    ~SerialReader();

    QSerialPort* getPort() const;
    void setPort(QSerialPort *);
    void setPortMutex(QMutex* portMutex);

     void readPortDataToBuffer();

private:
    class SerialReaderPrivate;
    SerialReaderPrivate* const d;
};

#endif // SERIALREADER_H
