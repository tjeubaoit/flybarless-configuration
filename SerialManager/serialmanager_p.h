#ifndef SERIALMANAGER_P_H
#define SERIALMANAGER_P_H

#include <QTimer>
#include <QSerialPort>

class SerialManager;
class AbstractSerialWritter;
class AbstractSerialReader;

class SerialManagerPrivate : public QObject
{
    Q_OBJECT

public:
    SerialManagerPrivate(SerialManager* q_ptr, QObject* parent = 0);
    ~SerialManagerPrivate();

    SerialManager* const q;
    friend class SerialManager;

    AbstractSerialReader* reader;
    AbstractSerialWritter* writter;
    QSerialPort *comPort;

    QTimer scanComTimer;

    QList<quint16> productIds;
    QList<quint16> vendorIds;

    QString helloRequest;
    QString helloResponse;

    bool openPort(QSerialPort*);
    bool configureForPort(QSerialPort*);
    bool checkDevice(QSerialPort*);

    void startWorkerThreads();
    void stopWorkerThreads();
    void closeAndDestroyPort();

public Q_SLOTS:
    void scanCom();

    virtual void handleSerialPortError(const QSerialPort::SerialPortError&);
    virtual void onConnect();
    virtual void onDisconnect();

    void stopScanComTimer();
    void startScanComTimer();

Q_SIGNALS:
    void _needReadMultipleAddr(const QVariant&);
    void _needReadSingleAddr(const QVariant&);
    void _needWriteConfig(const QVariant&);
    void _needWriteConfigBlocking(const QVariant&);

};

#endif // SERIALMANAGER_P_H
