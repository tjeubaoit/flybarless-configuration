#ifndef SERIALMANAGER_H
#define SERIALMANAGER_H

#include <QThread>
#include "serialreader.h"
#include "serialwritter.h"
#include "serialmanager_global.h"
#include "serialbase.h"
#include "serialabstractfactory.h"

class SerialManagerPrivate;
class QSerialPort;

class SERIALMANAGERSHARED_EXPORT SerialManager
        : public AbstractSerialManager, public AbstractSerialFactory
{
    Q_OBJECT
public:
    enum PortStatus {
        PortConnected = 100,
        PortDisconneted = -100,
    };

    explicit SerialManager(QObject *parent = 0);
    ~SerialManager();

    bool isPortConnected() const;
    QSerialPort* getPort() const;

    void setHelloPairString(const QString& req, const QString& res);
    void addProductId(const QVariant&);
    void addVendorId(const QVariant&);

    void startUpdateFirmware(const QByteArray&);
    void readSingleAddress(const QVariant&);
    void readMultipleAddress(const QVariant&);
    void writeConfig(const QVariant&);
    void writeConfigBlocking(const QVariant&);

public slots:
    void tryConnect();
    void tryReconnect();

    void clearAllSignalsInReader();

protected:
    virtual AbstractSerialReader* createReader();
    virtual AbstractSerialWritter* createWritter();

    SerialManagerPrivate* const d;
    friend class SerialManagerPrivate;

};

#endif // SERIALMANAGER_H
