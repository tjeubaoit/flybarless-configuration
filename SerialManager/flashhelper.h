#ifndef FLASHHELPER_H
#define FLASHHELPER_H

#include <QObject>
#include "serialmanager_global.h"

class FlashHelperPrivate;
class QSerialPort;

class SERIALMANAGERSHARED_EXPORT FlashHelper : public QObject
{
    Q_OBJECT
public:
    enum FlashStatus
    {
        StartDownloadNewFirmware = 1,
        DownloadFirmwareError = -1,
        PortCannotOpen = -2,
        StartEraseFlash = 3,
        StartWriteFirmware = 4,
        StartReconnect = 5,
        FlashResultOk = 0,
        FlashResultError = -999
    };

    explicit FlashHelper(QObject *parent = 0);
    ~FlashHelper();

signals:
    void _flashStatusChanged(int);
    void _flashProgressChanged(int);

public slots:
    void emitFlashStatus(int status);

    void setPort(QSerialPort *port);
    void setFlashData(const QByteArray&);

    virtual bool updateFlash(const QByteArray &);

private:
    FlashHelperPrivate* const d;
    friend class FlashHelperPrivate;
};

#endif // FLASHHELPER_H
