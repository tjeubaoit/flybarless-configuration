#include "serialwritter.h"
#include "serialmanager_global.h"
#include <QTextStream>
#include <QVariant>
#include <QMutexLocker>
#include <QDebug>
#include <QSerialPort>

#define WRITE_DELAY 100
#define READ_DELAY 5
#define SPLIT_CHAR ','


class SerialWritter::SerialWritterPrivate
{
public:
    QSerialPort *comPort;
    QMutex *portMutex;
};

SerialWritter::SerialWritter(QObject *) :
    d(new SerialWritterPrivate)
{
    moveToThread(this);
}

SerialWritter::~SerialWritter()
{
    delete d;
}

QSerialPort *SerialWritter::getPort() const
{
    return d->comPort;
}

void SerialWritter::setPort(QSerialPort *port)
{
    d->comPort = port;
}

void SerialWritter::setPortMutex(QMutex *portMutex)
{
    d->portMutex = portMutex;
}

void SerialWritter::readSingleAddress(const QVariant &addr)
{
    QString text = READ_MESSAGE(addr.toString());
    d->comPort->write(text.toUtf8());
    SLEEP(WRITE_DELAY)
}

void SerialWritter::readMultipleAddress(const QVariant &addrs)
{    
    if (d->comPort == NULL) {
        return;
    }

    foreach (const QString addr, addrs.toString().split(SPLIT_CHAR))
    {
        if (addr.isEmpty()) {
            continue;
        }

        QString text = READ_MESSAGE(addr);
        d->comPort->write(text.toUtf8());

        SLEEP(READ_DELAY)
    }
}

void SerialWritter::writeConfig(const QVariant& data)
{
    if (d->comPort == NULL) {
        return;
    }

    QStringList list = data.toString().split(SPLIT_CHAR);
    FILL(list.first(), 4);
    FILL(list.last(), 3);

    QString text = WRITE_MESSAGE(list.first(), list.last());
    qDebug() << "send write: " << text;

    d->comPort->write(text.toUtf8());

    SLEEP(WRITE_DELAY)
}

void SerialWritter::writeConfigBlocking(const QVariant &data)
{
    writeConfig(data);
}
