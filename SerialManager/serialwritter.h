#ifndef SERIALWRITTER_H
#define SERIALWRITTER_H

#include <QThread>
#include "serialbase.h"

class QSerialPort;
class QMutex;
class QVariant;

class SerialWritter : public AbstractSerialWritter
{
    Q_OBJECT

public:
    SerialWritter(QObject *parent = 0);
    ~SerialWritter();

    QSerialPort* getPort() const;
    void setPort(QSerialPort *);
    void setPortMutex(QMutex* portMutex);

    void readSingleAddress(const QVariant&);
    void readMultipleAddress(const QVariant&);
    void writeConfig(const QVariant&);
    void writeConfigBlocking(const QVariant&);

private:
    class SerialWritterPrivate;
    SerialWritterPrivate* const d;
};

#endif // SERIALWRITTER_H
