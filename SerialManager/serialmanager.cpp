#include "serialmanager.h"
#include "serialmanager_p.h"
#include "flashhelper.h"

#include <QSerialPort>
#include <QSerialPortInfo>
#include <QTextStream>
#include <QFile>
#include <QDebug>
#include <QCoreApplication>

#define MAX_TIME_WAIT_THREAD_DESTROY 1000
#define TIME_TRY_SCANCOM 2000
#define MAX_NUMBER_TRY_CONNECT 2

QMutex SyncHelper::g_mutex;

SerialManagerPrivate::SerialManagerPrivate(SerialManager* q_ptr, QObject* parent)
    : QObject(parent),
    q(q_ptr),
    comPort(NULL)
{
    reader = q->createReader();
    writter = q->createWritter();

    scanComTimer.moveToThread(q);
    scanComTimer.setInterval(TIME_TRY_SCANCOM);

    connect(&scanComTimer, SIGNAL(timeout()), this, SLOT(scanCom()));

    connect(this, SIGNAL(_needReadMultipleAddr(QVariant)),
            writter, SLOT(readMultipleAddress(QVariant)));

    connect(this, SIGNAL(_needReadSingleAddr(QVariant)),
            writter, SLOT(readSingleAddress(QVariant)));

    connect(this, SIGNAL(_needWriteConfig(QVariant)),
            writter, SLOT(writeConfig(QVariant)));

    connect(this, SIGNAL(_needWriteConfigBlocking(QVariant)),
            writter, SLOT(writeConfig(QVariant)),
            Qt::BlockingQueuedConnection);

    connect(reader, SIGNAL(_responseToUpdateUi(QVariant)),
            q, SIGNAL(_responseToUpdateUi(QVariant)));
}

SerialManagerPrivate::~SerialManagerPrivate()
{
    if (reader) delete reader;
    if (writter) delete writter;
}

void SerialManagerPrivate::closeAndDestroyPort()
{
    if (comPort != NULL) {
        if (comPort->isOpen()) {
            comPort->close();
        }
        delete comPort;
        comPort = NULL;
    }
}

void SerialManagerPrivate::stopWorkerThreads()
{
    if (reader->isRunning()) {
        reader->quit();
        reader->wait(MAX_TIME_WAIT_THREAD_DESTROY);
    }

    if (writter->isRunning()) {
        writter->quit();
        writter->wait(MAX_TIME_WAIT_THREAD_DESTROY);
    }

    reader->setPort(NULL);
    writter->setPort(NULL);
}

void SerialManagerPrivate::startWorkerThreads()
{
    reader->setPort(comPort);
    writter->setPort(comPort);

    reader->setPortMutex(&SyncHelper::g_mutex);
    writter->setPortMutex(&SyncHelper::g_mutex);

    if (!reader->isRunning()) {
        reader->start(QThread::NormalPriority);
    }

    if (!writter->isRunning()) {
        writter->start(QThread::HighPriority);
    }
}

void SerialManagerPrivate::scanCom()
{
    if (QSerialPortInfo::availablePorts().size() <= 0) {
        qDebug() << "no serial port interface detected";
        return;
    }

    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        if (!productIds.contains(info.productIdentifier())
                || !vendorIds.contains(info.vendorIdentifier()))
        {
#ifdef RELEASE_VERSION
            qDebug() << "product id or vendor id not match"
                      << info.vendorIdentifier() << " "
                      << info.productIdentifier();
            continue;
#endif
        }

        if (info.isNull()) {
            qDebug() << info.portName() << " null";
            return;
        }
        else if (!info.isValid()) {
            qDebug() << info.portName() << " invalid";
            return;
        }
        else if (info.isBusy()) {
            qDebug() << info.portName() << " busy";
            return;
        }

        QSerialPort *port = new QSerialPort(info);

        if (openPort(port) && checkDevice(port)) {
            comPort = port;
            onConnect();
            return;
        }

        delete port;
    }
}

bool SerialManagerPrivate::openPort(QSerialPort* port)
{
    if (port->isOpen()) {
        port->clear(QSerialPort::AllDirections);
        port->close();
    }

    if (! port->open(QIODevice::ReadWrite)) {
        qDebug() << "port cannot open" << port->errorString();
        return false;
    }

    if (! configureForPort(port)) {
        port->close();
        return false;
    }

    return true;
}

bool SerialManagerPrivate::configureForPort(QSerialPort *port)
{
    if (!port->setFlowControl(QSerialPort::NoFlowControl)) {
        qDebug() << "cannot set flow control";
        return false;
    }

    if (!port->setDataBits(QSerialPort::Data8)) {
        qDebug() << "cannot set databits";
        return false;
    }

    if (!port->setBaudRate(QSerialPort::Baud115200)) {
        qDebug() << "cannot set baudrate";
        return false;
    }

    if (!port->setStopBits(QSerialPort::OneStop)) {
        qDebug() << "cannot set stopbits";
        return false;
    }

    if (!port->setParity(QSerialPort::NoParity)) {
        qDebug() << "cannot set parity";
        return false;
    }

    return true;
}

bool SerialManagerPrivate::checkDevice(QSerialPort *port)
{
    QString request = helloRequest;
    if (port->write(request.toUtf8()) < 0)
        return false;
    port->flush();
    SLEEP(50)

    if (port->waitForReadyRead(1000)) {
        QString data;
        while (!port->atEnd()) {
            data.append(port->readAll());
            SLEEP(10)
        }
        qDebug() << data;
        if (data.contains(helloResponse)) {
            return true;
        }
        qDebug() << "hello response string not match";
    }
    qDebug() << "time out, no response";

    return false;
}

void SerialManagerPrivate::handleSerialPortError(const QSerialPort::SerialPortError& error)
{
    qDebug() << "error code" << error;
    switch (error)
    {
    case QSerialPort::NotOpenError:
    case QSerialPort::DeviceNotFoundError:
    case QSerialPort::PermissionError:
    case QSerialPort::OpenError:
    case QSerialPort::ParityError:
    case QSerialPort::FramingError:
    case QSerialPort::BreakConditionError:
    case QSerialPort::ResourceError:
    case QSerialPort::UnknownError:
    case QSerialPort::WriteError:
    case QSerialPort::ReadError:
    case QSerialPort::UnsupportedOperationError:
        q->tryReconnect();
        break;
    case QSerialPort::TimeoutError:
    case QSerialPort::NoError:
        return;
    }
}

void SerialManagerPrivate::onConnect()
{
    emit q->_portStatusChanged(SerialManager::PortConnected);

    connect(comPort, SIGNAL(readyRead()),
            reader, SLOT(readPortDataToBuffer()), Qt::UniqueConnection);

    connect(comPort, SIGNAL(error(QSerialPort::SerialPortError)),
            this, SLOT(handleSerialPortError(QSerialPort::SerialPortError)),
            (Qt::ConnectionType) (Qt::QueuedConnection | Qt::UniqueConnection));

    stopScanComTimer();
    startWorkerThreads();
}

void SerialManagerPrivate::onDisconnect()
{
    emit q->_portStatusChanged(SerialManager::PortDisconneted);

    if (q->isPortConnected()) {
        disconnect(comPort, SIGNAL(readyRead()), 0, 0);
        disconnect(comPort, SIGNAL(error(QSerialPort::SerialPortError)), 0, 0);

        QCoreApplication::removePostedEvents(q);
    }

    stopWorkerThreads();
    closeAndDestroyPort();
}

void SerialManagerPrivate::stopScanComTimer()
{
    if (scanComTimer.isActive()) {
        scanComTimer.stop();
    }
}

void SerialManagerPrivate::startScanComTimer()
{
    if (!scanComTimer.isActive()) {
        scanComTimer.start();
    }
}


/**
 * @brief SerialManager::SerialManager
 * @param ui
 * @param parent
 */
SerialManager::SerialManager(QObject *) :
    d(new SerialManagerPrivate(this))
{
    moveToThread(this);
    d->moveToThread(this);
    qRegisterMetaType<QSerialPort::SerialPortError>("QSerialPort::SerialPortError");
}

SerialManager::~SerialManager()
{
    d->stopWorkerThreads();
    d->closeAndDestroyPort();

    delete d;
}

AbstractSerialReader *SerialManager::createReader()
{
    return new SerialReader;
}

AbstractSerialWritter *SerialManager::createWritter()
{
    return new SerialWritter;
}

void SerialManager::startUpdateFirmware(const QByteArray& data)
{
    FlashHelper flashHelper;
    flashHelper.setPort(d->comPort);

    connect(&flashHelper, SIGNAL(_flashProgressChanged(int)),
            this, SIGNAL(_flashProgressChanged(int)));

    connect(&flashHelper, SIGNAL(_flashStatusChanged(int)),
            this, SIGNAL(_flashStatusChanged(int)));

    if (! isPortConnected()) { // if current not connect, old firmware failed
        foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
        {
#ifdef RELEASE_VERSION
            if (!d->productIds.contains(info.productIdentifier())) continue;
            if (!d->vendorIds.contains(info.vendorIdentifier())) continue;
#endif

            QSerialPort *port = new QSerialPort(info);
            if (d->openPort(port)) {
                d->comPort = port;
                break;
            }
            delete port;
        }
        if (! isPortConnected()) {
            // can not open port to write new firmware
            flashHelper.emitFlashStatus(FlashHelper::PortCannotOpen);
            return;
        }
    }
    else {
        // disconnect connection, use blocking method
        disconnect(d->comPort, SIGNAL(readyRead()), 0, 0);
    }

    if (flashHelper.updateFlash(data)) { // if write flash success
        // emit signal start try reconnect
        flashHelper.emitFlashStatus(FlashHelper::StartReconnect);

        // disconnect port va thu connect lai de test firmware moi
        d->onDisconnect();
        for (int i = 0; i < 5; i++) {
            d->scanCom();
            if (isPortConnected()) { // if reconnect success
                flashHelper.emitFlashStatus(FlashHelper::FlashResultOk);
                return;
            }
        }
    }

    // has error
    flashHelper.emitFlashStatus(FlashHelper::FlashResultError);
}

void SerialManager::tryConnect()
{
    if (!isPortConnected()) {
        d->startScanComTimer();
    }
}

void SerialManager::tryReconnect()
{
    d->onDisconnect();
    d->startScanComTimer();
}

void SerialManager::readSingleAddress(const QVariant &data)
{
    emit d->_needReadSingleAddr(data);
}

void SerialManager::readMultipleAddress(const QVariant &data)
{
    emit d->_needReadMultipleAddr(data);
}

void SerialManager::writeConfig(const QVariant &data)
{
    emit d->_needWriteConfig(data);
}

void SerialManager::writeConfigBlocking(const QVariant &data)
{
    emit d->_needWriteConfigBlocking(data);
}

void SerialManager::clearAllSignalsInReader()
{
    QCoreApplication::removePostedEvents(d->writter);
}

bool SerialManager::isPortConnected() const
{
    return (d->comPort != NULL);
}

QSerialPort *SerialManager::getPort() const
{
    return d->comPort;
}

void SerialManager::setHelloPairString(const QString &req, const QString &res)
{
    d->helloRequest = req;
    d->helloResponse = res;
}

void SerialManager::addProductId(const QVariant &producId)
{
    d->productIds.append((quint16) producId.toInt());
}

void SerialManager::addVendorId(const QVariant &vendorId)
{
    d->vendorIds.append((quint16) vendorId.toInt());
}






