#ifndef SLIDERADAPTOR_H
#define SLIDERADAPTOR_H

#include "abstractfieldadaptor.h"

class ADAPTORVIEWSSHARED_EXPORT SliderAdaptor : public AbstractFieldAdaptor
{
    Q_OBJECT
public:
    SliderAdaptor(QObject *field, QObject* parent = 0);

    virtual QVariant getValue() const;
    virtual void setValue(const QVariant &value);

Q_SIGNALS:
    void _sliderValueChanged(QObject*, int);

protected Q_SLOTS:
    virtual void sliderPressed();
    virtual void sliderReleased();
    virtual void sliderActionTriggered(int);
    virtual void sliderValueChanged(int);

};

#endif // SLIDERADAPTOR_H

