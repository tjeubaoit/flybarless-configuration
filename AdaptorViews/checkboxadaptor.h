#ifndef CHECKBOXADAPTOR_H
#define CHECKBOXADAPTOR_H

#include "abstractfieldadaptor.h"
#include <QCheckBox>

class ADAPTORVIEWSSHARED_EXPORT CheckBoxAdaptor : public AbstractFieldAdaptor
{
    Q_OBJECT

public:
    CheckBoxAdaptor(QObject* field, QObject* parent = 0);

    virtual QVariant getValue() const;
    virtual void setValue(const QVariant &value);

Q_SIGNALS:
    void _clicked(QObject*, bool);
    void _toggled(bool);

protected Q_SLOTS:
    virtual void clicked(bool);

};

#endif // CHECKBOXADAPTOR_H
