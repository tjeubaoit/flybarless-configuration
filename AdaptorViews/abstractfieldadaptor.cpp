#include "abstractfieldadaptor.h"

AbstractFieldAdaptor::AbstractFieldAdaptor(QObject *obj, QObject *parent)
    : QObject(parent),
      m_field(obj),
      m_readOnly(false)
{

}

void AbstractFieldAdaptor::setInputField(QObject *field)
{
    this->m_field = field;
}

QObject *AbstractFieldAdaptor::inputField() const
{
    return m_field;
}

void AbstractFieldAdaptor::removeField()
{
    m_field = 0;
}

bool AbstractFieldAdaptor::isReadOnly() const
{
    return m_readOnly;
}

void AbstractFieldAdaptor::setReadOnly(bool readOnly)
{
    m_readOnly = readOnly;
}

