#ifndef LINEEDITADAPTOR_H
#define LINEEDITADAPTOR_H

#include "abstractfieldadaptor.h"

class QEvent;

class ADAPTORVIEWSSHARED_EXPORT LineEditAdaptor : public AbstractFieldAdaptor
{
    Q_OBJECT

public:
    LineEditAdaptor(QObject* field, QObject* parent = 0);

    virtual QVariant getValue() const;
    virtual void setValue(const QVariant &value);

Q_SIGNALS:
    void _editReturnPressed(QObject*);
    void _editTextEdited(const QString&);
    void _editLostFocus(QObject*);

protected:
    bool eventFilter(QObject *, QEvent *);
    void emitValueChanged();

protected Q_SLOTS:
    virtual void returnPressed();
    virtual void textEdited(const QString&);

private:
    QString m_tmpText;
    bool m_needUndo;

};

#endif // LINEEDITADAPTOR_H
