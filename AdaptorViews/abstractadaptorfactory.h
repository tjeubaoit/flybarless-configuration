#ifndef ABSTRACTADAPTORFACTORY_H
#define ABSTRACTADAPTORFACTORY_H

#include "adaptorviews_global.h"

class AbstractFieldAdaptor;
class QObject;

class AbstractAdaptorFactory
{
public:
    virtual AbstractFieldAdaptor *createAdaptor(int, QObject*) = 0;
};

#endif // ABSTRACTADAPTORFACTORY_H

