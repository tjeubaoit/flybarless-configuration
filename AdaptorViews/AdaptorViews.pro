#-------------------------------------------------
#
# Project created by QtCreator 2014-08-27T15:36:07
#
#-------------------------------------------------

QT       += widgets gui

TARGET = vtadaptorviews
TEMPLATE = lib

DEFINES += ADAPTORVIEWS_LIBRARY

SOURCES += \
    abstractfieldadaptor.cpp \
    buttongroupadaptor.cpp \
    checkboxadaptor.cpp \
    comboboxadaptor.cpp \
    lineeditadaptor.cpp \
    slideradaptor.cpp \
    sliderwithlineeditadaptor.cpp \
    checkablebuttonadaptor.cpp

HEADERS +=\
        adaptorviews_global.h \
    abstractfieldadaptor.h \
    buttongroupadaptor.h \
    checkboxadaptor.h \
    comboboxadaptor.h \
    lineeditadaptor.h \
    slideradaptor.h \
    sliderwithlineeditadaptor.h \
    abstractadaptorfactory.h \
    checkablebuttonadaptor.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
