#include "checkablebuttonadaptor.h"

CheckableButtonAdaptor::CheckableButtonAdaptor(QObject *field, QObject *parent)
    : AbstractFieldAdaptor(field, parent)
{
    QAbstractButton *btn = qobject_cast<QAbstractButton*> (field);

    connect(btn, SIGNAL(clicked(bool)), this, SLOT(clicked(bool)));
    connect(btn, SIGNAL(toggled(bool)), this, SIGNAL(_toggled(bool)));
}

QVariant CheckableButtonAdaptor::getValue() const
{
    QAbstractButton *btt = qobject_cast<QAbstractButton*> (m_field);
    return (btt->isChecked() ? CHECKABLE_BUTTON_CHECKED
                             : CHECKABLE_BUTTON_UNCHECKED);
}

void CheckableButtonAdaptor::setValue(const QVariant &value)
{
    QAbstractButton *btt = qobject_cast<QAbstractButton*> (m_field);
    btt->setChecked(GET_VALUE(value));
}

void CheckableButtonAdaptor::clicked(bool checked)
{
    emit _clicked(m_field, checked);
    emit _adaptorValueChanged(getValue());
}
