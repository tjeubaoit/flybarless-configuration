#-------------------------------------------------
#
# Project created by QtCreator 2014-08-27T09:50:28
#
#-------------------------------------------------

QT       += widgets

TARGET = vtfieldadaptor
TEMPLATE = lib

DEFINES += INPUTFIELDADAPTOR_LIBRARY

SOURCES += \
    abstractfieldadaptor.cpp \
    slideradaptor.cpp \
    lineeditadaptor.cpp \
    sliderwithlineeditadaptor.cpp \
    comboboxadaptor.cpp \
    checkboxadaptor.cpp \
    buttongroupadaptor.cpp

HEADERS +=\
        inputfieldadaptor_global.h \
    adaptorfactory.h \
    checkboxadaptor.h \
    comboboxadaptor.h \
    buttongroupadaptor.h \
    lineeditadaptor.h \
    slideradaptor.h \
    abstractfieldadaptor.h \
    sliderwithlineeditadaptor.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
