#ifndef CHECKABLEBUTTONADAPTOR_H
#define CHECKABLEBUTTONADAPTOR_H

#include "abstractfieldadaptor.h"
#include <QAbstractButton>

class ADAPTORVIEWSSHARED_EXPORT CheckableButtonAdaptor : public AbstractFieldAdaptor
{
    Q_OBJECT

public:
    CheckableButtonAdaptor(QObject* field, QObject* parent = 0);

    virtual QVariant getValue() const;
    virtual void setValue(const QVariant &value);

Q_SIGNALS:
    void _clicked(QObject*, bool);
    void _toggled(bool);

protected Q_SLOTS:
    virtual void clicked(bool);

};

#endif // CHECKABLEBUTTONADAPTOR_H
